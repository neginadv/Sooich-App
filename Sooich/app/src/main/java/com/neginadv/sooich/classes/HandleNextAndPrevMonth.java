package com.neginadv.sooich.classes;

/**
 * Created by EhsanYazdanyar on 4/24/2018.
 */

public class HandleNextAndPrevMonth {

    public String Action(String Action, int BaseMonth){
        String New_M = "";

        if (Action != null) {
            if (Action.equals("N")) {
                String Temp_m = String.valueOf(BaseMonth + 1);
                if (Temp_m.equals("13")) {
                    New_M = "1";
                } else {
                    New_M = Temp_m;
                }
            } else if (Action.equals("P")) {
                String Temp_m = String.valueOf(BaseMonth - 1);
                if (Temp_m.equals("0")) {
                    New_M = "12";
                } else {
                    New_M = Temp_m;
                }
            }
        } else {
            New_M = "";
        }
        return New_M;
    }

}
