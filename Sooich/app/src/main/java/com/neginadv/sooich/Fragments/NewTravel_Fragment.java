package com.neginadv.sooich.Fragments;

import android.app.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.neginadv.sooich.Dialogs.Dialog_Verify_Travel;
import com.neginadv.sooich.Dialogs.Msg_Dialog;
import com.neginadv.sooich.Dialogs.timePickerDialog;
import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.Models.Taxi_Co;
import com.neginadv.sooich.Quick_Travel;
import com.neginadv.sooich.R;
import com.neginadv.sooich.classes.BaseBackPressedListener;
import com.neginadv.sooich.classes.StartStopRemoteView;
import com.neginadv.sooich.classes.ToastCustom;
import com.neginadv.sooich.helper.DatabaseHelper;
import com.neginadv.sooich.utils.PracticalClasses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by EhsanYazdanyar on 4/3/2018.
 */


public class NewTravel_Fragment extends Fragment {

    @BindView(R.id.NT_StartTime_view)
    TextView StartTime_view;
    @BindView(R.id.NT_TimePriod_view)
    TextView TimePriod_view;
    @BindView(R.id.TM_Temp_Time_Start)
    TextView Start_Time_invisible;
    @BindView(R.id.TM_Temp_Time_Priod)
    TextView Time_Priod_invisible;
    @BindView(R.id.NT_Payment_spinner)
    Spinner payment;
    @BindView(R.id.NT_Kilometer_spinner)
    Spinner Kilometer;
    @BindView(R.id.NT_taxiName_spinner)
    Spinner TaxiName;
    @BindView(R.id.NT_Cash)
    EditText Cash;

    private Unbinder unbinder;

    private String Current_Hour, Current_Min = "";
    private SharedPreferences Shp_QuickTravel;
    private PracticalClasses PC;
    private Msg_Dialog msg_dialog;
    private ToastCustom toastCustom;
    private ConvertNumbers_Fa_En ConNum;
    DatabaseHelper data;

    private static final String TAG = NewTravel_Fragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_newtravel, container, false);

        ini(view);

        if (Shp_QuickTravel.getInt("Status", 0) == 1) {

//            //Travel Started
//            msg_dialog = new Msg_Dialog(getActivity(), getActivity().getResources().getString(R.string.NewTravel_QuickTravel_Att), BG_Black, false, false, null);
//            msg_dialog.SetOkDialogResult(new Msg_Dialog.Msg_Ok_Btn() {
//                @Override
//                public void GetOkAction() {
//
//                    Intent intent = new Intent(getActivity(), Quick_Travel.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                    startActivity(intent);
//
//                }
//            });

            /*msg_dialog.SetCancelDialogResult(new Msg_Dialog.MSG_Cancel_Btn() {
                @Override
                public void GetCancelAction() {
                    new StartStopRemoteView().ResetServiceWithoutLunch(getActivity());
                    msg_dialog.CloseDialog();
                }
            });*/

            Log.e(TAG, "Travel Started !!");

        } // else -> Travel Not Started

        PC.SetCommaToNumbers(Cash);

        List BaseKilometer_list = data.GetBaseKilometer();
        List Fa_Kilometer = new ArrayList();
        if (BaseKilometer_list.size() > 0) {
            for (int i = 0; i < BaseKilometer_list.size(); i++) {
                Fa_Kilometer.add(ConNum.EN_TO_FA(BaseKilometer_list.get(i).toString()));
            }
            setSpinnerToAdapter(Kilometer, Fa_Kilometer);
        } else {
            Log.e(TAG, "number Of Row is : " + BaseKilometer_list.size());
        }

        Calendar Now = Calendar.getInstance();
        Current_Hour = String.valueOf(Now.get(Calendar.HOUR_OF_DAY));
        Current_Min = String.valueOf(Now.get(Calendar.MINUTE));

        if (Current_Hour.trim().length() < 2) {
            Current_Hour = "0" + Current_Hour;
        }

        if (Current_Min.trim().length() < 2) {
            Current_Min = "0" + Current_Min;
        }

        StartTime_view.setText(ConNum.EN_TO_FA(Current_Min) + " : " + ConNum.EN_TO_FA(Current_Hour));
        Start_Time_invisible.setText(Current_Hour + ":" + Current_Min);

        ArrayList<Taxi_Co> arrayList = data.GetAllTaxiCo();
        List Taxico_List = new ArrayList();
        if (arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                Taxi_Co Taxis = arrayList.get(i);
                Taxico_List.add(Taxis.getFa_name());
            }
            setSpinnerToAdapter(TaxiName, Taxico_List);
        } else {
            Log.e(TAG, "number Of Row is : " + arrayList.size());
        }

        List<String> Payment_Array = Arrays.asList(getResources().getStringArray(R.array.NewTravel_Payment_array));
        setSpinnerToAdapter(payment, Payment_Array);

        data.closeDb();

        return view;
    }

    private void ini(View view) {

        unbinder = ButterKnife.bind(this, view);
        ManageOnBackPressed();

        data = new DatabaseHelper(getActivity());
        ConNum = new ConvertNumbers_Fa_En();
        PC = new PracticalClasses();
        toastCustom = new ToastCustom();

        Shp_QuickTravel = getActivity().getSharedPreferences("Short_Travel", MODE_PRIVATE);

    }

    @OnClick(R.id.NT_back_btn)
    public void GoBack() {
        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.putExtra("MainPage", "MainPage");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @OnClick({R.id.NT_StartTime_Btn, R.id.NT_TimePriod_Btn})
    public void SetUserOwnTimes(Button btn) {
        if (btn.getId() == R.id.NT_StartTime_Btn) {
            timePickerDialog.TimePick(getActivity(), Integer.valueOf(Current_Hour), Integer.valueOf(Current_Min), StartTime_view, Start_Time_invisible);
        } else {
            timePickerDialog.TimePick(getActivity(), 0, 0, TimePriod_view, Time_Priod_invisible);
        }
    }

    @OnClick(R.id.NTF_Submit_BTN)
    public void SubmitTravel(View v) {
        PC.hideKeyboard(getActivity(), v);

        DatabaseHelper data = new DatabaseHelper(getActivity());
        List<String> Data_List = new ArrayList();
        if (Start_Time_invisible.getText().equals("") || Time_Priod_invisible.getText().equals("")) {
            toastCustom.viewToast(getActivity(), getResources().getString(R.string.New_Travel_Choose_Time));

        } else if (Cash.getText().toString().equals("")) {
            toastCustom.viewToast(getActivity(), getResources().getString(R.string.New_Travel_Insert_Fare));

        } else {
            String Taxi_EnName = data.GetTaxiCoByName(TaxiName.getSelectedItem().toString().trim(), "FA");

            String Payment_Method = payment.getSelectedItem().toString();

            if (Payment_Method.trim().equals(getString(R.string.NewTravel_Payment_Cash_Fa))) {
                Payment_Method = "Cash";
            } else if (Payment_Method.trim().equals(getString(R.string.NewTravel_Payment_Online_Fa))) {
                Payment_Method = "Online";
            }

            Data_List.add(Taxi_EnName);
            Data_List.add(Payment_Method);
            Data_List.add(Start_Time_invisible.getText().toString());
            Data_List.add(Time_Priod_invisible.getText().toString());
            Data_List.add(Kilometer.getSelectedItem().toString());
            Data_List.add(Cash.getText().toString().trim().replace(",", ""));

            //new Dialog_Verify_Travel(getActivity(), BG_Black, Data_List);

        }
        data.closeDb();
    }

    public void setSpinnerToAdapter(Spinner spin, List list) {

        final int Spinner_Lengh = (list.size()) - 1;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, list) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setGravity(Gravity.CENTER);
                ((TextView) v).setTextColor(getResources().getColor(R.color.weather_txt));
                ((TextView) v).setTextSize(15);

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) v;
                tv.setVisibility(View.VISIBLE);
                tv.setGravity(Gravity.CENTER);
                tv.setPadding(0, 10, 0, 10);

                if (position == Spinner_Lengh) {
                    tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.spinner_inner_tv2));
                } else {
                    tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.spinner_inner_tv));
                }

                //Log.e(TAG , Spinner_Lengh+"");

                return v;
            }

        };

        spin.setAdapter(dataAdapter);
    }

    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                GoBack();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (msg_dialog != null) {
            msg_dialog.dismiss();
            msg_dialog = null;
        }

        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        new StartStopRemoteView().StopService(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        new StartStopRemoteView().startService(getActivity());
        getActivity().finish();
    }

}
