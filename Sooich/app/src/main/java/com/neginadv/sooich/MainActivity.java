package com.neginadv.sooich;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.Dialogs.Dialog_GetDefaultValues;
import com.neginadv.sooich.Dialogs.Msg_Dialog;
import com.neginadv.sooich.adapters.UserAlertCustomAdapter;
import com.neginadv.sooich.adapters.main_viewpager_adapter;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.Models.User_Alert;
import com.neginadv.sooich.Models.User_Profile;
import com.neginadv.sooich.classes.Banner_Handler;
import com.neginadv.sooich.classes.CustomViewPager;
import com.neginadv.sooich.classes.GetSharedPreferencesData;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.StartStopRemoteView;
import com.neginadv.sooich.classes.ToastCustom;
import com.neginadv.sooich.classes.setCamaToNumber;
import com.neginadv.sooich.helper.Database;
import com.neginadv.sooich.helper.DatabaseHelper;
import com.neginadv.sooich.Shp.LoginSessionManager;
import com.neginadv.sooich.utils.ParsDate;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.ronash.pushe.Pushe;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    Button alert_btn, msg_btn, profile_btn;
    Button btn_right, btn_left;
    Button user_code_share1, Update_Profile;
    RelativeLayout user_profile, user_alert, user_code, UserAlert_Waiting_Rl;
    LinearLayout main_content, bottom_view;
    ListView UserAlert_ListView;
    UserAlertCustomAdapter Alert_customAdapter = null;
    ArrayList<User_Alert> alertArrayList = new ArrayList<>();

    ProgressBar adv_progress;

    Banner_Handler bannerHandler = new Banner_Handler(MainActivity.this);

    int OpenBottomViews_Area = 0;

    View Dialog_BG_Black;

    //Button Views Progress Rl
    RelativeLayout userprofile_progress_RL;

    //Main Button Views Rl
    RelativeLayout user_profile_detail;

    //Profile TextView
    TextView user_p_name, user_p_phone, user_p_car, user_p_daramad, user_p_sood, UserAlert_NoMsg;

    //User Code TextView
    TextView user_code_header, main_user_code, main_adv_TextView;

    View view_pager_next_view, view_pager_previous_view;
    Animation slide_right, slide_left, slide_right_back, slide_left_back;
    public CustomViewPager viewPager;
    int profile_status , alert_status , msg_status = 0;
    int next_view = 0;

    String token , username , password , User_code= "";

    private LoginSessionManager session;
    private SharedPreferences shp, shp2 , Banner_Shp, Permission_Shp , shp_UserProfile;
    private SharedPreferences.Editor editor, editor2, Banner_Editor, Permission_Shp_Editor , editor_UserProfile;

    ToastCustom toast;
    ImageView Adv_Image_View;

    ConvertNumbers_Fa_En NumFaceConverter;
    ParsDate parsDate;

    GetSharedPreferencesData getSharedPreferencesData;

    Msg_Dialog Dialog_Location;

    private static final String TAG = "Main Activity";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ini();


        /*Permission_Shp_Editor.putInt("LocationPermAsk" , 0);
        Permission_Shp_Editor.apply();*/

        //Todo  must have Manager Between Location Ask And GetDefaultValue
        boolean Location_Perm_Status = getSharedPreferencesData.GetLocationPermStatus();

  /*      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (!Location_Perm_Status) {
                //show dialog box
                Dialog_Location = new Msg_Dialog(MainActivity.this, getResources().getString(R.string.Get_Permission_Location), Dialog_BG_Black, false, true, getResources().getString(R.string.Get_Permission_CHBOX));
                Dialog_Location.cancel_Btn.setText("بستن");
                Dialog_Location.SetOkDialogResult(new Msg_Dialog.Msg_Ok_Btn() {
                    @Override
                    public void GetOkAction() {
                        //Dialog_Location.CloseDialog();
                        if (Dialog_Location.CheckBox.isChecked()) {
//                        Log.e("CheckBox : " , "is Checked");
                            Permission_Shp_Editor.putInt("LocationPermAsk", 1);
                            Permission_Shp_Editor.apply();
                        } else {
//                        Log.e("CheckBox : " , "is't Check");
                            String[] PERMISSIONS = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION};
                            for (int i = 0; i <= 1; i++) {
                                boolean showRationale = shouldShowRequestPermissionRationale(PERMISSIONS[i]);
                                if (!showRationale) {
                                    Intent intent = new Intent();
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);

                                } else {
                                    ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS, 112);
                                }
                            }
                        }
                        Dialog_Location.CloseDialog();
                    }
                });

                Dialog_Location.SetCancelDialogResult(new Msg_Dialog.MSG_Cancel_Btn() {
                    @Override
                    public void GetCancelAction() {
                        Dialog_Location.CloseDialog();
                    }
                });
            }
        }*/


        Date now = parsDate.Now();

        String Banner_Status = Banner_Shp.getString("Status", "");
        String Banner_Last_Update = Banner_Shp.getString("Last_Update", "");
        String Banner_URL = Banner_Shp.getString("Url", "");

        if (!session.isLoggedIn()) {
            Intent intent = new Intent(MainActivity.this, LoginAndReg.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

        } else {

            shp = getSharedPreferences("loginManagment", MODE_PRIVATE);
            editor = shp.edit();

            String UNAME = String.valueOf(shp.getString("TempUname", null));
            if (UNAME.equals("")) {
                token = shp.getString("token", null);
                username = shp.getString("username", null);
                password = shp.getString("password", null);
//                User_code = shp.getString("user_code", null);
            } else {
                token = shp.getString("TempToken", null);
                username = shp.getString("TempUname", null);
                password = shp.getString("TempPass", null);
//                User_code = shp.getString("TempUserCode", null);
            }

            if (!Banner_Status.equals("") && !Banner_Last_Update.equals("")) {
                if (parsDate.Parse_Time(Banner_Last_Update).before(now)) {
                    GetBanner();
                    Log.e("Banner 1 : ", "Banner Need To update");
                } else {
                    bannerHandler.Set_Banner_To_ImageView(Banner_URL, Adv_Image_View, adv_progress, main_adv_TextView);
                    Log.e("Banner 2 : ", "Banner dont Need To update , Status is :" + Banner_Shp + " - Url : " + Banner_URL);
                }
            } else {
                GetBanner();
                Log.e("Banner 3 : ", "Banner Need To update");
            }

        }

        CheckDefaultValues();

        main_viewpager_adapter adapter = new main_viewpager_adapter(this, MainActivity.this);
        viewPager.setAdapter(adapter);

        viewPager.setScrollEnable(true);

        viewPager.addOnPageChangeListener(pageChangeListener);

        btn_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(getItem(+1), true);
            }
        });

        btn_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(getItem(-1), true);
            }
        });



//
//        user_code_share1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
//
//                String SMS_Content = "";
//                SMS_Content = getResources().getString(R.string.share_User_Code_Txt1) + "\n" + User_code + "\n" + getResources().getString(R.string.share_User_Code_Txt2);
//                sendIntent.putExtra("sms_body", SMS_Content);
//                sendIntent.setType("vnd.android-dir/mms-sms");
//                startActivity(sendIntent);
//            }
//        });

        Update_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close_all();
                enable(main_content, true);
                OpenBottomViews_Area = 0;
                Intent intent = new Intent(MainActivity.this, CompleteRegisterActivity.class);
                intent.putExtra("Action", "UPDATE_P");
//                intent.putExtra("From" , "MainActivity");
//                intent.putExtra("From" , "ActivityForFragment");
                startActivity(intent);
            }
        });

        Button test_btn = findViewById(R.id.test_btn);

        test_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext() , ActivityForFragments.class);
                intent.putExtra("MainPage" , "MainPage");
                startActivity(intent);
            }
        });

    }

    private void ini() {

        /* View Pager */
        viewPager = findViewById(R.id.view_pager);

        /* Button */
        alert_btn = findViewById(R.id.main_alert_btn);
        msg_btn = findViewById(R.id.main_msg_btn);
        profile_btn = findViewById(R.id.main_profile_btn);
        btn_right = findViewById(R.id.view_pager_next);
        btn_left = findViewById(R.id.view_pager_previous);
        /* Animation */
        slide_right = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_right);
        slide_right_back = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_right_back);
        slide_left = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_left);
        slide_left_back = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_left_back);

        Adv_Image_View = findViewById(R.id.main_adv_ImageView2);

        /* Linear Layout */
        main_content = findViewById(R.id.main_content);
        bottom_view = findViewById(R.id.bottom_view);

        /* View */
        view_pager_next_view = findViewById(R.id.view_pager_next_view);
        view_pager_previous_view = findViewById(R.id.view_pager_previous_view);
        Dialog_BG_Black = findViewById(R.id.Dialog_BG_Black);


        main_adv_TextView = findViewById(R.id.main_adv_TextView);


        /*Progress*/
        adv_progress = findViewById(R.id.main_adv_progress);

        /* Class And Other*/
        NumFaceConverter = new ConvertNumbers_Fa_En();
        toast = new ToastCustom();
        session = new LoginSessionManager(getApplicationContext());
        getSharedPreferencesData = new GetSharedPreferencesData(MainActivity.this);
        parsDate = new ParsDate();

        Banner_Shp = getSharedPreferences("Main_Banner", MODE_PRIVATE);
        Banner_Editor = Banner_Shp.edit();

        Permission_Shp = getSharedPreferences("Permission", MODE_PRIVATE);
        Permission_Shp_Editor = Permission_Shp.edit();


        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Pushe.initialize(this, true);

    }

    public void close_all() {

        // TODO Alert Disabled
//        alert_btn.setBackgroundResource(R.drawable.alert1);
//        msg_btn.setBackgroundResource(R.drawable.msg1);

        profile_btn.setBackgroundResource(R.drawable.nd1);

        user_code.setVisibility(View.GONE);
        user_alert.setVisibility(View.GONE);
        user_profile.setVisibility(View.GONE);

        profile_status = 0;
        alert_status = 0;
        msg_status = 0;

        OpenBottomViews_Area = 0;

    }

    public void bottomviews(final Button btn, final String tok) {

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String txt = (String) btn.getText();

                switch (txt) {

                    case "msg":

                        // TODO Alert & Msg Disabled
//                        alert_btn.setBackgroundResource(R.drawable.alert1);

                        profile_btn.setBackgroundResource(R.drawable.nd1);
                        user_profile.setVisibility(View.INVISIBLE);
                        user_alert.setVisibility(View.INVISIBLE);
                        profile_status = 0;
                        alert_status = 0;
                        if (msg_status == 0) {
                            user_code.setVisibility(View.VISIBLE);
                            msg_btn.setBackgroundResource(R.drawable.msg2);
                            main_user_code.setText(NumFaceConverter.EN_TO_FA(User_code));
                            msg_status = 1;
                            disable(main_content, true);
                        } else {
                            user_code.setVisibility(View.INVISIBLE);
                            msg_btn.setBackgroundResource(R.drawable.msg1);
                            msg_status = 0;
                            enable(main_content, true);
                        }

                        break;

                    case "alert":
                        /*getAlert(MainActivity.this);
                        msg_btn.setBackgroundResource(R.drawable.msg1);
                        profile_btn.setBackgroundResource(R.drawable.nd1);
                        user_profile.setVisibility(View.INVISIBLE);
                        user_code.setVisibility(View.INVISIBLE);
                        msg_status = 0;
                        profile_status = 0;
                        if (alert_status == 0) {
                            user_alert.setVisibility(View.VISIBLE);
                            alert_btn.setBackgroundResource(R.drawable.alert2);
                            alert_status = 1;
                            disable(main_content, true);
                        } else {
                            user_alert.setVisibility(View.INVISIBLE);
                            alert_btn.setBackgroundResource(R.drawable.alert1);
                            alert_status = 0;
                            enable(main_content, true);
                        }*/
                        break;

                    case "profile":
                        Check_UserProfile_Status(MainActivity.this);
                        // TODO Alert & Msg Disabled
//                        alert_btn.setBackgroundResource(R.drawable.alert1);
//                        msg_btn.setBackgroundResource(R.drawable.msg1);

                        user_alert.setVisibility(View.INVISIBLE);
                        user_code.setVisibility(View.INVISIBLE);
                        msg_status = 0;
                        alert_status = 0;
                        if (profile_status == 0) {
                            user_profile.setVisibility(View.VISIBLE);
                            profile_btn.setBackgroundResource(R.drawable.nd2);
                            profile_status = 1;
                            disable(main_content, true);
                        } else {
                            user_profile.setVisibility(View.INVISIBLE);
                            profile_btn.setBackgroundResource(R.drawable.nd1);
                            profile_status = 0;
                            enable(main_content, true);
                        }
                        break;

                    default:
                        break;
                }

            }
        });
    }

    /* arrow animation */
    Handler handler = new Handler();
    Runnable Update = new Runnable() {
        public void run() {

            if (viewPager.getCurrentItem() == 0) {
                view_pager_previous_view.setVisibility(View.INVISIBLE);
            } else {
                view_pager_previous_view.setVisibility(View.VISIBLE);
            }

            if (viewPager.getCurrentItem() == viewPager.getAdapter().getCount() - 1) {
                view_pager_next_view.setVisibility(View.INVISIBLE);
            } else {
                view_pager_next_view.setVisibility(View.VISIBLE);
            }

            if (next_view != 1) {

                view_pager_next_view.startAnimation(slide_right);
                slide_right.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation arg0) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation arg0) {
                    }

                    @Override
                    public void onAnimationEnd(Animation arg0) {
                        view_pager_next_view.startAnimation(slide_right_back);
                    }
                });
            }

            if (view_pager_previous_view.getVisibility() != View.INVISIBLE) {

                view_pager_previous_view.startAnimation(slide_left);
                slide_left.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation arg0) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation arg0) {
                    }

                    @Override
                    public void onAnimationEnd(Animation arg0) {
                        view_pager_previous_view.startAnimation(slide_left_back);
                    }
                });

            }

            handler.postDelayed(this, 2000);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(Update, 1000);

    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(Update);
        if (isFinishing()) {
            Picasso.get().cancelRequest(Adv_Image_View);
        }
    }

    /* its for get position of view pager item*/
    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    /* attach my BYEKAN font */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /* view pager change animate */
    public class FadePageTransformer implements ViewPager.PageTransformer {
        public void transformPage(View view, float position) {
            view.setTranslationX(view.getWidth() * position);

            if (position <= -1.0F || position >= 1.0F) {

                view.setAlpha(0.0F);
            } else if (position == 0.0F) {
                view.setAlpha(1.0F);
            } else {
                // position is between -1.0F & 0.0F OR 0.0F & 1.0F
                view.setAlpha(1.0F - Math.abs(position));
            }
        }
    }

    public void close_button_view(Button close) {

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                close_all();
                enable(main_content, true);
                OpenBottomViews_Area = 0;
            }
        });

    }

    private Boolean exit = false;

    @Override
    public void onBackPressed() {

        if (profile_status != 0 || alert_status != 0 || msg_status != 0) {
            close_all();
            enable(main_content, true);
        } else {
            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
//                finish();
//                System.exit(0);// finish activity
            } else {
                toast.viewToast(MainActivity.this, getResources().getString(R.string.Exit_Txt));
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 3 * 1000);

            }
        }
    }

    private void logoutUser() {

        editor.putString("TempUname", "");
        editor.putString("TempPass", "");
        editor.putString("TempToken", "");
//        editor.putString("TempAdv", "");
//        editor.putString("TempUserCode", "");
        editor.commit();

        Banner_Editor.putString("Status", null);
        Banner_Editor.putString("Url", null);
        Banner_Editor.putString("Last_Update", null);
        Banner_Editor.commit();

        editor_UserProfile.putString("UserProfile", "1");
        editor_UserProfile.commit();

        session.setLogin(false);

        Intent intent = new Intent(MainActivity.this, LoginAndReg.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();

        new StartStopRemoteView().StopService(MainActivity.this);

        toast.viewToast(MainActivity.this, getResources().getString(R.string.Alert_Logout_Success));
    }

    public void Check_UserProfile_Status(Activity activity) {

        String U_Profile = shp_UserProfile.getString("UserProfile", null);
//        Log.e(TAG, "shp Value : " + U_Profile);
        if (U_Profile == null || U_Profile.equals("1")) {
            //get data from server and store in db
            getUserProfile(activity);
        } else {

            user_profile_detail.setVisibility(View.VISIBLE);

            DatabaseHelper db_Helper = new DatabaseHelper(MainActivity.this);

            List<User_Profile> User = db_Helper.GetUserProfile();
            if (User.size() > 0) {

                User_Profile user_profile = User.get(0);

                String User_Name = user_profile.getName();
                String User_Mobile = user_profile.getPhone();
                String User_Car = user_profile.getCar_Type();
                String User_Income = user_profile.getIncome();
                String User_Profit = user_profile.getProfit();

                user_p_name.setText(User_Name);
                user_p_phone.setText(NumFaceConverter.EN_TO_FA(User_Mobile));
                user_p_car.setText(NumFaceConverter.EN_TO_FA(User_Car));
                user_p_daramad.setText(NumFaceConverter.EN_TO_FA(new setCamaToNumber().Edit(User_Income)) + " " + getResources().getString(R.string.Rial));
                user_p_sood.setText(NumFaceConverter.EN_TO_FA(new setCamaToNumber().Edit(User_Profit)) + " " + getResources().getString(R.string.Rial));

//                Log.e(TAG, "have value !!!" + User_Name);

            } else {

                Log.e(TAG, "No Res !!!");
            }

        }
    }

    public void getUserProfile(final Activity activity) {

        userprofile_progress_RL.setVisibility(View.VISIBLE);
        user_profile_detail.setVisibility(View.INVISIBLE);

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.User_Profile, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                userprofile_progress_RL.setVisibility(View.INVISIBLE);
                user_profile_detail.setVisibility(View.VISIBLE);

                try {
                    response = response.getJSONObject("user_data");

                    String user_name = response.getString("fullname");
                    String user_phone = response.getString("mobile_no");
                    String user_car = new DatabaseHelper(MainActivity.this).GetCarsByName(response.getString("car_type"), "EN");
                    String user_income = response.getString("Income");
                    String user_profit = response.getString("profit");
                    String user_city = response.getString("city");
                    String user_state = response.getString("state");
                    String user_car_model = response.getString("car_type_models");
                    String user_base_kilometer = response.getString("base_kilometer");
                    String user_mount_cost = response.getString("mount_cost");
                    String user_price_tire = response.getString("price_tires");

                    /* Set Data 2 Array For Db Inserting */
                    final List<User_Profile> userProfile_List = new ArrayList<>();
                    User_Profile userProfile = new User_Profile();

                    userProfile.setName(user_name);
                    userProfile.setCar_Type(user_car);
                    userProfile.setPhone(user_phone);
                    userProfile.setIncome(user_income);
                    userProfile.setProfit(user_profit);
                    userProfile.setCity(user_city);
                    userProfile.setState(user_state);
                    userProfile.setCar_Model(user_car_model);
                    userProfile.setBase_Kilometer(user_base_kilometer);
                    userProfile.setMount_Cost(user_mount_cost);
                    userProfile.setPrice_Tire(user_price_tire);

                    userProfile_List.add(userProfile);

                    new DatabaseHelper(MainActivity.this).DeleteFromTbl(Database.TABLE_PROFILE_DETAIL);

                    user_p_name.setText(user_name);
                    user_p_phone.setText(NumFaceConverter.EN_TO_FA(user_phone));
                    user_p_car.setText(NumFaceConverter.EN_TO_FA(user_car));
                    user_p_daramad.setText(NumFaceConverter.EN_TO_FA(new setCamaToNumber().Edit(user_income)) + " " + getResources().getString(R.string.Rial));
                    user_p_sood.setText(NumFaceConverter.EN_TO_FA(new setCamaToNumber().Edit(user_profit)) + " " + getResources().getString(R.string.Rial));

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            new DatabaseHelper(MainActivity.this).InsertToUserProfile(userProfile_List);
                        }
                    }, 1000);

                    editor_UserProfile.putString("UserProfile", "0");
                    editor_UserProfile.commit();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                userprofile_progress_RL.setVisibility(View.INVISIBLE);

                close_all();
                enable(main_content, true);

                new VolleyErrorHelper().getMessage(error, MainActivity.this, null, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", token);

                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String getUserProfile_Tag = "GetUserProfile_Tag";
        RequestController.getInstance().addToRequestQueue(strReq, getUserProfile_Tag);

    }

    public void getAlert(final Activity activity) {

        UserAlert_ListView.setVisibility(View.INVISIBLE);
        UserAlert_Waiting_Rl.setVisibility(View.VISIBLE);
        UserAlert_NoMsg.setVisibility(View.INVISIBLE);
        alertArrayList.clear();

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.User_Alerts, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
//                userprofile_progress_RL.setVisibility(View.INVISIBLE);
//                user_profile_detail.setVisibility(View.VISIBLE);
                UserAlert_Waiting_Rl.setVisibility(View.INVISIBLE);

                try {

                    JSONArray Alert_Array = response.getJSONArray("user_alerts");

//                    Log.e(TAG , Alert_Array.length()+"");

                    if (Alert_Array.length() > 0) {

                        for (int i = 0; i < Alert_Array.length(); i++) {

                            JSONObject get_Alert_Child = Alert_Array.getJSONObject(i);
                            User_Alert alert_Model = new User_Alert();

                            alert_Model.setTitle(get_Alert_Child.getString("title"));
                            alert_Model.setDescription(get_Alert_Child.getString("description"));

                            alertArrayList.add(alert_Model);

                            UserAlert_ListView.setVisibility(View.VISIBLE);

                            Alert_customAdapter = new UserAlertCustomAdapter(MainActivity.this, R.layout.user_alert_custom, alertArrayList);

                            UserAlert_ListView.setAdapter(Alert_customAdapter);
                        }


                    } else {
                        UserAlert_NoMsg.setVisibility(View.VISIBLE);
                        Log.e(TAG, "no value For Alert Array");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, e.getMessage());
                    close_all();
                    enable(main_content, true);
                }

//                Log.e("response: ", String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                userprofile_progress_RL.setVisibility(View.INVISIBLE);
                UserAlert_Waiting_Rl.setVisibility(View.INVISIBLE);

                close_all();
                enable(main_content, true);

                toast.viewToast(activity, "خطای اتصال به سرور");
                Log.e("error", String.valueOf(error));
//                toast.viewToast(activity, error.getMessage(), 10);

                //TODO Error Handler
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            String jsonError = new String(response.data);
                            try {
                                JSONObject objMain = new JSONObject(jsonError);
                                String errorMsg = objMain.getString("message");

                                if (objMain.has("error_codes")) {
                                    String errorCode = objMain.getString("error_codes");
                                    if (errorCode.equals("200")) {

                                    } else if (errorCode.equals("201")) {

                                    }
                                }

                                //toast.viewToast(activity, errorMsg, 20);
                                Log.e("error1", errorMsg);
                            } catch (JSONException e) {
                                //age aslan json nabood
                            }

                            Log.e("error", jsonError);
                            break;
                    }
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", token);

                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Get_Alert_Tag = "Get_Alert_Tag";
        RequestController.getInstance().addToRequestQueue(strReq, Get_Alert_Tag);

    }

    //TODO Last Check
    public void CheckDefaultValues() {

        shp2 = getSharedPreferences(ServiceApi.LDVU, MODE_PRIVATE);
        editor2 = shp2.edit();

        String Last_Date = shp2.getString("Date", null);

        if (Last_Date == null) {
            new Dialog_GetDefaultValues(MainActivity.this, Dialog_BG_Black);
        } else {

            ParsDate parsDate = new ParsDate();

            Date now = parsDate.Now();

            if (parsDate.GetUpdateTime(parsDate.Parse_Time(Last_Date), ServiceApi.Update_DefultValues).after(now)) {

                Log.e(TAG, "Dont Need Update Default Valuess");

            } else {
                new Dialog_GetDefaultValues(MainActivity.this, Dialog_BG_Black);
            }
        }
    }

    private void disable(ViewGroup layout, boolean viewPagerDisable) {
        layout.setEnabled(false);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                disable((ViewGroup) child, viewPagerDisable);
            } else {
                child.setEnabled(false);
            }
        }
        if (viewPagerDisable) {
            viewPager.setScrollEnable(false);
        }
    }

    private void enable(ViewGroup layout, boolean viewPagerEnable) {
        layout.setEnabled(true);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                enable((ViewGroup) child, viewPagerEnable);
            } else {
                child.setEnabled(true);
            }
        }
        if (viewPagerEnable) {
            viewPager.setScrollEnable(true);
        }
    }

    private void GetBanner() {

        adv_progress.setVisibility(View.VISIBLE);
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.Banner_Url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {

                    if (response.getString("status").equals("ok")) {


                        JSONArray jsonArray = response.getJSONArray("user_banner");

                        if (jsonArray.length() > 0) {

                            Log.e("Json Array Length : ", jsonArray.length() + "");

                            int Json_Index = 0;
                            if (jsonArray.length() > 1) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    if (jsonObject.getString("is_public").equals("0")) {
                                        Json_Index = i;
                                    }
                                }
                            } else {
                                Json_Index = 0;
                            }

                            JSONObject jsonObject = jsonArray.getJSONObject(Json_Index);

                            if (jsonObject.length() > 0) {

                                Log.e(TAG, jsonObject.getString("imgban"));

                                String ImgUrl = ServiceApi.BASE_URL + jsonObject.getString("imgban");

                                String Now = parsDate.MyNow_DateFormat();

                                Banner_Editor.putString("Status", "1");
                                Banner_Editor.putString("Url", ImgUrl);
                                Banner_Editor.putString("Last_Update", Now);
                                Banner_Editor.commit();

                                bannerHandler.Set_Banner_To_ImageView(ImgUrl, Adv_Image_View, adv_progress, main_adv_TextView);

                            } else {
                                main_adv_TextView.setVisibility(View.VISIBLE);
                                adv_progress.setVisibility(View.INVISIBLE);
                            }
                        } else {
                            main_adv_TextView.setVisibility(View.VISIBLE);
                            adv_progress.setVisibility(View.INVISIBLE);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    main_adv_TextView.setVisibility(View.VISIBLE);
                    adv_progress.setVisibility(View.INVISIBLE);
                }

//                Log.e(TAG, "response:  " + String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                main_adv_TextView.setVisibility(View.VISIBLE);
                adv_progress.setVisibility(View.INVISIBLE);

                new VolleyErrorHelper().getMessage(error, MainActivity.this, null, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", token);
                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Reg_Tag = "Update_Profile_Tag";
        RequestController.getInstance().addToRequestQueue(strReq, Reg_Tag);


    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            if (viewPager.getCurrentItem() == 0) {
                view_pager_previous_view.setVisibility(View.INVISIBLE);
            } else {
                view_pager_previous_view.setVisibility(View.VISIBLE);
            }

            if (viewPager.getCurrentItem() == viewPager.getAdapter().getCount() - 1) {
                view_pager_next_view.setVisibility(View.INVISIBLE);
                next_view = 1;
            } else {
                view_pager_next_view.setVisibility(View.VISIBLE);
                next_view = 0;
            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }


    };

}
