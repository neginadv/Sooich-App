package com.neginadv.sooich.classes;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Admin on 4/23/2018.
 */

public class setCamaToNumber {

    public String Edit(String orginalSTR){

        String Return_Num = null;

        if (orginalSTR.length()>0){
            Long longval;
            if (orginalSTR.contains(",")){
                orginalSTR = orginalSTR.replaceAll("," , "");
            }
            longval = Long.parseLong(orginalSTR);
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.applyPattern("#,###,###,###");
            String formattedString = formatter.format(longval);

            Return_Num = formattedString;
        }else{
            Return_Num = "";
        }

        return Return_Num;
    }

}
