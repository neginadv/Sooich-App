package com.neginadv.sooich.Shp;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by EhsanYazdanyar on 24/11/2018.
 */

public class UserProfileManager {

    private static String TAG = UserProfileManager.class.getSimpleName();

    private SharedPreferences UserProfile_Shp;
    private SharedPreferences.Editor Editor;
    private Context c;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "U_Profile_Updated";
    private static final String PREF_USER_PROFILE = "UserProfile";

    public UserProfileManager(Context context) {
        this.c = context;
        UserProfile_Shp = c.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        Editor = UserProfile_Shp.edit();
    }


    public boolean NeedUpdate() {
        String U_Profile = UserProfile_Shp.getString(PREF_USER_PROFILE, null);
        if (U_Profile == null || U_Profile.equals("1")) {
            //get data from server and store in db
            return true;
        } else {
            return false;
        }
    }

    public void SetUpdated(){
        Editor.putString(PREF_USER_PROFILE, "0");
        Editor.commit();
    }

    public void ResetProfile(){
        Editor.putString(PREF_USER_PROFILE, "1");
        Editor.commit();
    }



}
