package com.neginadv.sooich.utils;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.neginadv.sooich.MainActivity;
import com.neginadv.sooich.Models.CarType;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.helper.DatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by EhsanYazdanyar on 4/4/2018.
 */

public class GetDefaultValue {

    private String CITY = "city";
    private String CAR_TYPE = "car_type";
    private String TAXI_CO_LIST = "TAXI_CO_LIST";
    private String FUEL_EFFICIENCY = "fuel_efficiency";
    private String PRICE_TIRES = "price_tires";
    private String STATE = "state";
    private String MOUNT_COST = "mount_cost";
    private String BASE_KILOMETER = "base_kilometer";

    public void GetDefaultValues(final Activity activity , final String url , final String KeyObj) {

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {

                    int i = 1;
                    response = response.getJSONObject(KeyObj);
                    Iterator iterator = response.keys();
                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();

//                        new DatabaseHelper(activity).insertCars(key , response.getString(key));

                        Log.e("res ", i + " - key = " + key + " ,value = " + response.getString(key));
                        i = i+1;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("response: ", String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("error", String.valueOf(error));

                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            String jsonError = new String(response.data);
                            try {
                                JSONObject objMain = new JSONObject(jsonError);
                                String errorMsg = objMain.getString("message");

                                if (objMain.has("error_codes")) {
                                    String errorCode = objMain.getString("error_codes");
                                    if (errorCode.equals("200")) {

                                    } else if (errorCode.equals("201")) {

                                    }
                                }

                                Log.e("error1", errorMsg);
                            } catch (JSONException e) {
                                //age aslan json nabood
                            }

                            Log.e("error", jsonError);
                            break;
                    }
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("APITOKEN", ServiceApi.API_TOKEN);

                return params;
            }
        };

        int socketTimeout = 5000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        RequestQueue queue = Volley.newRequestQueue(activity);
        queue.add(strReq);

    }

}


