package com.neginadv.sooich.Models;

/**
 * Created by EhsanYazdanyar on 4/4/2018.
 */

public class CarType {

    private int id;
    private String car_fa;
    private String car_en;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCar_fa() {
        return car_fa;
    }

    public void setCar_fa(String car_fa) {
        this.car_fa = car_fa;
    }

    public String getCar_en() {
        return car_en;
    }

    public void setCar_en(String car_en) {
        this.car_en = car_en;
    }


}
