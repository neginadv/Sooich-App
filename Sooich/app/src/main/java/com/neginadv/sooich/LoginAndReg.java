package com.neginadv.sooich;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.iid.InstanceID;
import com.neginadv.sooich.Dialogs.DialogVerifyForget;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.classes.KeyBoardVisibilityListener;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.StartStopRemoteView;
import com.neginadv.sooich.classes.ToastCustom;
import com.neginadv.sooich.Shp.LoginSessionManager;
import com.neginadv.sooich.utils.KeyboardListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.ronash.pushe.Pushe;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginAndReg extends AppCompatActivity implements KeyBoardVisibilityListener {

    @BindView(R.id.login_progress)
    ProgressBar login_progress;
    @BindView(R.id.register_progress)
    ProgressBar register_progress;

    @BindView(R.id.login_forget_btn)
    Button ForgotPass;

    @BindView(R.id.login_main_btn)
    Button LoginBtn;
    @BindView(R.id.register_btn)
    Button RegisterBtn;

    @BindView(R.id.reg_TextView_Rl)
    RelativeLayout Reg_Header;

    @BindView(R.id.login_username_edt)
    EditText login_username_edt;
    @BindView(R.id.login_password_edt)
    EditText login_password_edt;
    @BindView(R.id.reg_mobile_edt)
    EditText reg_mobile_edt;
    @BindView(R.id.reg_pass_edt)
    EditText reg_pass_edt;
    @BindView(R.id.reg_pass_r_edt)
    EditText reg_pass_r_edt;
    @BindView(R.id.reg_code_edt)
    EditText reg_code_edt;

    @BindView(R.id.login_checkbox)
    CheckBox CheckBox;

    String mobile, password = "";
    ToastCustom toast;
    private LoginSessionManager LoginSession;
    private SharedPreferences shp;
    private SharedPreferences.Editor editor;
    private SlidingUpPanelLayout mLayout;
    private Boolean ExitApp = false;

    private String VerifyCode = "";
    ConvertNumbers_Fa_En NumFaceConverter;

    boolean KeyVisible = false;

    private static String TAG = LoginAndReg.class.getSimpleName();

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_and_reg);

        ini();

        Log.e(TAG, "Poshe Id : " + Pushe.getPusheId(getApplicationContext()));

        String Saved_username = LoginSession.GetSavedUserName();
        String Saved_password = LoginSession.GetSavedPassword();

        if (!Saved_username.equals("")) {
            login_username_edt.setText(Saved_username);
            login_password_edt.setText(Saved_password);
            login_password_edt.setTypeface(login_username_edt.getTypeface());
            CheckBox.setChecked(true);
        }

        if (LoginSession.isLoggedIn()) {
            Intent intent = new Intent(LoginAndReg.this, ActivityForFragments.class);
            intent.putExtra("MainPage", "MainPage");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        }


        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.i(TAG, "onPanelStateChanged " + newState);

                if (KeyVisible && mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    hideKeyboard(panel);
                }

            }
        });


        login_password_edt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {

                    hideKeyboard(v);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            login_password_edt.clearFocus();
                            LoginBtn.requestFocus();

                        }
                    }, 1200);

                    return true;
                }
                return false;
            }
        });

    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private void ini() {

        ButterKnife.bind(this);

        KeyboardListener.setKeyboardVisibilityListener(LoginAndReg.this, R.id.slide_pannel_lay, this);

        // class And Other
        LoginSession = new LoginSessionManager(getApplicationContext());
        NumFaceConverter = new ConvertNumbers_Fa_En();
        toast = new ToastCustom();

        shp = getSharedPreferences("AdvManager", MODE_PRIVATE);
        editor = shp.edit();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mLayout = findViewById(R.id.slide_pannel_lay);

    }

    @OnClick(R.id.login_forget_btn)
    public void ForgotPassBtn() {
        Intent intent = new Intent(LoginAndReg.this, ForgotPassActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @OnClick(R.id.login_main_btn)
    public void LoginBtn(View view) {
        String username = NumFaceConverter.FA_TO_EN(login_username_edt.getText().toString());
        String pass = login_password_edt.getText().toString().trim();

        if (username.isEmpty() || pass.isEmpty()) {
            toast.viewToast(LoginAndReg.this, getResources().getString(R.string.login_UserOrPass_Is_Empty));
        } else {
            if (username.length() < 11) {
                toast.viewToast(LoginAndReg.this, getResources().getString(R.string.login_Error_Mobile));
            } else if (!username.substring(0, 2).equals("09")) {
                toast.viewToast(LoginAndReg.this, getResources().getString(R.string.login_Error_Mobile));
            } else {

                hideKeyboard(view);
                Login(username, pass);

            }
        }
    }

    private void BtnEnable(boolean Enable) {

        LoginBtn.setEnabled(Enable);
        ForgotPass.setEnabled(Enable);
        mLayout.setEnabled(Enable);
        CheckBox.setEnabled(Enable);
        login_username_edt.setEnabled(Enable);
        login_password_edt.setEnabled(Enable);
        reg_mobile_edt.setEnabled(Enable);
        reg_pass_edt.setEnabled(Enable);
        reg_pass_r_edt.setEnabled(Enable);
        reg_code_edt.setEnabled(Enable);
        RegisterBtn.setEnabled(Enable);

    }

    @OnClick(R.id.register_btn)
    public void RegisterBtn(View view) {
        mobile = reg_mobile_edt.getText().toString().trim();
        password = reg_pass_edt.getText().toString().trim();
        String repassword = reg_pass_r_edt.getText().toString().trim();
        String code = reg_code_edt.getText().toString().trim();

        if (!mobile.isEmpty() && !password.isEmpty() && !repassword.isEmpty()) {
            if (!password.equals(repassword)) {

                toast.viewToast(LoginAndReg.this, getResources().getString(R.string.login_Inputs_Not_Equal));

            } else if (mobile.length() < 11) {

                toast.viewToast(LoginAndReg.this, getResources().getString(R.string.login_Error_Mobile));

            } else if (!mobile.substring(0, 2).equals("09")) {
                toast.viewToast(LoginAndReg.this, getResources().getString(R.string.login_Error_Mobile));
            } else {
                // every things ok
                register(mobile, password, code);
                hideKeyboard(view);
            }
        } else {

            toast.viewToast(LoginAndReg.this, getResources().getString(R.string.login_Error_Insert_All_Inputs));

        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void register(final String mobile, final String password, final String code) {

        JSONObject Obj = new JSONObject();
        try {
            Obj.put("mobile_no", mobile);
            Obj.put("user_pass", password);
            Obj.put("introduction_code", code);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        register_progress.setVisibility(View.VISIBLE);
        BtnEnable(false);

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.register_page, Obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                register_progress.setVisibility(View.INVISIBLE);
                BtnEnable(true);

                new DialogVerifyForget(LoginAndReg.this, mobile, password);


                try {
                    if (response.get("status") == "ok") {
                        VerifyCode = response.getString("verifi_code");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("response: ", String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                register_progress.setVisibility(View.INVISIBLE);
                BtnEnable(true);

                new VolleyErrorHelper().getMessage(error, LoginAndReg.this, null, null);

            }
        });

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Register_req = "Register_Req";
        RequestController.getInstance().addToRequestQueue(strReq, Register_req);

    }

    public void Login(final String mobile, final String password) {

        String User_Unique_Id = InstanceID.getInstance(getApplicationContext()).getId();
        JSONObject Obj = new JSONObject();
        try {
            Obj.put("user_name", mobile);
            Obj.put("user_pass", password);
            Obj.put("notif_ID", User_Unique_Id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        login_progress.setVisibility(View.VISIBLE);
        BtnEnable(false);

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.User_Login, Obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                login_progress.setVisibility(View.INVISIBLE);
                BtnEnable(true);

                try {
                    if (response.get("status").equals("ok")) {

                        JSONObject objMain = new JSONObject(String.valueOf(response));
                        String new_token = objMain.getString("token");

                        JSONArray Banner_Array = response.getJSONArray("user_banner");
                        if (Banner_Array.length() > 0) {
                            JSONObject Banner_obj = Banner_Array.getJSONObject(0);

                            //TODO Handle Adv Temp
                            String Image_Url = Banner_obj.getString("imgban");
                            editor.putString("TempAdv", Image_Url);
                            editor.commit();
                            Log.e(TAG, "Image url : " + Image_Url);
                        }

                        LoginSession.setLogin(true);

                        if (CheckBox.isChecked()) {
                            LoginSession.SaveUserData(mobile, password, new_token);
                        }

                        LoginSession.StoreTempUser(mobile, password, new_token);

                        new StartStopRemoteView().startService(getApplicationContext());

                        toast.viewToast(LoginAndReg.this, getString(R.string.Welcome));

                        Intent intent = new Intent(LoginAndReg.this, ActivityForFragments.class);
                        intent.putExtra("MainPage", "MainPage");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("login exception =", e.getMessage());

                }

                Log.e("response: ", String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                login_progress.setVisibility(View.INVISIBLE);
                BtnEnable(true);

                new VolleyErrorHelper().getMessage(error, LoginAndReg.this, mobile, password);

            }
        });

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Login_req = "Login_Req";
        RequestController.getInstance().addToRequestQueue(strReq, Login_req);

    }

    @Override
    public void onBackPressed() {

        if (mLayout != null &&
                (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        } else {
            if (ExitApp) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                System.exit(0);// finish activity
            } else {
                toast.viewToast(LoginAndReg.this, getResources().getString(R.string.Exit_Txt));
                ExitApp = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ExitApp = false;
                    }
                }, 3 * 1000);

            }
        }

    }

    @Override
    public void onKeyboardVisibilityChange(boolean KeyboardVisible) {

        if (mLayout != null &&
                (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED || mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {
            KeyVisible = true;
            return;
        }

        if (KeyboardVisible) {
            ((RelativeLayout) findViewById(R.id.reg_TextView_Rl)).setVisibility(View.GONE);
            ((ScrollView) findViewById(R.id.Reg_ScrollView)).setVisibility(View.GONE);
        } else {
            ((RelativeLayout) findViewById(R.id.reg_TextView_Rl)).setVisibility(View.VISIBLE);
            ((ScrollView) findViewById(R.id.Reg_ScrollView)).setVisibility(View.VISIBLE);
            KeyVisible = false;
        }

    }
}






