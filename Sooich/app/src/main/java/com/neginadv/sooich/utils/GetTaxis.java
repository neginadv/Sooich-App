package com.neginadv.sooich.utils;

import android.content.Context;

import com.neginadv.sooich.R;

import java.util.ArrayList;

/**
 * Created by EhsanYazdanyar on 5/7/2018.
 */

public class GetTaxis {

    private Context c;

    public GetTaxis(Context context){
        this.c = context;
    }

    public String GetTaxisName(int index) {

        //TODO (Bayad Az Database Gerefte Shavad)
        ArrayList<String> Taxi_Array = new ArrayList<String>();

        Taxi_Array.add(0, "ding");
        Taxi_Array.add(1, "snapp");
        Taxi_Array.add(2, "tapsi");
        Taxi_Array.add(3, "carpino");

        return Taxi_Array.get(index);
    }

    public String GetTaxisTitle(int index) {

        ArrayList<String> TaxiTitle_Array = new ArrayList<String>();

        TaxiTitle_Array.add(0, c.getResources().getString(R.string.DA_Travel_With_Ding));
        TaxiTitle_Array.add(1, c.getResources().getString(R.string.DA_Travel_With_Snapp));
        TaxiTitle_Array.add(2, c.getResources().getString(R.string.DA_Travel_With_Tap30));
        TaxiTitle_Array.add(3, c.getResources().getString(R.string.DA_Travel_With_Carpino));

        return TaxiTitle_Array.get(index);
    }

    public int GetTaxiLogo(int index){

        ArrayList<Integer> TaxiLogo_Array = new ArrayList<Integer>();

        TaxiLogo_Array.add(0, R.mipmap.ding_logo_small);
        TaxiLogo_Array.add(1, R.mipmap.snapp_logo_small);
        TaxiLogo_Array.add(2, R.mipmap.tapc_logo_small);
        TaxiLogo_Array.add(3, R.mipmap.carpino_logo_small);

        return TaxiLogo_Array.get(index);
    }



}
