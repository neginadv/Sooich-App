package com.neginadv.sooich.Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neginadv.sooich.R;

import butterknife.BindViews;
import butterknife.ButterKnife;

/**
 * Created by EhsanYazdanyar on 3/6/2018.
 */

public class Msg_Dialog2 extends AlertDialog {

    private AlertDialog d;
    public Button OK_Btn, cancel_Btn;
    private TextView DM_Txt, CheckBox_TextView;
    private RelativeLayout Check_Rl;
    public CheckBox checkBox;
    private ImageView Att_Img;

    private Msg_Ok_Btn OK_BTN;
    private MSG_Cancel_Btn CANCEL_BTN;

    private ProgressBar Progress;

    public Msg_Dialog2(final Activity activity, final String Msg_Txt) {
        super(activity);

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.Theme_AppCompat_Dialog);

        LayoutInflater inflater = LayoutInflater.from(activity);
        View view = inflater.inflate(R.layout.dialog_msg2, null);

        builder.setView(view);

        ini(view);

        DM_Txt.setText(Msg_Txt);

        cancel_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CANCEL_BTN.GetCancelAction();
            }
        });

        OK_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OK_BTN.GetOkAction();
            }
        });

        d = builder.show();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);

    }

    public Msg_Dialog2(Activity activity) {
        super(activity);

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.Theme_AppCompat_Dialog);

        LayoutInflater inflater = LayoutInflater.from(activity);
        View view = inflater.inflate(R.layout.dialog_msg2, null);

        builder.setView(view);

        ini(view);

        Progress.setVisibility(View.VISIBLE);
        DM_Txt.setText(activity.getString(R.string.Loading));

        OK_Btn.setVisibility(View.GONE);
        cancel_Btn.setVisibility(View.GONE);

        d = builder.show();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);
    }

    private void ini(View view) {
        OK_Btn = view.findViewById(R.id.DM_Ok);
        cancel_Btn = view.findViewById(R.id.DM_Cancel);
        DM_Txt = view.findViewById(R.id.DM_Txt);
        Check_Rl = view.findViewById(R.id.DM_Check_Area);
        checkBox = view.findViewById(R.id.DM_Check);
        Att_Img = view.findViewById(R.id.DM_Att_Img);
        CheckBox_TextView = view.findViewById(R.id.DM_Check_Txt);
        Progress = view.findViewById(R.id.DM_Progress);
    }

    public void CheckBox(boolean Active, String Text) {

        if (Active) {
            Check_Rl.setVisibility(View.VISIBLE);
            CheckBox_TextView.setText(Text);
        }

    }

    public void HaveAttentionImage(boolean Active) {
        if (Active) {
            Att_Img.setVisibility(View.VISIBLE);
        }
    }

    public void SetOkDialogResult(Msg_Ok_Btn msg_ok_btn) {
        OK_BTN = msg_ok_btn;
    }

    public void SetCancelDialogResult(MSG_Cancel_Btn msg_cancel_btn) {
        CANCEL_BTN = msg_cancel_btn;
    }

    public interface Msg_Ok_Btn {
        void GetOkAction();
    }

    public interface MSG_Cancel_Btn {
        void GetCancelAction();
    }

    public void CloseDialog() {
        d.dismiss();
    }


}