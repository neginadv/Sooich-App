package com.neginadv.sooich.Models;

/**
 * Created by EhsanYazdanyar on 4/3/2018.
 */

public class Taxi_Co {

    private String en_name;
    private String fa_name;

    public Taxi_Co (){

    }

    public String getEn_name() {
        return en_name;
    }

    public void setEn_name(String en_name) {
        this.en_name = en_name;
    }

    public String getFa_name() {
        return fa_name;
    }

    public void setFa_name(String fa_name) {
        this.fa_name = fa_name;
    }
}
