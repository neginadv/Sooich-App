package com.neginadv.sooich.Models;

/**
 * Created by Ehsan on 20/05/2018.
 */

public class City_Names {

    private String En_Name;
    private String Fa_Name;

    public String getEn_Name() {
        return En_Name;
    }

    public void setEn_Name(String en_Name) {
        En_Name = en_Name;
    }

    public String getFa_Name() {
        return Fa_Name;
    }

    public void setFa_Name(String fa_Name) {
        Fa_Name = fa_Name;
    }
}
