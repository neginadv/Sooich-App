package com.neginadv.sooich;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.Models.CarType;
import com.neginadv.sooich.Models.State;
import com.neginadv.sooich.Models.Taxi_Co;
import com.neginadv.sooich.Shp.DefaultValueManager;
import com.neginadv.sooich.classes.AnimateFadeInFadeOut;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.helper.Database;
import com.neginadv.sooich.helper.DatabaseHelper;
import com.neginadv.sooich.Shp.LoginSessionManager;
import com.neginadv.sooich.utils.ParsDate;
import com.neginadv.sooich.utils.ReadJsonFromFile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.Splash_Update_Waiting)
    TextView UpdateWaiting;

    @BindView(R.id.Splash_Update_Btn)
    Button Update_Retry;

    @BindView(R.id.Splash_Progress)
    ProgressBar progress;

    LoginSessionManager session;
    private SharedPreferences shp;
    private TextView App_Ver;

    DefaultValueManager defaultValueManager;

    AnimateFadeInFadeOut animateFadeInFadeOut;
    boolean ShowAnimate = false;

    Unbinder unbinder;

    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ini();

        try {
            PackageInfo Pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String Pname = String.valueOf(Pinfo.versionCode);
            if (Pname.length() <= 1) {
                Pname = Pname + ".0";
            }
            App_Ver.setText("V E R" + " " + Pname);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }



        boolean GetDefaultValue = defaultValueManager.CanGetDefaultValue();

        if (GetDefaultValue) {
            Log.e(TAG, "GetDefaultValues");
            GetDefaultValues(this);
        } else {
            progress.setVisibility(View.VISIBLE);
            GoNext(2000);
        }

    }

    private void ini() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);

        session = new LoginSessionManager(SplashActivity.this);
        shp = getSharedPreferences("FirstUse", MODE_PRIVATE);
        defaultValueManager = new DefaultValueManager(this);

        App_Ver = findViewById(R.id.splash_app_ver);
    }

    private void GetDefaultValues(final Activity activity) {

        ShowAnimate = true;
        UpdateWaiting.setVisibility(View.VISIBLE);
        Update_Retry.setVisibility(View.INVISIBLE);

        if (ShowAnimate) {
            animateFadeInFadeOut = new AnimateFadeInFadeOut(this, UpdateWaiting);
            animateFadeInFadeOut.start();
        }

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.Get_Default_Val, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(final JSONObject response) {

//                DeleteFromDb();

                final Handler handler = new Handler();

                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        SetPostDelayToGetValues(handler, response, "base_kilometer", 0);
                        SetPostDelayToGetValues(handler, response, "mount_cost", 500);
                        SetPostDelayToGetValues(handler, response, "price_tires", 1000);
                        SetPostDelayToGetValues(handler, response, "fuel_efficiency", 1500);
                        SetPostDelayToGetValues(handler, response, "car_type", 2000);
                        SetPostDelayToGetValues(handler, response, "TAXI_CO_LIST", 2500);
                        SetPostDelayToGetValues(handler, response, "state", 3000);
                    }
                }, 1000);


                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        DatabaseHelper Db = new DatabaseHelper(activity);
                        ArrayList<State> arrayList = Db.GetAllState();
                        Log.e(TAG, arrayList.size() + "");

                        if (arrayList.size() > 0) {

                            defaultValueManager.InsertLastUpdateTime();
                            GoNext(500);

                        } else {
                            ShowAnimate = false;
                            animateFadeInFadeOut.stop();
                            UpdateWaiting.setVisibility(View.INVISIBLE);
                            Update_Retry.setVisibility(View.VISIBLE);
                        }
                        Db.closeDb();

                    }
                }, 5000);

                Log.e("response: ", String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ShowAnimate = false;
                animateFadeInFadeOut.stop();
                UpdateWaiting.setVisibility(View.INVISIBLE);
                Update_Retry.setVisibility(View.VISIBLE);

                new VolleyErrorHelper().getMessage(error, activity, null, null);

                Log.e(TAG, "" + error.getMessage());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("APITOKEN", ServiceApi.API_TOKEN);

                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Reg_Tag = "GetDefaultValues_Tag";
        RequestController.getInstance().addToRequestQueue(strReq, Reg_Tag);

    }

    private void SetPostDelayToGetValues(Handler handler, final JSONObject jsonObject, final String obj, int TimeDelay) {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                GetValuesOfObject(jsonObject, obj);
            }
        }, TimeDelay);

    }

    @SuppressLint("LongLogTag")
    private void GetValuesOfObject(JSONObject response, String obj) {

        JSONArray jsonArray = null;
        JSONObject object = null;
        DatabaseHelper databaseHelper = new DatabaseHelper(this);

        try {

            if (obj.equals("base_kilometer")) {
                jsonArray = response.getJSONArray(obj);
                String[] Kilometer = null;
                Kilometer = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    Kilometer[i] = jsonArray.getString(i);
                }
                databaseHelper.InsertBaseKilometer(Kilometer);
//               Log.e(TAG , "ALL OF (InsertBaseKilometer) WAS SUCCESSFULLY");

            } else if (obj.equals("mount_cost")) {
                jsonArray = response.getJSONArray(obj);
                String[] mount_cost = null;
                mount_cost = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    mount_cost[i] = jsonArray.getString(i);
                }
                databaseHelper.InsertMountCost(mount_cost);
//               Log.e(TAG , "ALL OF (Insert MountCoust) WAS SUCCESSFULLY");

            } else if (obj.equals("price_tires")) {
                jsonArray = response.getJSONArray(obj);
                String[] Price_Tire = null;
                Price_Tire = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    Price_Tire[i] = jsonArray.getString(i);
                }
                databaseHelper.InsertPrice_Tire(Price_Tire);
//               Log.e(TAG , "ALL OF (Insert price tires) WAS SUCCESSFULLY");

            } else if (obj.equals("fuel_efficiency")) {
                jsonArray = response.getJSONArray(obj);
                String[] Fuel_Efficiency = null;
                Fuel_Efficiency = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    Fuel_Efficiency[i] = jsonArray.getString(i);
                }
                databaseHelper.InsertFuel_Efficiency(Fuel_Efficiency);
//               Log.e(TAG , "ALL OF (Insert Fuel Efficiency) WAS SUCCESSFULLY");

            } else if (obj.equals("car_type")) {
                object = response.getJSONObject(obj);
                List<CarType> CarType_array = new ArrayList<>();

                int i = 1;
                Iterator iterator = object.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();

                    CarType Car_type = new CarType();
                    Car_type.setCar_en(key);
                    Car_type.setCar_fa(object.getString(key));

                    CarType_array.add(Car_type);
                    i = i + 1;
                }
                databaseHelper.insertCars(CarType_array);

            } else if (obj.equals("state")) {
                object = response.getJSONObject(obj);
                List<State> state_array = new ArrayList<>();

                int i = 1;
                Iterator iterator = object.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();

                    State state = new State();
                    state.setEn_Name(key);
                    state.setFa_Name(object.getString(key));

                    state_array.add(state);
                    i = i + 1;
                }
                databaseHelper.InsertState(state_array);

            } else if (obj.equals("TAXI_CO_LIST")) {
                object = response.getJSONObject(obj);
                List<Taxi_Co> TaxiCo_array = new ArrayList<>();

                int i = 1;
                Iterator iterator = object.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();

                    Taxi_Co TaxiCo = new Taxi_Co();
                    TaxiCo.setEn_name(key);
                    TaxiCo.setFa_name(object.getString(key));
//                    Log.e(TAG , object.getString(key));

                    TaxiCo_array.add(TaxiCo);
                    i = i + 1;
                }
                databaseHelper.InsertTaxiCo(TaxiCo_array);

            } else if (obj.equals("city")) {
//                Log.e("res ", i + " - key = " + key + " ,value = " + object.getString(key));
            }

            databaseHelper.closeDb();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void DeleteFromDb() {

        DatabaseHelper database = new DatabaseHelper(this);

        database.DeleteFromTbl(Database.TABLE_MOUNT_COST);
        database.DeleteFromTbl(Database.TABLE_BASE_KILOMETER);
        database.DeleteFromTbl(Database.TABLE_CAR_TYPE);
        database.DeleteFromTbl(Database.TABLE_TAXI_CO);
        database.DeleteFromTbl(Database.TABLE_STATE);
        database.DeleteFromTbl(Database.TABLE_FUEL_EFFICIENCY);
        database.DeleteFromTbl(Database.TABLE_PRICE_TIRE);

        database.closeDb();

    }

    private void GoNext(int Delay) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String First_Status = shp.getString("Status", "");

                if (First_Status.equals("Used")) {

                    if (!session.isLoggedIn()) {
                        Intent GoToLogin = new Intent(SplashActivity.this, LoginAndReg.class);
                        GoToLogin.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(GoToLogin);
                    } else {
                        Intent GoToMain = new Intent(SplashActivity.this, ActivityForFragments.class);
                        GoToMain.putExtra("MainPage", "MainPage");
                        //GoToMain.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(GoToMain);
                    }
                } else {
                    Intent GoToMainActivity = new Intent(SplashActivity.this, TutorialActivity.class);
                    GoToMainActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(GoToMainActivity);
                }

            }
        }, Delay);

    }

    @OnClick(R.id.Splash_Update_Btn)
    public void RetryGetDefaultVal() {
        GetDefaultValues(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ShowAnimate) {
            if (animateFadeInFadeOut == null) {
                animateFadeInFadeOut = new AnimateFadeInFadeOut(this, UpdateWaiting);
            }
            animateFadeInFadeOut.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ShowAnimate) {
            animateFadeInFadeOut.stop();
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
