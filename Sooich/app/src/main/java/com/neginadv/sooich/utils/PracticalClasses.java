package com.neginadv.sooich.utils;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.neginadv.sooich.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by EhsanYazdanyar on 14/07/2018.
 */

public class PracticalClasses {

    public void setSpinnerToAdapter(Spinner spin, List list  , final Context context) {

        final int Spinner_Lengh = (list.size()) - 1;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, list) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setGravity(Gravity.CENTER);
                ((TextView) v).setTextColor(context.getResources().getColor(R.color.weather_txt));
                ((TextView) v).setTextSize(15);

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) v;
                tv.setVisibility(View.VISIBLE);
                tv.setGravity(Gravity.CENTER);
                tv.setPadding(0, 10, 0, 10);

                if (position == Spinner_Lengh) {
                    tv.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.spinner_inner_tv2));
                } else {
                    tv.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.spinner_inner_tv));
                }

                //Log.e(TAG , Spinner_Lengh+"");

                return v;
            }

        };

        spin.setAdapter(dataAdapter);
    }

    public void SetCommaToNumbers(EditText edt) {

        edt.addTextChangedListener( new TextWatcher() {
            boolean isEdiging;
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override public void afterTextChanged(Editable s) {

                if (s.length()>1){

                    if(isEdiging) return;
                    isEdiging = true;

                    String str = s.toString().replaceAll( "[^\\d]", "" );
                    double s1 = Double.parseDouble(str);

                    NumberFormat nf2 = NumberFormat.getInstance(Locale.ENGLISH);
                    ((DecimalFormat)nf2).applyPattern("#,###,###,###");
                    s.replace(0, s.length(), nf2.format(s1));

                    isEdiging = false;
                }

            }
        });
    }

    public void hideKeyboard(Activity activity , View v) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

}
