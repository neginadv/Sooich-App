package com.neginadv.sooich.Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.Models.CarType;
import com.neginadv.sooich.Models.State;
import com.neginadv.sooich.Models.Taxi_Co;
import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.VolleyErrorHelper;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.helper.Database;
import com.neginadv.sooich.helper.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Ehsan on 21/05/2018.
 */

public class Dialog_GetDefaultValues2 extends Dialog {

    private static AlertDialog.Builder builder;
    private static View v;
    private ProgressBar progress;
    private AlertDialog d;
    private Button Retry_Btn;
    private View DialogBG;
    private TextView dialog_Txt;
    private SharedPreferences shp2;
    private SharedPreferences.Editor editor2;

    OnMyDialogResult  mDialogResult;

    private static final String TAG = "Dialog Get Default Values";

    public Dialog_GetDefaultValues2(final Activity activity, View Dialog_Bg) {
        super(activity);
        this.DialogBG = Dialog_Bg;

        builder = new AlertDialog.Builder(activity, R.style.Material_Dialog);
        builder.setCancelable(false);

        LayoutInflater inflater = LayoutInflater.from(activity);
        final View view = inflater.inflate(R.layout.getdefaultvalues, null);
        Retry_Btn = (Button) view.findViewById(R.id.GDF_Retry_Btn);
        progress = (ProgressBar) view.findViewById(R.id.GDF_dialog_progress);
        dialog_Txt = (TextView) view.findViewById(R.id.GDF_dialog_Txt);
        builder.setView(view);
        v = view;
        Dialog_Bg.setVisibility(View.VISIBLE);
        shp2 = activity.getSharedPreferences(ServiceApi.LDVU , Activity.MODE_PRIVATE);
        editor2 = shp2.edit();

        GetDefaultValues(activity);

        Retry_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDefaultValues(activity);
            }
        });


        d = builder.show();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void GetDefaultValues(final Activity activity) {

        //TODO OPEN DIALOG PROGRESS
        progress.setVisibility(View.VISIBLE);
        Retry_Btn.setVisibility(View.INVISIBLE);
        dialog_Txt.setText(R.string.GDF_Update_Defaultvalue_Txt);

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.Get_Default_Val, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(final JSONObject response) {
//                disable(main_content, true);
//                disable(main_content, true);

                DeleteFromDb(activity);

                final Handler handler = new Handler();

                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        SetPostDelayToGetValues(activity, handler, response, "base_kilometer", 0);
                        SetPostDelayToGetValues(activity, handler, response, "mount_cost", 500);
                        SetPostDelayToGetValues(activity, handler, response, "price_tires", 1000);
                        SetPostDelayToGetValues(activity, handler, response, "fuel_efficiency", 1500);
                        SetPostDelayToGetValues(activity, handler, response, "car_type", 2000);
                        SetPostDelayToGetValues(activity, handler, response, "TAXI_CO_LIST", 2500);
                        SetPostDelayToGetValues(activity, handler, response, "state", 3000);
                    }
                }, 1000);

                handler.postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        DatabaseHelper data = new DatabaseHelper(activity);
                        ArrayList<State> arrayList = data.GetAllState();
                        Log.e(TAG , arrayList.size()+"");

                        if (arrayList.size() > 0) {

                            Date date = new Date();

                            String newdate = (String) DateFormat.format("yyyy-MM-dd", date);
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                           if( mDialogResult != null ){
                                mDialogResult.finish("");
                            }

                            d.dismiss();
                            DialogBG.setVisibility(View.INVISIBLE);
                            editor2.putString("Date" , newdate);
                            editor2.commit();

//                            Date myDate = null;
//                            try {
//                                myDate = sdf.parse(newdate);
//
//                            } catch (ParseException e) {
//                                e.printStackTrace();
//                                /*dialog_Txt.setText(R.string.GDF_Update_Defaultvalue_Txt_Error);
//                                progress.setVisibility(View.INVISIBLE);
//                                Retry_Btn.setVisibility(View.VISIBLE);*/
//                            }
                        } else {
//                            editor2.putString("Date" , )
                            dialog_Txt.setText(R.string.GDF_Update_Defaultvalue_Txt_Error);
                            progress.setVisibility(View.INVISIBLE);
                            Retry_Btn.setVisibility(View.VISIBLE);
                        }
                    }
                }, 5000);


                //TODO AFTER STATE DONE CLOSE DIALOG PROGRESS
//                Log.e("response: ", String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.INVISIBLE);
                Retry_Btn.setVisibility(View.VISIBLE);
                dialog_Txt.setText(R.string.GDF_Update_Defaultvalue_Txt_Error);

                new VolleyErrorHelper().getMessage(error , activity , null , null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("APITOKEN", ServiceApi.API_TOKEN);

                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Reg_Tag = "GetDefaultValues_Tag";
        RequestController.getInstance().addToRequestQueue(strReq , Reg_Tag);

    }

    private void DeleteFromDb(Activity activity){

        new DatabaseHelper(activity).DeleteFromTbl(Database.TABLE_MOUNT_COST);
        new DatabaseHelper(activity).DeleteFromTbl(Database.TABLE_BASE_KILOMETER);
        new DatabaseHelper(activity).DeleteFromTbl(Database.TABLE_CAR_TYPE);
        new DatabaseHelper(activity).DeleteFromTbl(Database.TABLE_TAXI_CO);
        new DatabaseHelper(activity).DeleteFromTbl(Database.TABLE_STATE);
        new DatabaseHelper(activity).DeleteFromTbl(Database.TABLE_FUEL_EFFICIENCY);
        new DatabaseHelper(activity).DeleteFromTbl(Database.TABLE_PRICE_TIRE);

    }

    private void SetPostDelayToGetValues(final Activity activity, Handler handler, final JSONObject jsonObject, final String obj, int TimeDelay) {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                GetValuesOfObject(activity, jsonObject, obj);
            }
        }, TimeDelay);

    }

    private void GetValuesOfObject(Activity activity, JSONObject response, String obj) {

        JSONArray jsonArray = null;
        JSONObject object = null;
//        DatabaseHelper Db = new DatabaseHelper(MainActivity.this);

        try {

            if (obj.equals("base_kilometer")) {
                jsonArray = response.getJSONArray(obj);
                String[] Kilometer = null;
                Kilometer = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    Kilometer[i] = jsonArray.getString(i);
                }
                new DatabaseHelper(activity).InsertBaseKilometer(Kilometer);
//               Log.e(TAG , "ALL OF (InsertBaseKilometer) WAS SUCCESSFULLY");

            } else if (obj.equals("mount_cost")) {
                jsonArray = response.getJSONArray(obj);
                String[] mount_cost = null;
                mount_cost = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    mount_cost[i] = jsonArray.getString(i);
                }
                new DatabaseHelper(activity).InsertMountCost(mount_cost);
//               Log.e(TAG , "ALL OF (Insert MountCoust) WAS SUCCESSFULLY");

            } else if (obj.equals("price_tires")) {
                jsonArray = response.getJSONArray(obj);
                String[] Price_Tire = null;
                Price_Tire = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    Price_Tire[i] = jsonArray.getString(i);
                }
                new DatabaseHelper(activity).InsertPrice_Tire(Price_Tire);
//               Log.e(TAG , "ALL OF (Insert price tires) WAS SUCCESSFULLY");

            } else if (obj.equals("fuel_efficiency")) {
                jsonArray = response.getJSONArray(obj);
                String[] Fuel_Efficiency = null;
                Fuel_Efficiency = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    Fuel_Efficiency[i] = jsonArray.getString(i);
                }
                new DatabaseHelper(activity).InsertFuel_Efficiency(Fuel_Efficiency);
//               Log.e(TAG , "ALL OF (Insert Fuel Efficiency) WAS SUCCESSFULLY");

            } else if (obj.equals("car_type")) {
                object = response.getJSONObject(obj);
                List<CarType> CarType_array = new ArrayList<>();

                int i = 1;
                Iterator iterator = object.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();

                    CarType Car_type = new CarType();
                    Car_type.setCar_en(key);
                    Car_type.setCar_fa(object.getString(key));

                    CarType_array.add(Car_type);
                    i = i + 1;
                }
                new DatabaseHelper(activity).insertCars(CarType_array);

            } else if (obj.equals("state")) {
                object = response.getJSONObject(obj);
                List<State> state_array = new ArrayList<>();

                int i = 1;
                Iterator iterator = object.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();

                    State state = new State();
                    state.setEn_Name(key);
                    state.setFa_Name(object.getString(key));

                    state_array.add(state);
                    i = i + 1;
                }
                new DatabaseHelper(activity).InsertState(state_array);

            } else if (obj.equals("TAXI_CO_LIST")) {
                object = response.getJSONObject(obj);
                List<Taxi_Co> TaxiCo_array = new ArrayList<>();

                int i = 1;
                Iterator iterator = object.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();

                    Taxi_Co TaxiCo = new Taxi_Co();
                    TaxiCo.setEn_name(key);
                    TaxiCo.setFa_name(object.getString(key));

                    TaxiCo_array.add(TaxiCo);
                    i = i + 1;
                }
                new DatabaseHelper(activity).InsertTaxiCo(TaxiCo_array);

            } else if (obj.equals("city")) {
//                Log.e("res ", i + " - key = " + key + " ,value = " + object.getString(key));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void setDialogResult(OnMyDialogResult dialogResult){
        mDialogResult = dialogResult;
    }

    public interface OnMyDialogResult{
        void finish(String result);
    }
}
