package com.neginadv.sooich;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.nfc.Tag;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.Dialogs.Dialog_GetDefaultValues2;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.Models.CarType;
import com.neginadv.sooich.Models.City;
import com.neginadv.sooich.Models.City_Names;
import com.neginadv.sooich.Models.State;
import com.neginadv.sooich.Models.User_Profile;
import com.neginadv.sooich.Shp.LoginSessionManager;
import com.neginadv.sooich.Shp.UserProfileManager;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.ToastCustom;
import com.neginadv.sooich.classes.setCamaToNumber;
import com.neginadv.sooich.helper.DatabaseHelper;
import com.neginadv.sooich.utils.ParsDate;
import com.neginadv.sooich.utils.PracticalClasses;
import com.neginadv.sooich.utils.ReadJsonFromFile;


import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CompleteRegisterActivity extends AppCompatActivity {

    private Spinner city_spinner, State_Spinner, car_spinner;
    Button regc_btn, Cancel_Update, Btn_Update;
    EditText mount_cost_edt, regc_name, tire_cost, Kiometer, Car_Year;
    TextView RegC_header;
    ToastCustom toast = new ToastCustom();
    String user_name = "";
    String user_pass = "";
    LinearLayout Btn_Update_Area;

    private String Car_V = "";
    private String State_V = "";
    private String City_V = "";

    private setCamaToNumber comma;

    String Token = "";

    Bundle extras = null;

    private ProgressBar pb;
    private LoginSessionManager LoginSession;

    private PracticalClasses PC;

    View Dialog_BG_Black;

    ConvertNumbers_Fa_En Num_Converter;

    private static final String V = "VERIFY";
    private static final String U = "UPDATE_P";
    String Main_Action = "";
    ReadJsonFromFile GetJsonData;

    private static final String TAG = "Complete Reg Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_register);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ini();
        GetJsonData = new ReadJsonFromFile(this);

        Cancel_Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cancel_Update();
            }
        });

        Btn_Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDataAndSend(U);
            }
        });

        regc_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDataAndSend(V);
            }
        });


        // Demo Data
//        setDataIntoSpinners(U, "state", null);
//        setDataIntoSpinners(U, "city", null);
//        RegC_header.setText(getResources().getString(R.string.Regc_Header));


        extras = getIntent().getExtras();
        if (extras != null) {

            if (extras.containsKey("Action")) {

                String Action = extras.getString("Action");
                Log.e(TAG, Action);

                assert Action != null;
                if (Action.equals(U)) {

                    Main_Action = U;

                    RegC_header.setText(getResources().getString(R.string.Regc_Header_Update));
                    Btn_Update_Area.setVisibility(View.VISIBLE);

                    boolean UserInf_Res = GetUserData();
                    if (UserInf_Res) {
                        SetDefaultData(U);
                    } else {
                        //have error with user info
                    }

                    Log.e(TAG, "Action Is Update");

                } else if (Action.equals(V)) {

                    Main_Action = V;

                    RegC_header.setText(getResources().getString(R.string.Regc_Header));
                    regc_btn.setVisibility(View.VISIBLE);

                    if (extras.containsKey("user_name")) {
                        user_name = extras.getString("user_name");
                    }

                    if (extras.containsKey("user_pass")) {
                        user_pass = extras.getString("user_pass");
                    }
                    SetDefaultData(V);
                }

            } else {
                Log.e(TAG, "Bundle Dont Have Action Key(LINE:127)");
            }


        }

        State_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

//                if (++check > 1) {

                String State_FaName = State_Spinner.getSelectedItem().toString();
                State state = GetJsonData.GetStateByName(State_FaName);
                setSpinnerToAdapter(city_spinner, GetJsonData.GetCityByStateId(state.getId()), null);

//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        PC.SetCommaToNumbers(tire_cost);
        PC.SetCommaToNumbers(Kiometer);
        PC.SetCommaToNumbers(mount_cost_edt);

    }

    private void ini() {

        comma = new setCamaToNumber();

        pb = findViewById(R.id.complete_reg_progress);

        State_Spinner = findViewById(R.id.Regc_State_Spinner);
        city_spinner = findViewById(R.id.City_Spinner);
        car_spinner = findViewById(R.id.Car_Spinner);
        Car_Year = findViewById(R.id.Car_Year_edt);

        mount_cost_edt = findViewById(R.id.mount_cost_edt);
        regc_name = findViewById(R.id.Regc_Name);
        tire_cost = findViewById(R.id.tire_cost_edt);
        Kiometer = findViewById(R.id.Kilometer_edt);

        Btn_Update_Area = findViewById(R.id.Regc_update_btns);

        regc_btn = findViewById(R.id.regc_btn);

        Cancel_Update = findViewById(R.id.Regc_Cancel_Travel);
        Btn_Update = findViewById(R.id.Regc_Submit_Travel);

        Num_Converter = new ConvertNumbers_Fa_En();

        Dialog_BG_Black = findViewById(R.id.RegC_Dialog_BG_Black);

        PC = new PracticalClasses();

        RegC_header = findViewById(R.id.RegC_header);

        LoginSession = new LoginSessionManager(this);
        Token = LoginSession.GetUserToken();


    }

    private void GetDataAndSend(String Action) {

        String name = regc_name.getText().toString().trim();
        String mount_cost = mount_cost_edt.getText().toString().trim().replace(",", "");
        String state = State_Spinner.getSelectedItem().toString().trim();
        String city = city_spinner.getSelectedItem().toString().trim();
        String car = car_spinner.getSelectedItem().toString().trim();
        String car_year = Car_Year.getText().toString().trim();
        String Tire_Price = tire_cost.getText().toString().trim().replace(",", "");
        String Kilometer_Num = Kiometer.getText().toString().trim().replace(",", "");

        if (name.isEmpty()) {
            toast.viewToast(CompleteRegisterActivity.this, getResources().getString(R.string.Complete_Reg_name));

        } else if (car_year.isEmpty()) {
            toast.viewToast(CompleteRegisterActivity.this, getResources().getString(R.string.Complete_Reg_Error_Car_Year));

        } else if (Kilometer_Num.isEmpty()) {
            toast.viewToast(CompleteRegisterActivity.this, getResources().getString(R.string.Complete_Reg_Kilometer_IsNull));

        } else if (Tire_Price.isEmpty()) {
            toast.viewToast(CompleteRegisterActivity.this, getResources().getString(R.string.Complete_Reg_TirePrice_IsNull));

        } else if (mount_cost.isEmpty()) {
            toast.viewToast(CompleteRegisterActivity.this, getResources().getString(R.string.Complete_Reg_Get_Cost));

        } else {

            if (city.equals("")) {
                city = GetJsonData.GetStateByName(state).getEn_Name();
            } else {
                city = GetJsonData.GetCityByName(city).getEn_Name();
            }

            JSONObject Obj = new JSONObject();

            if (Action.equals(U)) {
                try {
                    Obj.put("fullname", name);
                    Obj.put("price_tires", Tire_Price);
                    Obj.put("state", GetJsonData.GetStateByName(state).getEn_Name());
                    Obj.put("city", city);
                    Obj.put("car_type", GetJsonData.GetCarByName(car).getCar_en());
                    Obj.put("car_type_models", car_year);
                    Obj.put("base_kilometer", Kilometer_Num);
                    Obj.put("mount_cost", mount_cost);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Update_Profile(CompleteRegisterActivity.this, Obj);

            } else if (Action.equals(V)) {

                if (user_name.isEmpty() || user_pass.isEmpty()) {
                    toast.viewToast(CompleteRegisterActivity.this, getResources().getString(R.string.Complete_Reg_Error_User_Pass_IsNull));
                } else {
                    try {
                        Obj.put("user_name", user_name);
                        Obj.put("user_pass", user_pass);
                        Obj.put("fullname", name);
                        Obj.put("price_tires", Tire_Price);
                        Obj.put("state", GetJsonData.GetStateByName(state).getEn_Name());
                        Obj.put("city", city);
                        Obj.put("car_type", GetJsonData.GetCarByName(car).getCar_en());
                        Obj.put("car_type_models", car_year);
                        Obj.put("base_kilometer", Kilometer_Num);
                        Obj.put("mount_cost", mount_cost);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Send_Data(CompleteRegisterActivity.this, Obj);
                }
            }

        }
    }


    public void SetDefaultData(final String Action) {

        setDataIntoSpinners(Action, "car_type", Car_V);
        setDataIntoSpinners(Action, "state", State_V);

        if (Action.equals(U)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setDataIntoSpinners(Action, "city", City_V);
                }
            }, 500);
        }

    }

    @SuppressLint("LongLogTag")
    private void setDataIntoSpinners(String Action, String Spin, String value) {

        switch (Spin) {

            case "car_type":

                List<CarType> arrayList = GetJsonData.GetCars();
                if (arrayList.size() > 0) {
                    List Cars = new ArrayList<>();
                    for (int i = 0; i < arrayList.size(); i++) {
                        Cars.add(arrayList.get(i).getCar_fa());
                    }
                    if (Action.equals(U)) {
                        setSpinnerToAdapter(car_spinner, Cars, value);
                    } else if (Action.equals(V)) {
                        setSpinnerToAdapter(car_spinner, Cars, null);
                    }


                } else {
                    Log.e(TAG, "CarType is null");
                }

                break;

            case "state":

                List<State> State_Array = GetJsonData.GetState();
                if (State_Array.size() > 0) {
                    List states = new ArrayList<>();
                    for (int i = 0; i < State_Array.size(); i++) {
                        states.add(State_Array.get(i).getFa_Name());
                    }

                    if (Action.equals(U)) {
                        setSpinnerToAdapter(State_Spinner, states, value);
                    } else if (Action.equals(V)) {
                        setSpinnerToAdapter(State_Spinner, states, null);
                    }

                } else {
                    Log.e(TAG, "State is null");
                }

                break;


            case "city":

                List<City> City_Array = GetJsonData.GetCities();
                if (City_Array.size() > 0) {
                    List Cities = new ArrayList<>();
                    for (int i = 0; i < City_Array.size(); i++) {
                        Cities.add(City_Array.get(i).getFa_Name());
                    }

                    if (Action.equals(U)) {
                        State state = GetJsonData.GetStateByName(State_V);
                        setSpinnerToAdapter(city_spinner, GetJsonData.GetCityByStateId(state.getId()), value);
                    } else if (Action.equals(V)) {
                        setSpinnerToAdapter(city_spinner, Cities, null);
                    }

                } else {
                    Log.e(TAG, "City is null");
                }

                break;
        }

    }

    public void Send_Data(final Activity activity, final JSONObject object) {

        pb.setVisibility(View.VISIBLE);
        regc_btn.setEnabled(false);
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.PUT,
                ServiceApi.register_page, object, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                pb.setVisibility(View.INVISIBLE);
                regc_btn.setEnabled(true);

                try {
                    if (response.getString("status").equals("ok")) {

//                        LoginSession.SaveUserData(object.getString("user_name") , object.getString("user_pass") , null);
//                        Login_editor.putString("username", object.getString("user_name"));
//                        Login_editor.putString("password", object.getString("user_pass"));
//                        Login_editor.commit();

                        Intent intent = new Intent(activity, LoginAndReg.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.putExtra("user_name", object.getString("user_name"));
                        intent.putExtra("user_pass", object.getString("user_pass"));
                        activity.startActivity(intent);
                        activity.finish();
                        toast.viewToast(CompleteRegisterActivity.this, activity.getResources().getString(R.string.ERR_Complete_User_SuccessRegister));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("response: ", String.valueOf(response));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                pb.setVisibility(View.INVISIBLE);
                regc_btn.setEnabled(true);

                new VolleyErrorHelper().getMessage(error, activity, null, null);
            }
        });

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Reg_Tag = "Complete_Reg_Submit_Tag";
        RequestController.getInstance().addToRequestQueue(strReq, Reg_Tag);

    }

    public void Update_Profile(final Activity activity, JSONObject object) {

        pb.setVisibility(View.VISIBLE);
        State_Spinner.setEnabled(false);
        Cancel_Update.setEnabled(false);
        Btn_Update.setEnabled(false);

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.PUT,
                ServiceApi.User_Profile, object, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                pb.setVisibility(View.INVISIBLE);
                State_Spinner.setEnabled(true);
                Cancel_Update.setEnabled(true);
                Btn_Update.setEnabled(true);

                try {

                    if (response.getString("status").equals("ok")) {

                        new UserProfileManager(CompleteRegisterActivity.this).ResetProfile();

                        toast.viewToast(CompleteRegisterActivity.this, getResources().getString(R.string.Update_P_Success));

                        Intent intent = new Intent(CompleteRegisterActivity.this, ActivityForFragments.class);
                        intent.putExtra("MainPage", "MainPage");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e(TAG, "response:  " + String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pb.setVisibility(View.INVISIBLE);
                State_Spinner.setEnabled(true);
                Cancel_Update.setEnabled(true);
                Btn_Update.setEnabled(true);

                new VolleyErrorHelper().getMessage(error, activity, null, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", Token);
                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Reg_Tag = "Update_Profile_Tag";
        RequestController.getInstance().addToRequestQueue(strReq, Reg_Tag);

    }


    private View.OnTouchListener spinnerOnTouch = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                hideKeyboard(v);
            }
            return false;
        }
    };

    public void setSpinnerToAdapter(Spinner spin, List<String> list, String value) {

        final int Spinner_Lengh = (list.size()) - 1;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setGravity(Gravity.CENTER);
                ((TextView) v).setTextColor(getResources().getColor(R.color.weather_txt));
                ((TextView) v).setTextSize(15);

                return v;
            }

            @SuppressLint("LongLogTag")
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) v;
                /*if (position == 0) {
                    // Hide the second item from Spinner
                    tv.setVisibility(View.INVISIBLE);
                } else {

                    tv.setVisibility(View.VISIBLE);
                    tv.setGravity(Gravity.CENTER);
                    tv.setPadding(0, 30, 0, 30);
                    tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.spinner_inner_tv));
                }*/
                tv.setVisibility(View.VISIBLE);
                tv.setGravity(Gravity.CENTER);
                tv.setPadding(0, 30, 0, 30);

                if (position == Spinner_Lengh) {
                    tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.spinner_inner_tv2));
                } else {
                    tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.spinner_inner_tv));
                }


                return v;
            }

            // Disable the first item from Spinner
//            @Override
//            public boolean isEnabled(int position) {
//                if (position == 0) {
//                    return false;
//                } else {
//                    return true;
//                }
//            }

        };
        spin.setAdapter(dataAdapter);

        if (value != null) {
            for (int i = 0; i < dataAdapter.getCount(); i++) {
                if (value.trim().equals(dataAdapter.getItem(i).toString())) {
                    spin.setSelection(i);
                    break;
                }
            }
        }

    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void addListenerOnSpinnerItemSelection() {
        //spinner1 = (Spinner) findViewById(R.id.spinner1);
        //spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    public boolean GetUserData() {

        DatabaseHelper data = new DatabaseHelper(CompleteRegisterActivity.this);

        List<User_Profile> User = data.GetUserProfile();
        if (User.size() > 0) {

            User_Profile user_profile = User.get(0);

            Car_V = user_profile.getCar_Type();
            City_V = user_profile.getCity();
            State_V = user_profile.getState();

            regc_name.setText(user_profile.getName());
            Kiometer.setText(user_profile.getBase_Kilometer());
            tire_cost.setText(comma.Edit(user_profile.getPrice_Tire()));
            mount_cost_edt.setText(comma.Edit(user_profile.getMount_Cost()));
            Car_Year.setText(user_profile.getCar_Model());

            data.closeDb();

            return true;
        } else {
            return false;
        }


    }


    private Boolean exit = false;

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        if (Main_Action.equals(U)) {
//            extras = getIntent().getExtras();
//            if (extras != null) {
//
//                String ComeFrom = extras.getString("From");
//                assert ComeFrom !=null;
//                if (ComeFrom.equals("MainActivity")){
//                    Intent intent = new Intent(CompleteRegisterActivity.this , MainActivity.class);
//                    startActivity(intent);
//                }else if (ComeFrom.equals("ActivityForFragment")){
//                    super.onBackPressed();
//                    /*Intent intent = new Intent(CompleteRegisterActivity.this , ActivityForFragments.class);
//                    startActivity(intent);*/
//                }
//            }
            super.onBackPressed();

        } else if (Main_Action.equals(V)) {
            if (exit) {
                Intent intent = new Intent(CompleteRegisterActivity.this, LoginAndReg.class);
                startActivity(intent);
            } else {
                toast.viewToast(CompleteRegisterActivity.this, getResources().getString(R.string.Exit_Txt));
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 3 * 1000);

            }
        }

    }

    private void Cancel_Update() {
        super.onBackPressed();
    }


}