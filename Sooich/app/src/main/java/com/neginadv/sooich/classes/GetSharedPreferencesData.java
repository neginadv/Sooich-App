package com.neginadv.sooich.classes;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by EhsanYazdanyar on 4/24/2018.
 */

public class GetSharedPreferencesData {

    private SharedPreferences Permission_Shp;
    private Context c;

    public GetSharedPreferencesData(Context context) {
        this.c = context;
    }




    public boolean GetLocationPermStatus() {

        Permission_Shp = c.getSharedPreferences("Permission", MODE_PRIVATE);

//        int Location_Perm = Permission_Shp.getInt("LocationPerm", 0);
        int Location_Perm_Ask = Permission_Shp.getInt("LocationPermAsk", 0);

        int access_Granted = PackageManager.PERMISSION_GRANTED;

        if (ActivityCompat.checkSelfPermission(c, Manifest.permission.ACCESS_FINE_LOCATION) != access_Granted
                && ActivityCompat.checkSelfPermission(c, Manifest.permission.ACCESS_COARSE_LOCATION) != access_Granted) {

            if (Location_Perm_Ask == 1) {
                return true; // The user does not want to access
            } else {
                return false;
            }

        } else {
            return true; // app dont have Location access
        }

    }


}
