package com.neginadv.sooich.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.neginadv.sooich.Models.User_Alert;
import com.neginadv.sooich.R;

import java.util.ArrayList;

/**
 * Created by EhsanYazdanyar on 5/14/2018.
 */

public class UserAlertCustomAdapter extends ArrayAdapter<User_Alert>{

    private int resourceId;
    private Context context;
    ArrayList<User_Alert> UseralertArrayList;

    public UserAlertCustomAdapter(Context c, int resource, ArrayList<User_Alert> objects) {
        super(c, resource, objects);
        this.context = c;
        this.resourceId = resource;
        this.UseralertArrayList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(resourceId , viewGroup , false);

        User_Alert UserAlert = UseralertArrayList.get(position);

        TextView Title = (TextView)view.findViewById(R.id.UserAlert_Title);
        TextView Description = (TextView)view.findViewById(R.id.UserAlert_Cont);

        Title.setText(UserAlert.getTitle());
        Description.setText(UserAlert.getDescription());

        return view;
    }
}
