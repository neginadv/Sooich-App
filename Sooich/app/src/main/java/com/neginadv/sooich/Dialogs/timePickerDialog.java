package com.neginadv.sooich.Dialogs;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.R;

/**
 * Created by EhsanYazdanyar on 5/12/2018.
 */

public class timePickerDialog {


    public static void TimePick(final Activity activity,final int Current_Hour , final int Current_Min , final TextView txt, final TextView TempTime) {

        final Button sabtTime;
        final Button CLOSE;
        final TimePicker tp;
        final ConvertNumbers_Fa_En ConNum;

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.Theme_AppCompat_Dialog);
        builder.setCancelable(true);

        LayoutInflater inflater = LayoutInflater.from(activity);
        final View view = inflater.inflate(R.layout.timepicker_dialog, null);
        builder.setView(view);
        ConNum = new ConvertNumbers_Fa_En();

        tp = (TimePicker) view.findViewById(R.id.TM_timePicker);
        tp.setIs24HourView(true);
        if (Current_Hour == 0 && Current_Min == 0){
            tp.setCurrentHour(00);
            tp.setCurrentMinute(00);
        }else{
            tp.setCurrentHour(Current_Hour);
            tp.setCurrentMinute(Current_Min);
        }


        sabtTime = (Button) view.findViewById(R.id.TM_GetTime);
        CLOSE = (Button) view.findViewById(R.id.Tm_Close_Btn);

        final AlertDialog d = builder.show();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        CLOSE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });

        sabtTime.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                //Log.e("dialog" , tp.getHour() + " : " + tp.getMinute());
                int min =0 ;
                int hour =0 ;

                int currentApiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentApiVersion > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
                    hour = tp.getHour();
                    min = tp.getMinute();
                } else {
                    hour = tp.getCurrentHour();
                    min = tp.getCurrentMinute();
                }

                String Min = String.valueOf(min);
                String Hour = String.valueOf(hour);

                if (Min.trim().length() < 2) {
                    Min = "0" + Min;
                }

                if (Hour.trim().length() < 2) {
                    Hour = "0" + Hour;
                }

                if (TempTime !=null){
                    txt.setText(ConNum.EN_TO_FA(Min) + " : " + ConNum.EN_TO_FA(Hour));
                    TempTime.setText(Hour + ":" + Min);
                }else{
                    txt.setText(Hour + ":" + Min);
                }


                d.dismiss();
            }
        });

        d.setCancelable(true);

    }


}
