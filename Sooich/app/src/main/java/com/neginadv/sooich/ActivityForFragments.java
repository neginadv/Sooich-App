package com.neginadv.sooich;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.neginadv.sooich.Fragments.MainPage;
import com.neginadv.sooich.Fragments.UserProfile;
import com.neginadv.sooich.Fragments.BestHourRide;
import com.neginadv.sooich.Fragments.DailyActivity;
import com.neginadv.sooich.Fragments.DailyActivityByOrder;
import com.neginadv.sooich.Fragments.G_Omomi_Main;
import com.neginadv.sooich.Fragments.G_Safar_Main;
import com.neginadv.sooich.Fragments.HourlyActivity;
import com.neginadv.sooich.Fragments.IncomeFromEachTaxi;
import com.neginadv.sooich.Fragments.KarkardMain_Fragment;
import com.neginadv.sooich.Fragments.MostProfitOfTaxis;
import com.neginadv.sooich.Fragments.MostTravel;
import com.neginadv.sooich.Fragments.NetProfitMonth;
import com.neginadv.sooich.Fragments.NewTravel_Fragment;
import com.neginadv.sooich.Fragments.NumberOfTravel;
import com.neginadv.sooich.Fragments.SuperiorDaysRide;
import com.neginadv.sooich.classes.IOnFocusListenable;
import com.neginadv.sooich.classes.OnBackPressedListener;
import com.neginadv.sooich.Shp.LoginSessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by EhsanYazdanyar on 4/7/2018.
 */

public class ActivityForFragments extends AppCompatActivity {

    @BindView(R.id.MAF_alert_btn)
    Button alert_btn;
    @BindView(R.id.MAF_msg_btn)
    Button msg_btn;
    @BindView(R.id.MAF_profile_btn)
    Button profile_btn;

    private SharedPreferences Banner_Shp;
    private SharedPreferences.Editor editor_UserProfile, Banner_Editor, editor;

    int profile_status, alert_status, msg_status  = 0;

    LoginSessionManager LoginSession;

    private static final String TAG = ActivityForFragments.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_fragments);

        ini();

        bottomviews(profile_btn);
//        bottomviews(alert_btn);
//        bottomviews(msg_btn);

        if (!LoginSession.isLoggedIn()) {
            Intent intent = new Intent(ActivityForFragments.this, LoginAndReg.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        } else {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                CheckExtras(extras);
            }
        }

    }

    private void CheckExtras(Bundle extras) {
        if (extras.containsKey("main")) {
            loadFragment(new KarkardMain_Fragment());
        } else if (extras.containsKey("New_Travel")) {
            loadFragment(new NewTravel_Fragment());
        } else if (extras.containsKey("G_Safar_Main")) {
            loadFragment(new G_Safar_Main());
        } else if (extras.containsKey("G_Omomi_Main")) {
            loadFragment(new G_Omomi_Main());
        } else if (extras.containsKey("Number_Of_Travel")) {
            loadFragment(new NumberOfTravel());
        } else if (extras.containsKey("IncomeFromEachTaxi")) {
            loadFragment(new IncomeFromEachTaxi());
        } else if (extras.containsKey("DailyActivity")) {
            loadFragment(new DailyActivity());
        } else if (extras.containsKey("NetProfitMonth")) {
            loadFragment(new NetProfitMonth());
        } else if (extras.containsKey("HourlyActivity")) {
            loadFragment(new HourlyActivity());
        } else if (extras.containsKey("DailyActivityByOrder")) {
            loadFragment(new DailyActivityByOrder());
        } else if (extras.containsKey("BestHourRide")) {
            loadFragment(new BestHourRide());
        } else if (extras.containsKey("SuperiorDaysRide")) {
            loadFragment(new SuperiorDaysRide());
        } else if (extras.containsKey("MostTravel")) {
            loadFragment(new MostTravel());
        } else if (extras.containsKey("MostProfitOfTaxis")) {
            loadFragment(new MostProfitOfTaxis());
        } else if (extras.containsKey("MainPage")) {
            loadFragment(new MainPage());
        } else if (extras.containsKey("UserProfile")) {
            loadFragment(new UserProfile());
        }
    }

    private void ini() {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ButterKnife.bind(this);



        Banner_Shp = getSharedPreferences("Main_Banner", MODE_PRIVATE);
        Banner_Editor = Banner_Shp.edit();

        LoginSession = new LoginSessionManager(this);

    }

    protected OnBackPressedListener onBackPressedListener;

    private void loadFragment(Fragment fragment) {

        FragmentManager fr = getFragmentManager();
        FragmentTransaction fragmentTransaction = fr.beginTransaction();

        fragmentTransaction.replace(R.id.Detail_FrameLayout, fragment);
        fragmentTransaction.commit();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    @Override
    public void onBackPressed() {

        if (onBackPressedListener != null)
            onBackPressedListener.doBack();
        else
            super.onBackPressed();

    }

    public void bottomviews(final Button btn) {

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String txt = (String) btn.getText();

                hideKeyboard(view);

                switch (txt) {

                    case "msg":

                        // TODO Alert & Msg Disabled
//                        alert_btn.setBackgroundResource(R.drawable.alert1);

                        profile_btn.setBackgroundResource(R.drawable.nd1);
                        profile_status = 0;
                        alert_status = 0;
                        if (msg_status == 0) {
                            msg_btn.setBackgroundResource(R.drawable.msg2);
                            msg_status = 1;
                        } else {
                            msg_btn.setBackgroundResource(R.drawable.msg1);
                            msg_status = 0;
                        }
                        break;

                    case "alert":
                        msg_btn.setBackgroundResource(R.drawable.msg1);
                        profile_btn.setBackgroundResource(R.drawable.nd1);
                        msg_status = 0;
                        profile_status = 0;
                        if (alert_status == 0) {
                            alert_btn.setBackgroundResource(R.drawable.alert2);
                            alert_status = 1;
                        }
                        break;

                    case "profile":

                        // TODO Alert & Msg Disabled
//                        alert_btn.setBackgroundResource(R.drawable.alert1);
//                        msg_btn.setBackgroundResource(R.drawable.msg1);

                        msg_status = 0;
                        alert_status = 0;
                        if (profile_status == 0) {
                            loadFragment(new UserProfile());
                            profile_btn.setBackgroundResource(R.drawable.nd2);
                            profile_status = 1;
                        }
                        break;

                    default:
                        break;

                }

            }
        });
    }



//    public void getAlert(final Activity activity) {
//
//        UserAlert2_ListView.setVisibility(View.INVISIBLE);
//        UserAlert_Waiting_Rl.setVisibility(View.VISIBLE);
//        UserAlert_NoMsg.setVisibility(View.INVISIBLE);
//        alertArrayList.clear();
//
//        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
//                ServiceApi.User_Alerts, null, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject response) {
////                userprofile_progress_RL.setVisibility(View.INVISIBLE);
////                user_profile_detail.setVisibility(View.VISIBLE);
//                UserAlert_Waiting_Rl.setVisibility(View.INVISIBLE);
//
//                try {
//
//                    JSONArray Alert_Array = response.getJSONArray("user_alerts");
//
////                    Log.e(TAG , Alert_Array.length()+"");
//
//                    if (Alert_Array.length() > 0) {
//
//                        for (int i = 0; i < Alert_Array.length(); i++) {
//
//                            JSONObject get_Alert_Child = Alert_Array.getJSONObject(i);
//                            User_Alert alert_Model = new User_Alert();
//
//                            alert_Model.setTitle(get_Alert_Child.getString("title"));
//                            alert_Model.setDescription(get_Alert_Child.getString("description"));
//
//                            alertArrayList.add(alert_Model);
//
//                            UserAlert2_ListView.setVisibility(View.VISIBLE);
//
//                            Alert_customAdapter = new UserAlertCustomAdapter(ActivityForFragments.this, R.layout.user_alert_custom, alertArrayList);
//
//                            UserAlert2_ListView.setAdapter(Alert_customAdapter);
//                        }
//
//
//                    } else {
//                        UserAlert_NoMsg.setVisibility(View.VISIBLE);
//                        Log.e(TAG, "no value For Alert Array");
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Log.e(TAG, e.getMessage());
//                    close_all();
////                    enable(main_content, true);
//                }
//
//                Log.e("response: ", String.valueOf(response));
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
////                userprofile_progress_RL.setVisibility(View.INVISIBLE);
//                UserAlert_Waiting_Rl.setVisibility(View.INVISIBLE);
//
//                close_all();
////                enable(main_content, true);
//
////                toast.viewToast(activity, "خطای اتصال به سرور", 10);
//                Log.e("error", String.valueOf(error));
////                toast.viewToast(activity, error.getMessage(), 10);
//                toast.viewToast(activity, "خطای اتصال به سرور");
//
//                NetworkResponse response = error.networkResponse;
//                if (response != null && response.data != null) {
//                    switch (response.statusCode) {
//                        case 403:
//                            String jsonError = new String(response.data);
//                            try {
//                                JSONObject objMain = new JSONObject(jsonError);
//                                String errorMsg = objMain.getString("message");
//
//                                if (objMain.has("error_codes")) {
//                                    String errorCode = objMain.getString("error_codes");
//                                    if (errorCode.equals("200")) {
//
//                                    } else if (errorCode.equals("201")) {
//
//                                    }
//                                }
//
//                                //toast.viewToast(activity, errorMsg, 20);
//                                Log.e("error1", errorMsg);
//                            } catch (JSONException e) {
//                                //age aslan json nabood
//                            }
//
//                            Log.e("error", jsonError);
//                            break;
//                    }
//                }
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("USERTOKEN", UToken);
//
//                return params;
//            }
//        };
//
//        int socketTimeout = ServiceApi.SocketTimeout;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        strReq.setRetryPolicy(policy);
//
//        String Reg_Tag = "GetAlert_Req";
//        RequestController.getInstance().addToRequestQueue(strReq, Reg_Tag);
//
//
//    }


//    @OnClick(R.id.main_user_code_share1)
//    public void ShareUserCode(){
//        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
//        String SMS_Content = "";
//        SMS_Content = getResources().getString(R.string.share_User_Code_Txt1) + "\n" + User_Code + "\n" + getResources().getString(R.string.share_User_Code_Txt2);
//        sendIntent.putExtra("sms_body", SMS_Content);
//        sendIntent.setType("vnd.android-dir/mms-sms");
//        startActivity(sendIntent);
//    }


//    @OnClick(R.id.UP_logout)
//    public void logoutUser() {
//
//        LoginSession.setLogout();
//
//        Banner_Editor.putString("Status", null);
//        Banner_Editor.putString("Url", null);
//        Banner_Editor.putString("Last_Update", null);
//        Banner_Editor.commit();
//
//        editor_UserProfile.putString("UserProfile", "1");
//        editor_UserProfile.commit();
//
//        Intent intent = new Intent(ActivityForFragments.this, LoginAndReg.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        startActivity(intent);
//        finish();
//
//        new StartStopRemoteView().StopService(ActivityForFragments.this);
//
//        toast.viewToast(ActivityForFragments.this, getString(R.string.Alert_Logout_Success));
//
//    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        FragmentManager fr = getFragmentManager();

        if (fr instanceof IOnFocusListenable) {
            ((IOnFocusListenable) fr).onWindowFocusChanged(hasFocus);
        }

    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


/*    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }*/
}
