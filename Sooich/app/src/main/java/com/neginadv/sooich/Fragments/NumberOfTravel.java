package com.neginadv.sooich.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.Dialogs.Msg_Dialog2;
import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.VolleyErrorHelper;
import com.neginadv.sooich.classes.AnimateMonthViews;
import com.neginadv.sooich.classes.BaseBackPressedListener;
import com.neginadv.sooich.classes.ConvertDpAndPx;
import com.neginadv.sooich.classes.GetDeviceSize;
import com.neginadv.sooich.classes.GetMonthByIndex;
import com.neginadv.sooich.classes.HandleNextAndPrevMonth;
import com.neginadv.sooich.classes.IOnFocusListenable;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.Shp.LoginSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by EhsanYazdanyar on 4/7/2018.
 */

public class NumberOfTravel extends Fragment implements IOnFocusListenable {

    @BindView(R.id.NOT_Success_Cont)
    RelativeLayout Success_Cont;

    @BindView(R.id.NOT_G_Snapp_color)
    View Snapp_View;
    @BindView(R.id.NOT_G_Tap30_color)
    View Tap30_View;
    @BindView(R.id.NOT_G_Ding_color)
    View Ding_View;
    @BindView(R.id.NOT_G_Carpino_color)
    View Carpino_View;
    @BindView(R.id.Num_Of_Tr_G_Koll_color)
    View All_Travel_View;
    @BindView(R.id.NOT_arrow_month_left)
    View Previous_month;
    @BindView(R.id.NOT_arrow_month_right)
    View Next_month;

    @BindView(R.id.NOT_month_view)
    TextView month_view;
    @BindView(R.id.NOT_year)
    TextView Year_Txt;
    @BindView(R.id.NOT_Snapp_Num)
    TextView Snapp_Num;
    @BindView(R.id.NOT_Tap30_Num)
    TextView Tap30_Num;
    @BindView(R.id.NOT_Ding_Num)
    TextView Ding_Num;
    @BindView(R.id.NOT_Carpino_Num)
    TextView Carpino_Num;
    @BindView(R.id.NOT_Sum_NummTravels)
    TextView All_Travel_Num;


    Unbinder unbinder;
    private AnimateMonthViews animateMonthViews;
    int Month = 0;
    private String Token = "";
    private ConvertDpAndPx converter;
    private ConvertNumbers_Fa_En Num_Converter;

    boolean FirstLunch = true;
    private Msg_Dialog2 Error_Dialog, Progress_Dialog;

    private static final String TAG = "Number Of Travel";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_numberof_travel, container, false);
        ini(view);

        Request_NumberOfTravel(getActivity(), "");

        return view;
    }

    private void ini(View view) {

        ManageOnBackPressed();
        unbinder = ButterKnife.bind(this, view);
        converter = new ConvertDpAndPx();
        Num_Converter = new ConvertNumbers_Fa_En();
        Token = new LoginSessionManager(getActivity()).GetUserToken();

    }

    @OnClick({R.id.NOT_btn_month_right, R.id.NOT_btn_month_left})
    public void Next_PrevMonth(Button btn) {

        Progress(false);

        if (btn.getId() == R.id.NOT_btn_month_right) {
            Request_NumberOfTravel(getActivity(), "N");
        } else {
            Request_NumberOfTravel(getActivity(), "P");
        }

    }

    @OnClick(R.id.NOT_back_btn)
    public void GoBack() {
        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.putExtra("G_Safar_Main", "G_Safar_Main");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void Clear_All_Views() {

        Snapp_View.setBackgroundResource(0);
        Tap30_View.setBackgroundResource(0);
        Ding_View.setBackgroundResource(0);
        Carpino_View.setBackgroundResource(0);
        All_Travel_View.setBackgroundResource(0);

        /*Snapp_Num.setText("");
        Tap30_Num.setText("");
        Ding_Num.setText("");
        Carpino_Num.setText("");
        All_Travel_Num.setText("");*/

    }

    private void SetTaxisParameters(int income_Num_Travel, View view, TextView textView) {

        int width = new GetDeviceSize(getActivity()).GetWidth();

        int Colorize_Full_Width = converter.dpToPx(width - 102);
        int Colorize_Width_Mines_20 = converter.dpToPx(width - 102) - 20;

        int Main_Width = (income_Num_Travel * Colorize_Full_Width) / 500;
//        Log.e("Main_Width : " , Main_Width+"");

        if (Main_Width == 0) {
            view.getLayoutParams().width = 0;
        } else if (Main_Width < 20) {
            view.setBackground(getResources().getDrawable(R.drawable.horizontal_graph_bg2));
            view.getLayoutParams().width = 20;
        } else if (Main_Width > Colorize_Width_Mines_20) {

            view.setBackground(getResources().getDrawable(R.drawable.horizontal_graph_bg3));
            view.getLayoutParams().width = Colorize_Full_Width;
        } else {
            view.setBackground(getResources().getDrawable(R.drawable.horizontal_graph_bg2));
            view.getLayoutParams().width = Main_Width;
        }
        textView.setText(Num_Converter.EN_TO_FA(String.valueOf(income_Num_Travel)));
    }

    public void Request_NumberOfTravel(final Activity activity, String action) {

        String New_M = new HandleNextAndPrevMonth().Action(action, Month);

        JSONObject object = new JSONObject();

        try {
            object.put("month", New_M);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Progress(true);

        JsonObjectRequest ObjReq = new JsonObjectRequest(Request.Method.POST, ServiceApi.Number_Of_Travel, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Progress(false);
                Success_Cont.setVisibility(View.VISIBLE);
                Clear_All_Views();

                FirstLunch = false;

                try {
                    String Year = response.getString("year");
                    Month = Integer.valueOf(response.getString("month"));

                    String month_count = response.getString("month_count");
                    if (Integer.valueOf(month_count) > 0) {

                        JSONObject Trip_Taxi_Co = response.getJSONObject("trip_taxi_co");

                        if (Trip_Taxi_Co.has("ding")) {
                            SetTaxisParameters(Trip_Taxi_Co.getInt("ding"), Ding_View, Ding_Num);
                        } else {
                            SetTaxisParameters(0, Ding_View, Ding_Num);
                        }

                        if (Trip_Taxi_Co.has("snapp")) {
                            SetTaxisParameters(Trip_Taxi_Co.getInt("snapp"), Snapp_View, Snapp_Num);
                        } else {
                            SetTaxisParameters(0, Snapp_View, Snapp_Num);
                        }

                        if (Trip_Taxi_Co.has("tapsi")) {
                            SetTaxisParameters(Trip_Taxi_Co.getInt("tapsi"), Tap30_View, Tap30_Num);
                        } else {
                            SetTaxisParameters(0, Tap30_View, Tap30_Num);
                        }

                        if (Trip_Taxi_Co.has("carpino")) {
                            SetTaxisParameters(Trip_Taxi_Co.getInt("carpino"), Carpino_View, Carpino_Num);
                        } else {
                            SetTaxisParameters(0, Carpino_View, Carpino_Num);
                        }

                    } else {

                        SetTaxisParameters(0, Snapp_View, Snapp_Num);
                        SetTaxisParameters(0, Tap30_View, Tap30_Num);
                        SetTaxisParameters(0, Ding_View, Ding_Num);
                        SetTaxisParameters(0, Carpino_View, Carpino_Num);
                        SetTaxisParameters(0, All_Travel_View, All_Travel_Num);

                    }


                    SetTaxisParameters(response.getInt("month_count"), All_Travel_View, All_Travel_Num);

                    String MyMonth = new GetMonthByIndex().GetMonth(Month);
                    month_view.setText(MyMonth);
                    Year_Txt.setText(Num_Converter.EN_TO_FA(Year));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e(TAG, "response : " + response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Progress(false);

                if (FirstLunch) {
                    ErrorDialog();
                }

                new VolleyErrorHelper().getMessage(error, activity, null, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", Token);
                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        ObjReq.setRetryPolicy(policy);

        String Reg_Tag = "Request_NumberOfTravel_Tag";
        RequestController.getInstance().addToRequestQueue(ObjReq, Reg_Tag);

    }

    private void ErrorDialog() {
        Error_Dialog = new Msg_Dialog2(getActivity(), getString(R.string.ERR_RecieveData));
        Error_Dialog.HaveAttentionImage(true);
        Error_Dialog.cancel_Btn.setText(getString(R.string.Back));
        Error_Dialog.OK_Btn.setText(getString(R.string.Retry));

        Error_Dialog.SetOkDialogResult(new Msg_Dialog2.Msg_Ok_Btn() {
            @Override
            public void GetOkAction() {
                Error_Dialog.CloseDialog();
                Request_NumberOfTravel(getActivity(), "");
            }
        });

        Error_Dialog.SetCancelDialogResult(new Msg_Dialog2.MSG_Cancel_Btn() {
            @Override
            public void GetCancelAction() {
                GoBack();
            }
        });
    }

    private void Progress(boolean Enable) {

        if (Enable) {
            Progress_Dialog = new Msg_Dialog2(getActivity());
        } else {
            if (Progress_Dialog != null) {
                Progress_Dialog.CloseDialog();
                Progress_Dialog = null;
            }
        }

    }

    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                GoBack();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (animateMonthViews == null) {
            animateMonthViews = new AnimateMonthViews(getActivity(), Next_month, Previous_month);
        }
        animateMonthViews.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            animateMonthViews.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Error_Dialog != null) {
            Error_Dialog.dismiss();
            Error_Dialog = null;
        }

        if (Progress_Dialog != null) {
            Progress_Dialog.dismiss();
            Progress_Dialog = null;
        }
        unbinder.unbind();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        /*View v = (View) view.findViewById(R.id.NOT_G_Snapp);
        String x = Integer.toString(v.getWidth());*/
    }


}
