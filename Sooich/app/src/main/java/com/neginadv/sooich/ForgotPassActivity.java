package com.neginadv.sooich;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.nfc.Tag;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.iid.InstanceID;
import com.neginadv.sooich.Shp.LoginSessionManager;
import com.neginadv.sooich.classes.AnimateSlideForgotPass;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.StartStopRemoteView;
import com.neginadv.sooich.classes.ToastCustom;
import com.neginadv.sooich.utils.PracticalClasses;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPassActivity extends AppCompatActivity {

    @BindView(R.id.FPASS_SendNumber_RL)
    RelativeLayout Num_Rl;
    @BindView(R.id.FPASS_SendCode_RL)
    RelativeLayout Code_Rl;
    @BindView(R.id.FPASS_SendNewPass_RL)
    RelativeLayout Pass_Rl;

    @BindView(R.id.FPASS_Send_btn)
    Button Send_Num_Btn;
    @BindView(R.id.FPASS_SendCode_btn)
    Button Send_Code_Btn;
    @BindView(R.id.verify_sendAgain)
    Button SendVerifyAgain;
    @BindView(R.id.FPASS_EditPhone_btn)
    Button EditPhoneBtn;
    @BindView(R.id.FPASS_SendNewPass_btn)
    Button Send_Pass;

    @BindView(R.id.FPASS_Number_Edt)
    EditText PhoneNum_Edt;
    @BindView(R.id.FPASS_Code_Edt)
    EditText VerifyCode_Edt;
    @BindView(R.id.FPASS_NewPass_Edt)
    EditText Pass_Edt;
    @BindView(R.id.FPASS_ReNewPass_Edt)
    EditText RePass_Edt;

    @BindView(R.id.FPASS_Phone_progress)
    ProgressBar Progress_Phone;
    @BindView(R.id.FPASS_Code_progress)
    ProgressBar Progress_Code;
    @BindView(R.id.FPASS_NewPass_progress)
    ProgressBar Progress_Pass;

    @BindView(R.id.FPASS_Code_ViewNumber)
    TextView NumView;
    @BindView(R.id.verify_Timer)
    TextView Timer_Txt;

    AnimateSlideForgotPass Animate;
    String Temp_Phone = null;
    PracticalClasses PC;
    LoginSessionManager session;
    int Status = 0;
    String PhoneNumber = null;

    private static final String TAG = ForgotPassActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);

        ini();

    }

    private void ini() {

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ButterKnife.bind(this);
        Animate = new AnimateSlideForgotPass(ForgotPassActivity.this);
        PC = new PracticalClasses();
        session = new LoginSessionManager(ForgotPassActivity.this);

    }

    @OnClick(R.id.FPASS_Send_btn)
    public void SendPhoneNumber(View v) {

        VerifyCode_Edt.setText("");

        PhoneNumber = PhoneNum_Edt.getText().toString();
        if (PhoneNumber.isEmpty()) {
            ToastCustom.viewToast(ForgotPassActivity.this, getResources().getString(R.string.login_Error_Mobile2));
        } else {
            if (PhoneNumber.length() < 11) {
                ToastCustom.viewToast(ForgotPassActivity.this, getResources().getString(R.string.login_Error_Mobile));
            } else if (!PhoneNumber.substring(0, 2).equals("09")) {
                ToastCustom.viewToast(ForgotPassActivity.this, getResources().getString(R.string.login_Error_Mobile));
            } else {

                PC.hideKeyboard(ForgotPassActivity.this, v);
                SendPhone(PhoneNumber, true);

            }
        }

    }

    @OnClick(R.id.FPASS_SendCode_btn)
    public void SendCode(View v) {
        String Code = VerifyCode_Edt.getText().toString().trim();

        if (Code.length() < 5) {
            ToastCustom.viewToast(ForgotPassActivity.this, getResources().getString(R.string.ERR_DialogVerifyForget_InsertCOde));
        } else {
            PC.hideKeyboard(ForgotPassActivity.this, v);
            SendVerifyCode(Temp_Phone.trim(), Code);
        }
    }

    @OnClick(R.id.FPASS_SendNewPass_btn)
    public void SendPass(View v) {
        String Password = Pass_Edt.getText().toString().trim();
        String RePassword = RePass_Edt.getText().toString().trim();

        if (Password.isEmpty() || RePassword.isEmpty()) {
            ToastCustom.viewToast(ForgotPassActivity.this, getResources().getString(R.string.FPASS_PassNull));
        } else {
            if (!Password.equals(RePassword)) {
                ToastCustom.viewToast(ForgotPassActivity.this, getResources().getString(R.string.login_Inputs_Not_Equal));
            } else {
                PC.hideKeyboard(ForgotPassActivity.this, v);
                SendNewPass(Temp_Phone, Password);
            }
        }
    }

    @OnClick(R.id.FPASS_EditPhone_btn)
    public void EditPhone(View v) {
        PC.hideKeyboard(ForgotPassActivity.this, v);
        Animate.Start(Num_Rl, Code_Rl, true);
    }

    @OnClick(R.id.verify_sendAgain)
    public void Retry_Code(View v) {
        PC.hideKeyboard(ForgotPassActivity.this, v);
        timer();
        NumView.setText(getResources().getString(R.string.FPASS_ReNumView) + " " + Temp_Phone + " " + getResources().getString(R.string.FPASS_NumView2));
        SendPhone(PhoneNumber, false);
    }

    @Override
    public void onBackPressed() {

        if (Status == 0) {
            Intent intent = new Intent(ForgotPassActivity.this, LoginAndReg.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);

        } else if (Status == 1) {
            Animate.Start(Num_Rl, Code_Rl, true);
            Status = 0;
            PhoneNumber = null;
        } else if (Status == 2) {
            Animate.Start(Num_Rl, Pass_Rl, true);
            Status = 0;
            PhoneNumber = null;
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void SendPhone(final String Phone_Num, final boolean Action) {

        JSONObject Obj = new JSONObject();
        try {
            Obj.put("mobile_no", Phone_Num);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (Action) {
            //send First Phone
            Progress_Phone.setVisibility(View.VISIBLE);
            Send_Num_Btn.setEnabled(false);
            Temp_Phone = null;
        }
        //Else Send Again


        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.Forgot_Pass, Obj, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {

                if (Action) {
                    //send First Phone
                    Progress_Phone.setVisibility(View.INVISIBLE);
                    Send_Num_Btn.setEnabled(true);
                }

                try {
                    if (response.get("status").equals("ok")) {

                        if (Action) {
                            //send First Phone

                            Log.e(TAG, "all thing ok");
                            Temp_Phone = Phone_Num;

                            Animate.Start(Num_Rl, Code_Rl, false);
                            Status = 1;

                            NumView.setText(getResources().getString(R.string.FPASS_NumView) + " " + Temp_Phone + " " + getResources().getString(R.string.FPASS_NumView2));
                            timer();
                        }

                    } else if (response.get("status").equals("error")) {
                        ToastCustom.viewToast(ForgotPassActivity.this, response.get("message") + "");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (Action) {
                    //send First Phone
                    Progress_Phone.setVisibility(View.INVISIBLE);
                    Send_Num_Btn.setEnabled(true);
                    new VolleyErrorHelper().getMessage(error, ForgotPassActivity.this, null, null);
                }

            }
        });

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String SendPhone = "Forgot Pass PhoneNum";
        RequestController.getInstance().addToRequestQueue(strReq, SendPhone);
    }

    public void SendVerifyCode(final String Phone_Num, final String Code) {

        JSONObject Obj = new JSONObject();
        try {
            Obj.put("user_name", Phone_Num);
            Obj.put("user_pass", Code);
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        Log.e(TAG , Phone_Num + "  -  " + Code);

        Progress_Code.setVisibility(View.VISIBLE);
        Send_Code_Btn.setEnabled(false);
        EditPhoneBtn.setVisibility(View.INVISIBLE);
//        Temp_Phone = null;

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.sms_verifi, Obj, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {
                Progress_Code.setVisibility(View.INVISIBLE);
                Send_Code_Btn.setEnabled(true);


                Log.e("volley res:", response.toString());

                try {
                    if (response.get("status").equals("ok")) {

                        Animate.Start(Code_Rl, Pass_Rl, false);
                        Status = 2;

                    } else if (response.get("status").equals("error")) {
//                        toast.viewToast(ForgotPassActivity.this, response.get("message") + "");
                        ToastCustom.viewToast(ForgotPassActivity.this, "check server error");
                        EditPhoneBtn.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Progress_Code.setVisibility(View.INVISIBLE);
                Send_Code_Btn.setEnabled(true);
                EditPhoneBtn.setVisibility(View.VISIBLE);

                new VolleyErrorHelper().getMessage(error, ForgotPassActivity.this, null, null);

            }
        });

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String SendPhone = "Forgot Pass Code";
        RequestController.getInstance().addToRequestQueue(strReq, SendPhone);
    }

    public void SendNewPass(final String Phone_Num, final String Pass) {

        JSONObject Obj = new JSONObject();
        try {
            Obj.put("mobile_no", Phone_Num);
            Obj.put("user_pass", Pass);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Progress_Pass.setVisibility(View.VISIBLE);
        Send_Pass.setEnabled(false);

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.PUT,
                ServiceApi.Forgot_Pass, Obj, new Response.Listener<JSONObject>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(JSONObject response) {
                Progress_Pass.setVisibility(View.INVISIBLE);
                Send_Pass.setEnabled(true);

                try {
                    if (response.get("status").equals("ok")) {

                        session.SaveUserData("", "", "");

                        Intent intent = new Intent(ForgotPassActivity.this, LoginAndReg.class);
                        startActivity(intent);

                        ToastCustom.viewToast(ForgotPassActivity.this, getResources().getString(R.string.FPASS_Change_Pass_Successfuly));

                    } else if (response.get("status").equals("error")) {

                        ToastCustom.viewToast(ForgotPassActivity.this, getResources().getString(R.string.FPASS_Error));

                        Pass_Edt.setText("");
                        RePass_Edt.setText("");

                        Animate.Start(Pass_Rl, Num_Rl, false);
                        Status = 2;

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Progress_Pass.setVisibility(View.INVISIBLE);
                Send_Pass.setEnabled(true);

                new VolleyErrorHelper().getMessage(error, ForgotPassActivity.this, null, null);

            }
        });

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String SendPhone = "Forgot Pass Inputs";
        RequestController.getInstance().addToRequestQueue(strReq, SendPhone);
    }


    private void timer() {

        SendVerifyAgain.setVisibility(View.INVISIBLE);
        Timer_Txt.setVisibility(View.VISIBLE);

//        120000

        final CountDownTimer Timer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                String MS = ("" + String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
                Timer_Txt.setText(MS);
            }

            public void onFinish() {

                /*false == btn sho & timer hide
                true == textview must be enable and btn&timer must be hidden*/

               /* if (!action) {
                    //countdownTimerText.setText("TIME'S UP!!"); //On finish change timer text


                } else if (action) {

                    Timer_Txt.setVisibility(View.GONE);
                    SendVerifyAgain.setVisibility(View.VISIBLE);
                }*/

                Timer_Txt.setVisibility(View.INVISIBLE);
                SendVerifyAgain.setVisibility(View.VISIBLE);

            }
        }.start();
    }


}
