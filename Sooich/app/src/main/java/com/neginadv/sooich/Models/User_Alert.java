package com.neginadv.sooich.Models;

/**
 * Created by EhsanYazdanyar on 5/14/2018.
 */

public class User_Alert {

    private String title;
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
