package com.neginadv.sooich.Models;

/**
 * Created by EhsanYazdanyar on 5/15/2018.
 */

public class State {

    private int id;
    private String Fa_Name;
    private String En_Name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFa_Name() {
        return Fa_Name;
    }

    public void setFa_Name(String fa_Name) {
        Fa_Name = fa_Name;
    }

    public String getEn_Name() {
        return En_Name;
    }

    public void setEn_Name(String en_Name) {
        En_Name = en_Name;
    }


}
