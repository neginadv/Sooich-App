package com.neginadv.sooich.Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.VolleyErrorHelper;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.setCamaToNumber;
import com.neginadv.sooich.helper.DatabaseHelper;
import com.neginadv.sooich.Shp.UserProfileManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by EhsanYazdanyar on 18/05/2018.
 */

public class Dialog_Verify_Travel {

    private static AlertDialog.Builder builder;
    private static View v;
    private ProgressBar progress;
    private AlertDialog d;
    private String Token = "";
    private int Sabt_Safar = 0;
    private Button Send_Data , Close;
    private TextView View_Data , TXT_Successfully;
    private DatabaseHelper data;
    private ConvertNumbers_Fa_En ConvertNum;
    private UserProfileManager userProfileManager;

    private static final String TAG = "Dialog Verify Travel";

    public Dialog_Verify_Travel(final Activity activity , final View B_Black , List<String> Data_List){


        builder = new AlertDialog.Builder(activity, R.style.Material_Dialog);
        builder.setCancelable(false);

        LayoutInflater inflater = LayoutInflater.from(activity);
        final View view = inflater.inflate(R.layout.dialog_verify_travel, null);
        ini(activity, view);


        builder.setView(view);
        v = view;

        final String Taxi_EnName = Data_List.get(0);
        String Taxi_FaName = null;
        if (Taxi_EnName !=null){
            Taxi_FaName = data.GetTaxiCoByName(Taxi_EnName , "EN");
        }

        final String Payment_En = Data_List.get(1);
        String Payment_Fa = null;
        if (Payment_En !=null){
            if (Payment_En.equals("Online")){
                Payment_Fa = activity.getResources().getString(R.string.NewTravel_Payment_Online_Fa);
            }else{
                Payment_Fa = activity.getResources().getString(R.string.NewTravel_Payment_Cash_Fa);
            }
        }

        final String Start_Time = Data_List.get(2);
        String Start_Time_Fa = null;
        if (Start_Time !=null){
            Start_Time_Fa = ConvertNum.EN_TO_FA(Start_Time);
        }

        final String Priod_Time = Data_List.get(3);
        String Priod_Time_Fa = null;
        if (Priod_Time !=null){
            Priod_Time_Fa = ConvertNum.EN_TO_FA(Priod_Time);
        }

        String Kilometer = Data_List.get(4);
        String Kilometer_Fa = null;
        String Kilometer_En = null;
        if (Kilometer !=null){
            Kilometer_Fa = Kilometer;
            Kilometer_En = ConvertNum.FA_TO_EN(Kilometer_Fa);
        }

        final String Cash = Data_List.get(5);
        String Cash_Fa = null;
        if (Cash !=null){
            Cash_Fa = ConvertNum.EN_TO_FA(new setCamaToNumber().Edit(Cash));
        }


        View_Data.append(activity.getResources().getString(R.string.Taxi_Co) + " : " + Taxi_FaName);
        View_Data.append("\n");
        View_Data.append(activity.getResources().getString(R.string.Pardakht) + " : " + Payment_Fa);
        View_Data.append("\n");
        View_Data.append(activity.getResources().getString(R.string.NewTravel_StartTime) + " : " + Start_Time_Fa);
        View_Data.append("\n");
        View_Data.append(activity.getResources().getString(R.string.NewTravel_PriodTime) + " : " + Priod_Time_Fa);
        View_Data.append("\n");
        View_Data.append(activity.getResources().getString(R.string.NewTravel_kilometer) + " : " + Kilometer_Fa + " " + activity.getResources().getString(R.string.Kilometer));
        View_Data.append("\n");
        View_Data.append(activity.getResources().getString(R.string.NewTravel_cash) + " : " + Cash_Fa + " " + activity.getResources().getString(R.string.Rial));

        Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
                B_Black.setVisibility(View.INVISIBLE);

                if (Sabt_Safar == 1){
                    activity.recreate();
                }
            }
        });

        final String finalKilometer_En = Kilometer_En;
        Send_Data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.e(TAG , "taxi_co :" + Taxi_EnName);
//                Log.e(TAG , "payment_method :" + Payment_En);
//                Log.e(TAG , "hour_in :" + Start_Time);
//                Log.e(TAG , "time_period :" + Priod_Time);
//                Log.e(TAG , "distance :" + finalKilometer_En);
//                Log.e(TAG , "count :" + Cash);

                Log.e(TAG , "Send Data Started");

                JSONObject Obj = new JSONObject();
                try {
                    Obj.put("taxi_co", Taxi_EnName);
                    Obj.put("payment_method", Payment_En);
                    Obj.put("hour_in", Start_Time);
                    Obj.put("time_period", Priod_Time);
                    Obj.put("distance", finalKilometer_En);
                    Obj.put("count", Cash);
                    Obj.put("description", "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Submit_Travel(activity , Obj);

            }
        });

        B_Black.setVisibility(View.VISIBLE);
        d = builder.show();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void ini(Activity activity, View view) {
//        Token = new GetSharedPreferencesData(activity).GetLoginData();
        data = new DatabaseHelper(activity);
        Send_Data = view.findViewById(R.id.DVT_Send_Data);
        progress = view.findViewById(R.id.DVT_dialog_progress);
        View_Data = (TextView) view.findViewById(R.id.DVT_dialog_Txt);
        TXT_Successfully = (TextView) view.findViewById(R.id.DVT_TXT_Successfully);
        Close = (Button) view.findViewById(R.id.DVT_dialog_close_Btn);
        ConvertNum = new ConvertNumbers_Fa_En();
        userProfileManager = new UserProfileManager(activity);

    }

    public void Submit_Travel(final Activity activity, JSONObject object){

        progress.setVisibility(View.VISIBLE);
        TXT_Successfully.setVisibility(View.INVISIBLE);
        Send_Data.setEnabled(false);
        Close.setEnabled(false);

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.New_Travel, object, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                progress.setVisibility(View.INVISIBLE);
                Send_Data.setEnabled(true);
                Close.setEnabled(true);

                try {

                    if (response.getString("status").equals("ok")){
                        Send_Data.setVisibility(View.GONE);
                        progress.setVisibility(View.GONE);
                        TXT_Successfully.setVisibility(View.VISIBLE);
                        TXT_Successfully.setText(activity.getResources().getString(R.string.NewTravel_Alert_Txt));

                        Sabt_Safar = 1;

                        userProfileManager.SetUpdated();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e(TAG , "response:  " + String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.INVISIBLE);
                Send_Data.setEnabled(true);
                Close.setEnabled(true);

                new VolleyErrorHelper().getMessage(error , activity , null , null);

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", Token);
                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Reg_Tag = "Submit_Travel_Tag";
        RequestController.getInstance().addToRequestQueue(strReq , Reg_Tag);

    }



}
