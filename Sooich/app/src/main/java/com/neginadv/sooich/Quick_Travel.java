package com.neginadv.sooich;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.Dialogs.timePickerDialog;
import com.neginadv.sooich.Models.Taxi_Co;
import com.neginadv.sooich.Shp.LoginSessionManager;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.StartStopRemoteView;
import com.neginadv.sooich.classes.ToastCustom;
import com.neginadv.sooich.helper.DatabaseHelper;
import com.neginadv.sooich.Shp.UserProfileManager;
import com.neginadv.sooich.utils.PracticalClasses;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by EhsanYazdanyar on 10/07/2018.
 */

public class Quick_Travel extends AppCompatActivity {

    @BindView(R.id.QRS_taxiName_spinner)
    Spinner Taxi_Spin;
    @BindView(R.id.QRS_Payment_spinner)
    Spinner Payment_Spin;

    @BindView(R.id.QRS_Submit_Travel)
    Button Submit_Btn;
    @BindView(R.id.QRS_Cancel_Travel)
    Button Cancel_btn;

    @BindView(R.id.QRS_StartTime_TxtView)
    TextView StartTime;
    @BindView(R.id.QRS_TimeTravel_TxtView)
    TextView TimeDuration;

    @BindView(R.id.QRS_Distance_Edt)
    EditText Distance_Edt;
    @BindView(R.id.QRS_Cash_Edt)
    EditText Cash_Edt;

    @BindView(R.id.QRS_progress)
    ProgressBar progressBar;

    Unbinder unbinder;
    private ToastCustom toast;
    private String Token = "";
    private DatabaseHelper data;
    int Hour, Min;

    private PracticalClasses PC;
    private SharedPreferences Shp_Travel;
    private SharedPreferences.Editor Travel_Editor;
    private UserProfileManager userProfileManager;

    LoginSessionManager LoginSession;

    private static final String TAG = "Quick Travel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quick_travel_system);

        ini();

        TextView location_txt = findViewById(R.id.QT_txt);

        String First_Lat = Shp_Travel.getString("F_LocLat", null);
        String First_Long = Shp_Travel.getString("F_LocLong", null);
        String End_Lat = Shp_Travel.getString("E_LocLat", null);
        String End_Long = Shp_Travel.getString("E_LocLong", null);

        location_txt.append("First Location : " + First_Lat + "," + First_Long);
        location_txt.append("\nEnd Location : " + End_Lat + "," + End_Long);


        if (First_Lat != null && End_Lat != null) {

            Location location1 = new Location("");
            location1.setLatitude(Double.valueOf(First_Lat));
            location1.setLongitude(Double.valueOf(First_Long));

            Location location2 = new Location("");

            location2.setLatitude(Double.valueOf(End_Lat));
            location2.setLongitude(Double.valueOf(End_Long));

            int Final_Kilometer;

            final double dis = location1.distanceTo(location2);

            final double Distance_kilo = dis / 1000;

            final int Int_Of_Distance_kilo = (int) Distance_kilo;

            final double decimal = Distance_kilo - Int_Of_Distance_kilo;

            location_txt.append("\nDistance : " + Distance_kilo);

            if (decimal >= 0.5) {
                Final_Kilometer = Int_Of_Distance_kilo + 1;
            } else {
                Final_Kilometer = Int_Of_Distance_kilo;
            }

            Distance_Edt.setText(String.valueOf(Final_Kilometer));

        } else {
            location_txt.append("\nDistance : is null");
        }

        location_txt.append("\nLast Known Location : " + Shp_Travel.getString("TempLat", null));

        PC.SetCommaToNumbers(Cash_Edt);


        ArrayList<Taxi_Co> arrayList = data.GetAllTaxiCo();
        List Taxico_List = new ArrayList();
        if (arrayList.size() > 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                Taxi_Co Taxis = arrayList.get(i);
                Taxico_List.add(Taxis.getFa_name());
            }

            PC.setSpinnerToAdapter(Taxi_Spin, Taxico_List, Quick_Travel.this);
        } else {
            Log.e(TAG, "number Of Row (Taxi_Co) is : " + arrayList.size());
        }

        List<String> Payment_Array = Arrays.asList(getResources().getStringArray(R.array.NewTravel_Payment_array));
        PC.setSpinnerToAdapter(Payment_Spin, Payment_Array, Quick_Travel.this);


    }

    private void setTimes() {

        int Travel_Status = Shp_Travel.getInt("Status", 0);

        if (Travel_Status == 1) {
            Log.e(TAG, "Travel Started");
            String Started_Time = Shp_Travel.getString("StartTime", "");
            StartTime.setText(Started_Time);

            try {
                Date time1 = new SimpleDateFormat("HH:mm").parse(Started_Time);
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(time1);

                SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
                Date today = new Date();
                String Now_Time = formatter.format(today);
                Date time2 = new SimpleDateFormat("HH:mm").parse(Now_Time);
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(time2);

                Date Time1 = calendar1.getTime();
                Date Time2 = calendar2.getTime();

                long diff = Time2.getTime() - Time1.getTime();
                long diffmin = diff / (60 * 1000);
                long diff_hours = diffmin / 60;

                Min = (int) diffmin;
                Hour = (int) diff_hours;

                while (Min >= 60) {
                    Min = Min - (Hour * 60);
                }

                String Str_DiffMin = String.valueOf(Min);
                String Str_DiffHour = String.valueOf(Hour);

                if (Str_DiffMin.trim().length() < 2) {
                    Str_DiffMin = "0" + Str_DiffMin;

                }

                if (Str_DiffHour.trim().length() < 2) {
                    Str_DiffHour = "0" + Str_DiffHour;
                }

                String TimeDurationTxt = Str_DiffHour + ":" + Str_DiffMin;
                TimeDuration.setText(TimeDurationTxt);

            } catch (ParseException e) {
                e.printStackTrace();

            }

        } else {
            Log.e(TAG, "Travel Not Started");
        }
    }

    private void ini() {

        unbinder = ButterKnife.bind(this);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        data = new DatabaseHelper(Quick_Travel.this);

        LoginSession = new LoginSessionManager(this);
        Token = LoginSession.GetUserToken();

        Shp_Travel = getSharedPreferences("Short_Travel", MODE_PRIVATE);
        Travel_Editor = Shp_Travel.edit();
        userProfileManager = new UserProfileManager(this);
        toast = new ToastCustom();
        PC = new PracticalClasses();

        setTimes();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @OnClick(R.id.QRS_Cancel_Travel)
    public void Cancel_Travel() {
        new StartStopRemoteView().ResetService(Quick_Travel.this);

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @OnClick(R.id.QRS_Submit_Travel)
    public void Submit_Travel(View v) {

        String Payment_Method = Payment_Spin.getSelectedItem().toString().trim();
        if (Payment_Method.equals(getString(R.string.NewTravel_Payment_Cash_Fa))) {
            Payment_Method = "Cash";
        } else if (Payment_Method.equals(getString(R.string.NewTravel_Payment_Online_Fa))) {
            Payment_Method = "Online";
        }

        String Taxi_Name = Taxi_Spin.getSelectedItem().toString().trim();
        if (!Taxi_Name.equals("")) {
            Taxi_Name = data.GetTaxiCoByName(Taxi_Name, "FA");
        }

        String distance = Distance_Edt.getText().toString().trim();
        String Cash = Cash_Edt.getText().toString().trim();

        if (distance.equals("")) {
            toast.viewToast(Quick_Travel.this, getResources().getString(R.string.Insert_Distance));
        } else {
            if (Cash.equals("")) {
                toast.viewToast(Quick_Travel.this, getResources().getString(R.string.New_Travel_Insert_Fare));
            } else {

                PC.hideKeyboard(Quick_Travel.this, v);

                Log.e(TAG, "Taxi co : " + Taxi_Name);
                Log.e(TAG, "Payment : " + Payment_Method);
                Log.e(TAG, "Start Time : " + StartTime.getText().toString().trim());
                Log.e(TAG, "Time Duration : " + TimeDuration.getText().toString().trim());
                Log.e(TAG, "Distance : " + distance);
                Log.e(TAG, "Cash : " + Cash.replace(",", ""));

                JSONObject Obj = new JSONObject();

                try {
                    Obj.put("taxi_co", Taxi_Name);
                    Obj.put("payment_method", Payment_Method);
                    Obj.put("hour_in", StartTime.getText().toString().trim());
                    Obj.put("time_period", TimeDuration.getText().toString().trim());
                    Obj.put("distance", distance);
                    Obj.put("count", Cash.replace(",", ""));
                    Obj.put("description", "");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Submit_Travel(Obj);

            }
        }
    }

    public void Submit_Travel(JSONObject object) {

        progressBar.setVisibility(View.VISIBLE);
        Submit_Btn.setEnabled(false);
        Cancel_btn.setEnabled(false);

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.New_Travel, object, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                progressBar.setVisibility(View.INVISIBLE);
                Submit_Btn.setEnabled(true);
                Cancel_btn.setEnabled(true);

                try {

                    if (response.getString("status").equals("ok")) {

                        toast.viewToast(Quick_Travel.this, getResources().getString(R.string.NewTravel_Alert_Txt));

                        userProfileManager.SetUpdated();

                        new StartStopRemoteView().ResetService(Quick_Travel.this);

                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e(TAG, "response:  " + String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);
                Submit_Btn.setEnabled(true);
                Cancel_btn.setEnabled(true);

                new VolleyErrorHelper().getMessage(error, Quick_Travel.this, null, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", Token);
                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Reg_Tag = "Submit_Travel_Tag";
        RequestController.getInstance().addToRequestQueue(strReq, Reg_Tag);

    }

    @OnClick(R.id.QRS_Edit_Duration_Btn)
    public void Edit_DurationTravel() {
        timePickerDialog.TimePick(Quick_Travel.this, Hour, Min, TimeDuration, null);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "On Resume Started");
        new StartStopRemoteView().StopService(Quick_Travel.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        new StartStopRemoteView().startService(Quick_Travel.this);
        finish();

    }


}
