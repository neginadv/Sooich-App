package com.neginadv.sooich.Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.CompleteRegisterActivity;
import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.VolleyErrorHelper;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.ToastCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by EhsanYazdanyar on 3/10/2018.
 */

public class DialogVerifyForget extends AlertDialog {

    @BindView(R.id.dialog_edt_code)
    EditText edt_verifyCode;
    @BindView(R.id.verify_Timer)
    TextView Timer_Txt;
    @BindView(R.id.verify_sendAgain)
    Button SendAgain;
    @BindView(R.id.verify_send)
    Button SendCode_Btn;
    @BindView(R.id.dialog_close)
    Button Close;
    @BindView(R.id.dialog_progress)
    ProgressBar progress;

    private AlertDialog d;
    private TextView verify_txt1;
    private ToastCustom toast;

    private Unbinder unbinder;

    public DialogVerifyForget(final Activity activity, final String username, final String pass) {
        super(activity);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.Theme_AppCompat_Dialog);
        builder.setCancelable(false);

        LayoutInflater inflater = LayoutInflater.from(activity);
        final View view = inflater.inflate(R.layout.dialog_verifi, null);
        builder.setView(view);
        unbinder = ButterKnife.bind(this , view);

        toast = new ToastCustom();

        timer(Timer_Txt, view, false, verify_txt1);

        SendAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toast.viewToast(activity, activity.getResources().getString(R.string.ERR_DialogVerifyForget_StopRetry));

            }
        });

        SendCode_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConnectivityManager conMgr = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
                if (activeNetwork != null && activeNetwork.isConnected()) {

                    String verify_code = edt_verifyCode.getText().toString();

                    if (verify_code.length() < 5) {
                        toast.viewToast(activity, activity.getResources().getString(R.string.ERR_DialogVerifyForget_InsertCOde));
                    } else {
                        sendVerify(activity, pass, username, verify_code);
                    }
                } else {
                    toast.viewToast(activity, activity.getResources().getString(R.string.ERR_DialogVerifyForget_NoConnection));
                }

            }
        });

        d = builder.show();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
                unbinder.unbind();
            }
        });

    }

    private void sendVerify(final Activity activity, final String password, final String mobile, final String code) {

        JSONObject Obj = new JSONObject();
        try {
            Obj.put("user_name", mobile);
            Obj.put("user_code", code);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        progress.setVisibility(View.VISIBLE);
        SendCode_Btn.setEnabled(false);

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.sms_verifi, Obj, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                progress.setVisibility(View.INVISIBLE);
                SendCode_Btn.setEnabled(true);

                try {
                    if (response.getString("status").equals("ok")) {

                        Intent intent = new Intent(activity, CompleteRegisterActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.putExtra("user_name", mobile);
                        intent.putExtra("user_pass", password);
                        intent.putExtra("Action", "VERIFY");
                        activity.startActivity(intent);
                        activity.finish();
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////go to main page

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("response: ", String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progress.setVisibility(View.INVISIBLE);
                SendCode_Btn.setEnabled(true);

                new VolleyErrorHelper().getMessage(error, activity, null, null);

                //Log.e("onErrorResponse: ", error.getMessage() );

            }
        });

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Reg_Tag = "sendVerify_Tag";
        RequestController.getInstance().addToRequestQueue(strReq, Reg_Tag);

    }

    private void timer(final TextView verifyTimer, final View view, final Boolean action, final TextView textView) {

        if (action) {
            SendAgain.setVisibility(View.GONE);
            Timer_Txt.setVisibility(View.VISIBLE);
        }

        final CountDownTimer Timer = new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                long millis = millisUntilFinished;
                //Convert milliseconds into hour,minute and seconds
                String hms = ("" + String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millis), TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))));
                verifyTimer.setText(hms);//set text
            }

            public void onFinish() {

                /*false == btn sho & timer hide
                true == textview must be enable and btn&timer must be hidden*/

                if (!action) {
                    //countdownTimerText.setText("TIME'S UP!!"); //On finish change timer text

                    Timer_Txt.setVisibility(View.GONE);
                    SendAgain.setVisibility(View.VISIBLE);
                } else if (action) {

                    textView.setText(R.string.dialog_verifyforget_txt1);
                    textView.setVisibility(View.VISIBLE);
                    Timer_Txt.setVisibility(View.GONE);

                }

            }
        }.start();
    }


}