package com.neginadv.sooich.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.Dialogs.Msg_Dialog2;
import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.VolleyErrorHelper;
import com.neginadv.sooich.classes.AnimateMonthViews;
import com.neginadv.sooich.classes.BaseBackPressedListener;
import com.neginadv.sooich.classes.GetMonthByIndex;
import com.neginadv.sooich.classes.HandleNextAndPrevMonth;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.Shp.LoginSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by EhsanYazdanyar on 4/11/2018.
 */

public class SuperiorDaysRide extends Fragment {

    @BindView(R.id.SDR_Success_Cont)
    RelativeLayout Success_Cont;

    @BindView(R.id.SDR_month_view)
    TextView Month_View;
    @BindView(R.id.SDR_Year)
    TextView year;
    @BindView(R.id.SDR_Show_Day)
    TextView Best_Day;
    @BindView(R.id.SDR_Show_Least)
    TextView Worst_Day;
    @BindView(R.id.SDR_Average_Travel_Txt)
    TextView Average_Travel;

    @BindView(R.id.SDR_arrow_month_left)
    View Previous_month;
    @BindView(R.id.SDR_arrow_month_right)
    View Next_month;

    Unbinder unbinder;
    private AnimateMonthViews animateMonthViews;
    int Month = 0;
    private String Token = "";
    private ConvertNumbers_Fa_En Num_Converter;

    boolean FirstLunch = true;
    private Msg_Dialog2 Error_Dialog, Progress_Dialog;

    private static final String TAG = "Superior Days Ride";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_superior_days_ride, container, false);
        ini(view);

        Request_SuperiorDaysRide(getActivity(), null);

        return view;
    }

    private void ini(View view) {

        ManageOnBackPressed();
        unbinder = ButterKnife.bind(this, view);
        Num_Converter = new ConvertNumbers_Fa_En();
        Token = new LoginSessionManager(getActivity()).GetUserToken();

    }

    @OnClick({R.id.SDR_btn_month_right, R.id.SDR_btn_month_left})
    public void Next_PrevMonth(Button btn) {

        Progress(false);

        if (btn.getId() == R.id.SDR_btn_month_right) {
            Request_SuperiorDaysRide(getActivity(), "N");
        } else {
            Request_SuperiorDaysRide(getActivity(), "P");
        }

    }

    @OnClick(R.id.SDR_back_btn)
    public void GoBack() {
        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.putExtra("G_Omomi_Main", "G_Omomi_Main");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void Clear_All_Views() {

        Best_Day.setText("");
        Worst_Day.setText("");
        Average_Travel.setText("");

    }

    public void Request_SuperiorDaysRide(final Activity activity, String action) {

        String New_M = new HandleNextAndPrevMonth().Action(action, Month);

        JSONObject object = new JSONObject();

        try {
            object.put("month", New_M);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Progress(true);

        JsonObjectRequest ObjReq = new JsonObjectRequest(Request.Method.POST, ServiceApi.SuperiorDaysRide, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Progress(false);
                Success_Cont.setVisibility(View.VISIBLE);
                Clear_All_Views();

                FirstLunch = false;

                try {

                    String all_trip_no = response.getString("all_trip_no");

                    if (all_trip_no.equals("0")) {
                        Best_Day.setText(getResources().getString(R.string.BHR_No_Result));
                        Worst_Day.setText(getResources().getString(R.string.BHR_No_Result));
                        Average_Travel.setText(getResources().getString(R.string.BHR_No_Result));
                    } else {
                        String BestDay = GetWeekNameByIndex(Integer.valueOf(response.getString("best_day")));
                        String WorstDay = GetWeekNameByIndex(Integer.valueOf(response.getString("worst_day")));

                        Best_Day.setText(BestDay);
                        Worst_Day.setText(WorstDay);
                    }

                    String Year = response.getString("year");
                    Month = Integer.valueOf(response.getString("month"));

                    String MyMonth = new GetMonthByIndex().GetMonth(Month);
                    Month_View.setText(MyMonth);
                    //Log.e("month" , MyMonth);

                    year.setText(Num_Converter.EN_TO_FA(Year));
                    Average_Travel.setText(Num_Converter.EN_TO_FA(all_trip_no));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e(TAG, "response : " + response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Progress(false);

                if (FirstLunch) {
                    ErrorDialog();
                }

                new VolleyErrorHelper().getMessage(error, activity, null, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", Token);
                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        ObjReq.setRetryPolicy(policy);

        String Reg_Tag = "Request_SuperiorDaysRide_Tag";
        RequestController.getInstance().addToRequestQueue(ObjReq, Reg_Tag);

    }

    private void ErrorDialog() {
        Error_Dialog = new Msg_Dialog2(getActivity(), getString(R.string.ERR_RecieveData));
        Error_Dialog.HaveAttentionImage(true);
        Error_Dialog.cancel_Btn.setText(getString(R.string.Back));
        Error_Dialog.OK_Btn.setText(getString(R.string.Retry));

        Error_Dialog.SetOkDialogResult(new Msg_Dialog2.Msg_Ok_Btn() {
            @Override
            public void GetOkAction() {
                Error_Dialog.CloseDialog();
                Request_SuperiorDaysRide(getActivity(), null);
            }
        });

        Error_Dialog.SetCancelDialogResult(new Msg_Dialog2.MSG_Cancel_Btn() {
            @Override
            public void GetCancelAction() {
                GoBack();
            }
        });
    }

    private void Progress(boolean Enable) {

        if (Enable) {
            Progress_Dialog = new Msg_Dialog2(getActivity());
        } else {
            if (Progress_Dialog != null) {
                Progress_Dialog.CloseDialog();
                Progress_Dialog = null;
            }
        }

    }

    public String GetWeekNameByIndex(int Index) {

        String Week = "";

        switch (Index) {
            case 0:
                Week = "شنبه";
                break;
            case 1:
                Week = "یکشنبه";
                break;
            case 2:
                Week = "دوشنبه";
                break;
            case 3:
                Week = "سه شنبه";
                break;
            case 4:
                Week = "چهارشنبه";
                break;
            case 5:
                Week = "پنجشنبه";
                break;
            case 6:
                Week = "جمعه";
                break;
        }
        return Week;
    }

    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                GoBack();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (animateMonthViews == null) {
            animateMonthViews = new AnimateMonthViews(getActivity(), Next_month, Previous_month);
        }
        animateMonthViews.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            animateMonthViews.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Error_Dialog != null) {
            Error_Dialog.dismiss();
            Error_Dialog = null;
        }

        if (Progress_Dialog != null) {
            Progress_Dialog.dismiss();
            Progress_Dialog = null;
        }
        unbinder.unbind();
    }

}
