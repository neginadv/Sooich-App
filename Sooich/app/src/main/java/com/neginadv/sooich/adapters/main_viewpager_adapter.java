package com.neginadv.sooich.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.R;

/**
 * Created by EhsanYazdanyar on 3/6/2018.
 */

public class main_viewpager_adapter extends PagerAdapter {

    Context context;
    Activity activity;
    private boolean swipeable = true;

    private int[] GalImages = new int[]{
            R.drawable.m2,
            R.drawable.m1,
            R.drawable.m3,
            R.drawable.m4,
            R.drawable.m5,
            R.drawable.m6,
            R.drawable.m7,
    };

    public main_viewpager_adapter(Context context , Activity activity) {

        this.context = context;
        this.activity = activity;

    }

    @Override
    public int getCount() {
        return GalImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final ImageView imageView = new ImageView(context);
        int padding = context.getResources().getDimensionPixelSize(R.dimen.padding_medium);

        imageView.setPadding(120, 200, 120, 200);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        imageView.setImageResource(GalImages[position]);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("TAG", "This page was clicked: " + position);

                if (position == 0){
                    Intent intent = new Intent(activity , ActivityForFragments.class);
                    intent.putExtra("New_Travel" , "New_Travel");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    activity.startActivity(intent);
                }

                if (position == 1){
                    Intent intent = new Intent(activity , ActivityForFragments.class);
                    intent.putExtra("main" , "main");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    activity.startActivity(intent);
                }
            }
        });

        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN){

                    Log.e("toch action" , "Down");
                }else if (motionEvent.getAction() == MotionEvent.ACTION_UP){

                    Log.e("toch action" , "up");
                }

                return false;

            }
        });

        ((ViewPager) container).addView(imageView, 0);

        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }

}
