package com.neginadv.sooich.classes;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.neginadv.sooich.R;

/**
 * Created by EhsanYazdanyar on 4/22/2018.
 */

public class AnimateMonthViews {

    private Animation slide_right;
    private Animation slide_right_back;
    private Animation slide_left;
    private Animation slide_left_back;

    private View Next_month ;
    private View Previous_month ;
    private Handler Handler = new Handler();

    public AnimateMonthViews(Context c , View Next_month , View Previous_month){

        slide_right = AnimationUtils.loadAnimation(c, R.anim.slide_right);
        slide_right_back = AnimationUtils.loadAnimation(c, R.anim.slide_right_back);
        slide_left = AnimationUtils.loadAnimation(c, R.anim.slide_left);
        slide_left_back = AnimationUtils.loadAnimation(c, R.anim.slide_left_back);

        this.Next_month = Next_month;
        this.Previous_month = Previous_month;

    }

    public void start() {
        Handler.removeCallbacks(Update);
        Handler.post(Update);
    }

    public void stop() {
        Handler.removeCallbacks(Update);
    }

    private Runnable Update  = new Runnable() {
        public void run() {

            Next_month.startAnimation(slide_right);
            slide_right.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation arg0) {
                }

                @Override
                public void onAnimationRepeat(Animation arg0) {
                }

                @Override
                public void onAnimationEnd(Animation arg0) {
                    Next_month.startAnimation(slide_right_back);
                }
            });

            Previous_month.startAnimation(slide_left);
            slide_left.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation arg0) {
                }
                @Override
                public void onAnimationRepeat(Animation arg0) {
                }

                @Override
                public void onAnimationEnd(Animation arg0) {
                    Previous_month.startAnimation(slide_left_back);
                }
            });

            Handler.postDelayed(Update, 2000);

        }
    };



}
