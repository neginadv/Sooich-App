package com.neginadv.sooich.classes;

import android.content.res.Resources;
import android.util.DisplayMetrics;

/**
 * Created by EhsanYazdanyar on 4/24/2018.
 */

public class ConvertDpAndPx {

    public int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }


}
