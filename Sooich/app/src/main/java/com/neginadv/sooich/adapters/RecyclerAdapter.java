package com.neginadv.sooich.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.neginadv.sooich.Models.BestDriver;
import com.neginadv.sooich.R;

import java.util.List;

/**
 * Created by EhsanYazdanyar on 21/08/2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private List<BestDriver> BestDriver;
    public static ImageView BDL_Img_Rank;

    public RecyclerAdapter(List<BestDriver> bestDrivers) {
        this.BestDriver = bestDrivers;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.best_driver_custom_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        BestDriver BestDrivers = BestDriver.get(position);
        holder.BDL_Amount.setText(BestDrivers.getAmount());
        holder.BDL_Driver_Name.setText(BestDrivers.getName());
        holder.BDL_Num_Travel.setText(BestDrivers.getNum_Travel());

        int Rank = Integer.valueOf(BestDrivers.getRank());
        if (Rank == 1){
            BDL_Img_Rank.setVisibility(View.VISIBLE);
        }else{
            holder.BDL_Rank.setText(BestDrivers.getRank());
        }

    }

    @Override
    public int getItemCount() {
        return BestDriver.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView BDL_Amount, BDL_Num_Travel, BDL_Driver_Name, BDL_Rank;


        public ViewHolder(View view) {
            super(view);

            BDL_Amount = view.findViewById(R.id.BDL_Amount);
            BDL_Num_Travel = view.findViewById(R.id.BDL_Num_Travel);
            BDL_Driver_Name = view.findViewById(R.id.BDL_Driver_Name);
            BDL_Rank = view.findViewById(R.id.BDL_Rank);
            BDL_Img_Rank = view.findViewById(R.id.BDL_Img_Rank);

        }
    }

}
