package com.neginadv.sooich.classes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.neginadv.sooich.utils.Constants;
import com.neginadv.sooich.utils.NotificationService;

/**
 * Created by EhsanYazdanyar on 07/07/2018.
 */

public class StartStopRemoteView {

    public void StopService(Context activity) {
        Intent serviceIntent = new Intent(activity, NotificationService.class);
        serviceIntent.setAction(Constants.ACTION.STOPFOREGROUND_ACTION);
        activity.startService(serviceIntent);
    }

    public void startService(Context activity) {
        Intent serviceIntent = new Intent(activity, NotificationService.class);
        serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
        activity.startService(serviceIntent);
    }

    public void ResetService(Context activity) {
        Intent serviceIntent = new Intent(activity, NotificationService.class);
        serviceIntent.setAction(Constants.ACTION.N_ResetQuickTravel);
        activity.startService(serviceIntent);
    }

    public void ResetServiceWithoutLunch(Context activity) {
        Intent serviceIntent = new Intent(activity, NotificationService.class);
        serviceIntent.setAction(Constants.ACTION.N_ResetQuickWithoutLunch);
        activity.startService(serviceIntent);
    }

    public void GetLocation(Context activity) {
        Intent serviceIntent = new Intent(activity, NotificationService.class);
        serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
        activity.startService(serviceIntent);
    }

}
