package com.neginadv.sooich.classes;

import android.app.Activity;
import android.app.FragmentManager;
import android.support.v4.app.FragmentActivity;


/**
 * Created by EhsanYazdanyar on 4/23/2018.
 */

public class BaseBackPressedListener implements OnBackPressedListener {

    private final FragmentActivity activity;

    public BaseBackPressedListener(FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void doBack() {
        if (!activity.getSupportFragmentManager().popBackStackImmediate()){
            activity.supportFinishAfterTransition();
        }
    }

}
