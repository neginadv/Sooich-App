package com.neginadv.sooich.classes;

import android.app.Application;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.neginadv.sooich.R;
import com.neginadv.sooich.Shp.LoginSessionManager;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by EhsaYazdanyar on 02/06/2018.
 */

public class RequestController extends Application {

    public static final String TAG = RequestController.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private LoginSessionManager session;
    private ImageLoader mImageLoader;

    private static RequestController mInstance;

    private int Access_Granted = PackageManager.PERMISSION_GRANTED;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        session = new LoginSessionManager(getApplicationContext());

        if (session.isLoggedIn()) {
//            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != Access_Granted
//                    && ActivityCompat.checkSelfPermission(getApplicationContext() , Manifest.permission.ACCESS_COARSE_LOCATION) !=Access_Granted){
//
//                //new StartStopRemoteView().PermissionNeeded(getApplicationContext());
//
//            }else{
//
//            }
            new StartStopRemoteView().startService(getApplicationContext());
        }

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/iranyekanwebbold.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }

    public static synchronized RequestController getInstance(){
        return mInstance;
    }

    public RequestQueue getRequestQueue(){
        if (mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void  addToRequestQueue(Request<T> request , String tag){
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(request);
    }

    public <T> void  addToRequestQueue(Request<T> request){
        request.setTag(TAG);
        getRequestQueue().add(request);
    }

    public void cancelPendingRequest(Object tag){
        if (mRequestQueue !=null){
            mRequestQueue.cancelAll(tag);
        }
    }


}
