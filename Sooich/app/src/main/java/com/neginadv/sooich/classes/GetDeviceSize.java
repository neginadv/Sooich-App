package com.neginadv.sooich.classes;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

/**
 * Created by EhsanYazdanyar on 4/24/2018.
 */

public class GetDeviceSize {

    Context c;

    public GetDeviceSize(Context context){
        this.c = context;
    }

    private DisplayMetrics getsize(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        if (windowmanager != null) {
            windowmanager.getDefaultDisplay().getMetrics(displayMetrics);

        }
        return displayMetrics;
    }

    public int GetHeight(){

        int Height = 0;

        Height = Math.round(getsize().heightPixels / getsize().density);

        return Height;
    }

    public int GetWidth(){

        int Width = 0;

        Width = Math.round(getsize().widthPixels / getsize().density);

        return Width;
    }

    public Float getdestiny(){

        return getsize().density;
    }

}
