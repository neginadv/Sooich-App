package com.neginadv.sooich.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.neginadv.sooich.R;

/**
 * Created by EhsanYazdanyar on 02/07/2018.
 */

public class Constants {

    public interface ACTION {

        public static String MAIN_ACTION = "com.marothiatechs.customnotification.action.main";
        public static String INIT_ACTION = "com.marothiatechs.customnotification.action.init";
        public static String PREV_ACTION = "com.marothiatechs.customnotification.action.prev";
        public static String PLAY_ACTION = "com.marothiatechs.customnotification.action.play";
        public static String NEXT_ACTION = "com.marothiatechs.customnotification.action.next";

        public static String N_StartTravel = "Start_Travel";
        public static String N_EndTravel = "End_Travel";
        public static String N_CancelTravel = "Cancel_Travel";
        public static String N_ResetQuickTravel = "ResetQuick";
        public static String N_ResetQuickWithoutLunch = "ResetQuick2";
        public static String N_GetLocation = "GetLocation";
        public static String N_SkipLocationSearching = "SkipLocationSearching";
        public static String N_Permission_Req = "PermissionReq";

        public static String STARTFOREGROUND_ACTION = "com.marothiatechs.customnotification.action.startforeground";
        public static String STOPFOREGROUND_ACTION = "com.marothiatechs.customnotification.action.stopforeground";

    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }

    /*public static Bitmap getDefaultAlbumArt(Context context) {
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            bm = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.default_album_art, options);
        } catch (Error ee) {
        } catch (Exception e) {
        }
        return bm;
    }*/

}
