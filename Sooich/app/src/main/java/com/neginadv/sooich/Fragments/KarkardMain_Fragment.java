package com.neginadv.sooich.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.MainActivity;
import com.neginadv.sooich.R;
import com.neginadv.sooich.classes.BaseBackPressedListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by EhsanYazdanyar on 4/4/2018.
 */

public class KarkardMain_Fragment extends Fragment {

    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        ManageOnBackPressed();

        return view;
    }

    @OnClick(R.id.FM_G_OMOMI_RL)
    public void GoToOmomiMain() {
        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.putExtra("G_Omomi_Main", "G_Omomi_Main");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @OnClick(R.id.FM_G_SAFAR_RL)
    public void GoToSafarMain() {
        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.putExtra("G_Safar_Main", "G_Safar_Main");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @OnClick(R.id.FM_back_btn)
    public void GoBack() {
        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.putExtra("MainPage" , "MainPage");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                GoBack();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}

