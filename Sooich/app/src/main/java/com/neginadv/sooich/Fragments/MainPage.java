package com.neginadv.sooich.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.R;
import com.neginadv.sooich.adapters.main_viewpager_adapter;
import com.neginadv.sooich.classes.BaseBackPressedListener;
import com.neginadv.sooich.classes.CustomViewPager;
import com.neginadv.sooich.classes.GetSharedPreferencesData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by EhsanYazdanyar on 4/7/2018.
 */

public class MainPage extends Fragment {

    @BindView(R.id.view_pager_previous_view)
    View view_pager_previous_view;
    @BindView(R.id.view_pager_next_view)
    View view_pager_next_view;
    @BindView(R.id.view_pager) CustomViewPager viewPager;

    Animation slide_right, slide_left, slide_right_back, slide_left_back;
    String UToken , UName , UPassword = "";

    Unbinder unbinder;
    int next_view = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_mainpage, container, false);
        unbinder = ButterKnife.bind(this, view);
        ManageOnBackPressed();

        /* Animation */
        slide_right = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_right);
        slide_right_back = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_right_back);
        slide_left = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_left);
        slide_left_back = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_left_back);


        main_viewpager_adapter adapter = new main_viewpager_adapter(getActivity(), getActivity());
        viewPager.setAdapter(adapter);

        viewPager.setScrollEnable(true);

        viewPager.addOnPageChangeListener(pageChangeListener);

        return view;

    }

    /* arrow animation */
    Handler handler = new Handler();
    Runnable Update = new Runnable() {
        public void run() {

            if (viewPager.getCurrentItem() == 0) {
                view_pager_previous_view.setVisibility(View.INVISIBLE);
            } else {
                view_pager_previous_view.setVisibility(View.VISIBLE);
            }

            if (viewPager.getCurrentItem() == viewPager.getAdapter().getCount() - 1) {
                view_pager_next_view.setVisibility(View.INVISIBLE);
            } else {
                view_pager_next_view.setVisibility(View.VISIBLE);
            }

            if (next_view != 1) {

                view_pager_next_view.startAnimation(slide_right);
                slide_right.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation arg0) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation arg0) {
                    }

                    @Override
                    public void onAnimationEnd(Animation arg0) {
                        view_pager_next_view.startAnimation(slide_right_back);
                    }
                });
            }

            if (view_pager_previous_view.getVisibility() != View.INVISIBLE) {

                view_pager_previous_view.startAnimation(slide_left);
                slide_left.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation arg0) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation arg0) {
                    }

                    @Override
                    public void onAnimationEnd(Animation arg0) {
                        view_pager_previous_view.startAnimation(slide_left_back);
                    }
                });

            }

            handler.postDelayed(this, 2000);
        }
    };

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            if (viewPager.getCurrentItem() == 0) {
                view_pager_previous_view.setVisibility(View.INVISIBLE);
            } else {
                view_pager_previous_view.setVisibility(View.VISIBLE);
            }

            if (viewPager.getCurrentItem() == viewPager.getAdapter().getCount() - 1) {
                view_pager_next_view.setVisibility(View.INVISIBLE);
                next_view = 1;
            } else {
                view_pager_next_view.setVisibility(View.VISIBLE);
                next_view = 0;
            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }


    };

    /* its for get position of view pager item*/
    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    @OnClick({R.id.view_pager_next, R.id.view_pager_previous})
    public void PrevAndNextViewPager(Button btn) {

        int Id = btn.getId();

        if (Id == R.id.view_pager_next) {
            viewPager.setCurrentItem(getItem(+1), true);
        } else {
            viewPager.setCurrentItem(getItem(-1), true);
        }

    }


    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                //Page_Intent("main");
                Toast.makeText(getActivity() , "Back Must Inserted" , Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        handler.postDelayed(Update, 1000);
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(Update);
//        if (isFinishing()) {
//            Picasso.get().cancelRequest(Adv_Image_View);
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
