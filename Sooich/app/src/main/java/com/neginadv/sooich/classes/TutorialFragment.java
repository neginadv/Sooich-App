package com.neginadv.sooich.classes;

import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.neginadv.sooich.R;

/**
 * Created by EhsanYazdanyar on 15/07/2018.
 */

public class TutorialFragment extends Fragment {

    int Page_Position;
    View view = null;
    VideoView videoView , videoView2;
    private static final String TAG = "Tutorial Fragment";

    public TutorialFragment(int position) {
        this.Page_Position = position;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        switch (Page_Position) {

            case 0:
                view = inflater.inflate(R.layout.tt_page1 , container,false);
                TextViewEx Txt = (TextViewEx)view.findViewById(R.id.TT_FirstPageText);
                Txt.setText(getResources().getString(R.string.TT_FirstPage_Txt) , true);
                videoView = (VideoView) view.findViewById(R.id.videoView);
                playVideo(videoView , R.raw.car);
                break;

            case 1:
                view = inflater.inflate(R.layout.tt_page2 , container,false);
                TextViewEx Txt2 = (TextViewEx)view.findViewById(R.id.TT_SocondPageText);
                Txt2.setText(getResources().getString(R.string.TT_SocondPage_Txt) , true);
                videoView2 = (VideoView) view.findViewById(R.id.videoView2);
                playVideo(videoView2 , R.raw.watch);
                break;

            case 2:
                view = inflater.inflate(R.layout.tt_page3 , container,false);
                TextViewEx Txt3 = (TextViewEx)view.findViewById(R.id.TT_ThirdPageText);
                Txt3.setText(getResources().getString(R.string.TT_ThirdPage_Txt) , true);
                videoView = (VideoView) view.findViewById(R.id.videoView3);
                playVideo(videoView , R.raw.repair_car);
                break;

            case 3:
                view = inflater.inflate(R.layout.tt_page4 , container,false);
                TextViewEx Txt4 = (TextViewEx)view.findViewById(R.id.TT_FourthPageText);
                Txt4.setText(getResources().getString(R.string.TT_FourthPage_Txt) , true);
                videoView = (VideoView) view.findViewById(R.id.videoView4);
                playVideo(videoView , R.raw.officer);
                break;

            case 4:
                view = inflater.inflate(R.layout.tt_page5 , container,false);
                TextViewEx Txt5 = (TextViewEx)view.findViewById(R.id.TT_FifthPageText);
                Txt5.setText(getResources().getString(R.string.TT_FifthPage_Txt) , true);
                videoView = (VideoView) view.findViewById(R.id.videoView5);
                playVideo(videoView , R.raw.zarebin);
                break;
        }
        return view;

    }

    private void setVideoLocation(Uri uri , VideoView VS) {

        try {
            VS.setVideoURI(uri);
        } catch (Exception e) {
//            Log.e(TAG, "VideoPlayer uri was invalid", e);
            Toast.makeText(getActivity(), "Not found", Toast.LENGTH_SHORT).show();
        }

    }

    public void playVideo(final VideoView VS , final int VideoRes) {

        Uri uri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + VideoRes);
        setVideoLocation(uri , VS);
        VS.setZOrderOnTop(true);
        VS.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                mp.setVolume(0f, 0f);
                VS.start();
//                Log.e(TAG , "Video " + VideoRes + "started");

                /*if (!VS.isPlaying()) {
                    VS.start();
                    Log.e(TAG , "Video " + VideoRes + "started");
                }*/
            }
        });

    }

    public void stopVideo(VideoView VS){

        if (VS.isPlaying()){
            VS.pause();
        }

    }

}
