package com.neginadv.sooich.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.R;
import com.neginadv.sooich.classes.BaseBackPressedListener;
import com.neginadv.sooich.classes.ToastCustom;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by EhsanYazdanyar on 4/7/2018.
 */

public class G_Safar_Main extends Fragment {

    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_safar_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        ManageOnBackPressed();

        return view;

    }

    @OnClick({R.id.Number_Travel_Brn, R.id.Cost_From_Taxis, R.id.Daily_Activity, R.id.Monthly_Profit, R.id.Hourly_report, R.id.Daily_report, R.id.G_SAFAR_back_btn})
    public void btn_choose(Button btn) {

        switch (btn.getId()) {

            case R.id.Number_Travel_Brn:
                Page_Intent("Number_Of_Travel");
                break;

            case R.id.Cost_From_Taxis:
                Page_Intent("IncomeFromEachTaxi");
                break;

            case R.id.Daily_Activity:
                Page_Intent("DailyActivity");
                break;

            case R.id.Monthly_Profit:
                Page_Intent("NetProfitMonth");
                break;

            case R.id.Hourly_report:
                Page_Intent("HourlyActivity");
                break;

            case R.id.Daily_report:
                Page_Intent("DailyActivityByOrder");
                break;

            case R.id.G_SAFAR_back_btn:
                Page_Intent("main");
                break;

        }
    }

    private void Page_Intent(String tag) {

        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra(tag, tag);
        startActivity(intent);

    }

    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                Page_Intent("main");
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
