package com.neginadv.sooich.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Ehsan Yazdanyar on 3/23/2018.
 */

public class Database extends SQLiteOpenHelper {

    private static Database sInstance;

    private static final String DB_NAME = "sooich_DB";
    private static final int DB_VERSION = 1;

    /* TABLES NAME */
    public static final String TABLE_USER = "user";
    public static final String TABLE_PROFILE_DETAIL = "profile_detail";
    public static final String TABLE_TAXI_CO = "taxi_co";
    public static final String TABLE_CAR_TYPE = "car_type";
    public static final String TABLE_BASE_KILOMETER = "base_kilometer";
    public static final String TABLE_MOUNT_COST = "mount_cost";
    public static final String TABLE_PRICE_TIRE = "price_tires";
    public static final String TABLE_FUEL_EFFICIENCY = "fuel_efficiency";
    public static final String TABLE_STATE = "state_Tbl";
    /* END TABLES NAME */

    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_MOBILE = "phone";
    public static final String KEY_UNIQUE_CODE = "unique_code";
    public static final String KEY_CREATED_AT = "created_at";


    /* TABLE_PROFILE_DETAIL */
    public static final String USER_ID = "id";
    public static final String USER_NAME = "name";
    public static final String USER_MOBILE = "mobile";
    public static final String USER_CAR = "car";
    public static final String USER_INCOME = "income";
    public static final String USER_PROFIT = "profit";
    public static final String USER_CITY = "city";
    public static final String USER_STATE = "state";
    public static final String USER_CAR_MODEL = "car_model";
    public static final String USER_BASE_KILOMETER = "base_kilometer";
    public static final String USER_MOUNT_COST = "mount_cost";
    public static final String USER_PRICE_TIRE = "price_tires";
    public static final String USER_CREATED_AT = "created_at";
    /* END TABLE_PROFILE_DETAIL */

    public static final String TAXI_EN_NAME = "en_name";
    public static final String TAXI_FA_NAME = "fa_name";

    public static final String CAR_FA = "car_fa";
    public static final String CAR_EN = "car_en";

    public static final String STATE_FA = "state_fa";
    public static final String STATE_EN = "state_en";

    public static final String Kilometer = "kilometer";
    public static final String Mount_Cost_Value = "value";
    public static final String Price_Tires = "price";
    public static final String Fuel_Efficiency = "fuel";
    public static final String State= "state";


    private static final String TAG = "DataBase";

    public static synchronized Database getInstance(Context context){

        if (sInstance == null){
            sInstance = new Database(context.getApplicationContext());
        }
        return sInstance;
    }

    public Database(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase mydb) {

        String CREATE_LOGIN_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, " + KEY_NAME + " TEXT,"
                + KEY_UNIQUE_CODE + " TEXT," + KEY_MOBILE + " TEXT,"
                + KEY_CREATED_AT + " TEXT" + ")";
        mydb.execSQL(CREATE_LOGIN_TABLE);

        //user profile detail table
        String CREATE_USER_DETAIL_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_PROFILE_DETAIL + "("
                + USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + USER_NAME + " TEXT,"
                + USER_MOBILE + " TEXT,"
                + USER_CAR + " TEXT,"
                + USER_INCOME + " TEXT,"
                + USER_PROFIT + " TEXT,"
                + USER_CITY + " TEXT,"
                + USER_STATE + " TEXT,"
                + USER_CAR_MODEL + " TEXT,"
                + USER_PRICE_TIRE + " TEXT,"
                + USER_BASE_KILOMETER + " TEXT,"
                + USER_MOUNT_COST + " TEXT,"
                + KEY_CREATED_AT + " TEXT" + ")";

        mydb.execSQL(CREATE_USER_DETAIL_TABLE);

        String CREATE_TAXI_CO_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TAXI_CO + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + TAXI_EN_NAME + " TEXT,"
                + TAXI_FA_NAME + " TEXT" + ")";
        mydb.execSQL(CREATE_TAXI_CO_TABLE);

        String CREATE_CARTYPE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_CAR_TYPE + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + CAR_FA + " TEXT," + CAR_EN + " TEXT" + ")";
        mydb.execSQL(CREATE_CARTYPE_TABLE);

        String CREATE_BASE_KILOMETER_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_BASE_KILOMETER + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Kilometer + " TEXT" + ")";
        mydb.execSQL(CREATE_BASE_KILOMETER_TABLE);

        String CREATE_MOUNT_COST_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_MOUNT_COST + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Mount_Cost_Value + " TEXT" + ")";
        mydb.execSQL(CREATE_MOUNT_COST_TABLE);

        String CREATE_PRICE_TIRE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_PRICE_TIRE + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Price_Tires + " TEXT" + ")";
        mydb.execSQL(CREATE_PRICE_TIRE_TABLE);

        String CREATE_FUEL_EFFICIENCY_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_FUEL_EFFICIENCY + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + Fuel_Efficiency + " TEXT" + ")";
        mydb.execSQL(CREATE_FUEL_EFFICIENCY_TABLE);

        String CREATE_STATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_STATE + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + STATE_FA + " TEXT," + STATE_EN + " TEXT" + ")";
        mydb.execSQL(CREATE_STATE_TABLE);

        Log.e(TAG, "Database tables created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase mydb, int oldVersion, int newVersion) {

        mydb.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        mydb.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILE_DETAIL);
        mydb.execSQL("DROP TABLE IF EXISTS " + TABLE_TAXI_CO);
        mydb.execSQL("DROP TABLE IF EXISTS " + TABLE_CAR_TYPE);
        mydb.execSQL("DROP TABLE IF EXISTS " + TABLE_BASE_KILOMETER);
        mydb.execSQL("DROP TABLE IF EXISTS " + TABLE_MOUNT_COST);
        mydb.execSQL("DROP TABLE IF EXISTS " + TABLE_PRICE_TIRE);
        mydb.execSQL("DROP TABLE IF EXISTS " + TABLE_FUEL_EFFICIENCY);
        mydb.execSQL("DROP TABLE IF EXISTS " + TABLE_STATE);
        onCreate(mydb);

        Log.e(TAG, "Database tables Updated");

    }

}
