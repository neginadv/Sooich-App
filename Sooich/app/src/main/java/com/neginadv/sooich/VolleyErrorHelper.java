package com.neginadv.sooich;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.neginadv.sooich.classes.StartStopRemoteView;
import com.neginadv.sooich.classes.ToastCustom;
import com.neginadv.sooich.Shp.LoginSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by EhsanYazdanyar on 3/10/2018.
 */


public class VolleyErrorHelper {
    /**
     * @param error
     * @param context
     * @return
     */

    private static final String TAG = "Volley Error Helper";

    public void getMessage(Object error, Activity context, String Uname, String Pass) {

        if (error instanceof TimeoutError) {
            ToastCustom.viewToast(context, context.getResources().getString(R.string.ERR_TimeoutError));

        } else if (error instanceof NoConnectionError) {

            if (CheckOnline(context)) {
                ToastCustom.viewToast(context, context.getResources().getString(R.string.ERR_NoConnectionToServer));
            } else {
                ToastCustom.viewToast(context, context.getResources().getString(R.string.ERR_NoConnectionError));
            }

        } else if (error instanceof AuthFailureError) {
            handleServerError(error, context, Uname, Pass);

        } else if (error instanceof NetworkError) {
            ToastCustom.viewToast(context, context.getResources().getString(R.string.ERR_NetworkError));

        } else if (error instanceof ServerError) {
            handleServerError(error, context, Uname, Pass);

        }

    }

    private void handleServerError(Object error, Activity context, String Username, String Password) {

        VolleyError er = (VolleyError) error;
        NetworkResponse response = er.networkResponse;
        if (response != null) {

            switch (response.statusCode) {

                case 400:
                    String Error400 = new String(response.data);
                    Log.e("Error 400 - 1,", Error400);

                    try {

                        JSONObject Error400_obj = new JSONObject(Error400);
                        String errorMsg_400 = Error400_obj.getString("status");

                        if (Error400_obj.has("status")) {
                            if (Error400_obj.getString("status").equals("error")) {
                                if (Error400_obj.has("message")) {
                                    ToastCustom.viewToast(context, Error400_obj.getString("message"));
                                } else {
                                    ToastCustom.viewToast(context, context.getResources().getString(R.string.ERR_Other_Error));
                                }
                            }

                        }

                        if (Error400_obj.has("error_codes")) {
                            Log.e("Error 400 Obj ,", Error400_obj.getString("error_codes"));
                        }

                    } catch (JSONException e) {
                        //age aslan json nabood
                        Log.e("Error 400 Exception", e.getMessage());
                    }
                    break;

                case 404:
                    String Error404 = new String(response.data);
                    try {

                        JSONObject Error404_obj = new JSONObject(Error404);
                        String errorMsg_404 = Error404_obj.getString("message");

                        Log.e("Error 404 ,", errorMsg_404);

                        if (Error404_obj.has("error_codes")) {
                            Log.e("Error 404 Obj ,", Error404_obj.getString("error_codes"));
                        }

                    } catch (JSONException e) {
                        //age aslan json nabood
                    }
                    break;

                case 422:
                    break;

                case 401:
                    try {
                        // server might return error like this { "error": "Some error occured" }
                        // Use "Gson" to parse the result
                        HashMap<String, String> result = new Gson().fromJson(new String(response.data),
                                new TypeToken<Map<String, String>>() {
                                }.getType());

                        if (result != null && result.containsKey("error")) {

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    // invalid request
                    break;

                case 403:
                    String jsonError = new String(response.data);
                    try {
                        JSONObject objMain = new JSONObject(jsonError);
                        String errorMsg = objMain.getString("message");

                        if (objMain.has("error_codes")) {
                            String errorCode = objMain.getString("error_codes");
                            if (errorCode.equals("101")) {

                                //User Most Complete Details
                                ToastCustom.viewToast(context, context.getResources().getString(R.string.ERR_Complete_User_Detail));
                                Intent intent = new Intent(context, CompleteRegisterActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                intent.putExtra("user_name", Username);
                                intent.putExtra("user_pass", Password);
                                intent.putExtra("Action", "VERIFY");
                                context.startActivity(intent);
                                context.finish();

                            } else if (errorCode.equals("100")) {

                                // User Must Login
                                ToastCustom.viewToast(context, context.getResources().getString(R.string.ERR_Must_Login));
                                logoutUser(context);

                            } else if (errorCode.equals("200")) {

                                ToastCustom.viewToast(context, context.getResources().getString(R.string.ERR_Duplicate_User_Reg));

                            } else if (errorCode.equals("201")) {

                                ToastCustom.viewToast(context, context.getResources().getString(R.string.ERR_DialogVerifyForget_SMS_Sended));

                            }

                            Log.e("Error_Code", errorCode);
                            Log.e("Error_Msg", errorMsg);

                        } else {
                            ToastCustom.viewToast(context, errorMsg);
                        }
                    } catch (JSONException e) {
                        //age aslan json nabood
                        ToastCustom.viewToast(context, context.getResources().getString(R.string.ERR_Must_Login));
                    }

                    Log.e("403 Error:", jsonError);

                    break;

                default:
                    ToastCustom.viewToast(context, context.getResources().getString(R.string.ERR_Other_Error));

            }
        }

    }

    private static boolean isServerProblem(Object error) {
        return (error instanceof ServerError || error instanceof AuthFailureError);
    }

    private static boolean isNetworkProblem(Object error) {
        return (error instanceof NetworkError || error instanceof NoConnectionError);
    }

    private boolean CheckOnline(Activity context) {

        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = false;
        if (connectivityManager != null) {
            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
            isConnected = (activeNetwork != null) && (activeNetwork.isConnectedOrConnecting());
        }

        return isConnected;

    }

    private void logoutUser(Activity activity) {

        SharedPreferences shp = activity.getSharedPreferences("loginManagment", MODE_PRIVATE);
        SharedPreferences shp_UserProfile = activity.getSharedPreferences("UserProfile", MODE_PRIVATE);

        SharedPreferences.Editor editor;
        SharedPreferences.Editor editor_UserProfile;

        editor = shp.edit();
        editor_UserProfile = shp_UserProfile.edit();

        LoginSessionManager session = new LoginSessionManager(activity);

        editor.putString("TempUname", "");
        editor.putString("TempPass", "");
        editor.putString("TempToken", "");
        editor.putString("TempAdv", "");
        editor.commit();

        editor_UserProfile.putString("UserProfile", "1");
        editor_UserProfile.commit();

        Log.e(TAG, "Logout");

        session.setLogin(false);

        new StartStopRemoteView().StopService(activity);

        Intent intent = new Intent(activity, LoginAndReg.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        activity.startActivity(intent);
        activity.finish();

    }

}
