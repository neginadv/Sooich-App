package com.neginadv.sooich.classes;

/**
 * Created by EhsanYazdanyar on 4/23/2018.
 */

public interface OnBackPressedListener {
    public void doBack();
}
