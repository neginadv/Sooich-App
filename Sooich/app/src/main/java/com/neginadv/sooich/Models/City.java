package com.neginadv.sooich.Models;

/**
 * Created by EhsanYazdanyar on 11/26/2018.
 */

public class City {

    private int id;
    private String Fa_Name;
    private String En_Name;
    private int State_id;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFa_Name() {
        return Fa_Name;
    }

    public void setFa_Name(String fa_Name) {
        Fa_Name = fa_Name;
    }

    public String getEn_Name() {
        return En_Name;
    }

    public void setEn_Name(String en_Name) {
        En_Name = en_Name;
    }

    public int getState_id() {
        return State_id;
    }

    public void setState_id(int state_id) {
        State_id = state_id;
    }
}
