package com.neginadv.sooich.classes;

import java.util.ArrayList;

/**
 * Created by EhsanYazdanyar on 4/22/2018.
 */

public class GetMonthByIndex {

    public String GetMonth (int index){

        ArrayList<String> Month = new ArrayList<String>();

        Month.add(0 , "First Position");
        Month.add(1 , "فروردین");
        Month.add(2 , "اردیبهشت");
        Month.add(3 , "خرداد");
        Month.add(4 , "تیر");
        Month.add(5 , "مرداد");
        Month.add(6 , "شهریور");
        Month.add(7 , "مهر");
        Month.add(8 , "آبان");
        Month.add(9 , "آذر");
        Month.add(10 ,"دی");
        Month.add(11 ,"بهمن");
        Month.add(12 ,"اسفند");

        return Month.get(index);
    }
}
