package com.neginadv.sooich.Models;

/**
 * Created by EhsanYazdanyar on 5/15/2018.
 */

public class User_Profile {

    private int ID;
    private String Name;
    private String Phone;
    private String Car_Type;
    private String income;
    private String Profit;
    private String City;
    private String Price_Tire;
    private String State;
    private String Car_Model;
    private String Base_Kilometer;
    private String Mount_Cost;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getCar_Type() {
        return Car_Type;
    }

    public void setCar_Type(String car_Type) {
        Car_Type = car_Type;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getProfit() {
        return Profit;
    }

    public void setProfit(String profit) {
        Profit = profit;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPrice_Tire() {
        return Price_Tire;
    }

    public void setPrice_Tire(String price_Tire) {
        Price_Tire = price_Tire;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCar_Model() {
        return Car_Model;
    }

    public void setCar_Model(String car_Model) {
        Car_Model = car_Model;
    }

    public String getBase_Kilometer() {
        return Base_Kilometer;
    }

    public void setBase_Kilometer(String base_Kilometer) {
        Base_Kilometer = base_Kilometer;
    }

    public String getMount_Cost() {
        return Mount_Cost;
    }

    public void setMount_Cost(String mount_Cost) {
        Mount_Cost = mount_Cost;
    }

}
