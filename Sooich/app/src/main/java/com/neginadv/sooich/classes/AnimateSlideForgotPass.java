package com.neginadv.sooich.classes;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.neginadv.sooich.R;


/**
 * Created by EhsanYazdanyar on 02/08/2018.
 */

public class AnimateSlideForgotPass {

    private Animation slide_left , slide_left2;
    private Animation slide_Right , slide_Right2;

    private RelativeLayout Prev;
    private RelativeLayout Next;
    private Handler handler = new Handler();

    public AnimateSlideForgotPass(Context c) {

        slide_left = AnimationUtils.loadAnimation(c, R.anim.slide_left_fpass);
        slide_left2 = AnimationUtils.loadAnimation(c, R.anim.slide_left_fpass2);

        slide_Right = AnimationUtils.loadAnimation(c, R.anim.slide_right_fpass);
        slide_Right2 = AnimationUtils.loadAnimation(c, R.anim.slide_right_fpass2);

    }

    public void Start(RelativeLayout Prev_Rl, RelativeLayout Next_Rl , Boolean TrueRight_FalseLeft) {
        this.Prev = Prev_Rl;
        this.Next = Next_Rl;

        Stop();
        if (TrueRight_FalseLeft){
            handler.post(AnimateRight_Update);
        }else{
            handler.post(AnimateLeft_Update);
        }
    }

    public void Stop() {
        handler.removeCallbacks(AnimateLeft_Update);
        handler.removeCallbacks(AnimateRight_Update);
    }

    private Runnable AnimateLeft_Update = new Runnable() {
        @Override
        public void run() {
            AnimateLeft(Prev, Next);
        }
    };

    private Runnable AnimateRight_Update = new Runnable() {
        @Override
        public void run() {
            AnimateRight(Prev, Next);
        }
    };

    private void AnimateLeft(final RelativeLayout Rl, final RelativeLayout Rl2) {

        Rl.startAnimation(slide_left);
        slide_left.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Rl.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

        });

        Rl2.setVisibility(View.VISIBLE);
        Rl2.startAnimation(slide_left2);
        slide_left2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

    }

    private void AnimateRight(final RelativeLayout Rl, final RelativeLayout Rl2) {

        Rl.setVisibility(View.VISIBLE);
        Rl.startAnimation(slide_Right);
        slide_Right.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

        });


        Rl2.startAnimation(slide_Right2);
        slide_Right2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Rl2.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

    }

}
