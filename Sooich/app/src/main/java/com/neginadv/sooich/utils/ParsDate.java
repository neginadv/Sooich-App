package com.neginadv.sooich.utils;

import android.text.format.DateFormat;

import com.neginadv.sooich.ServiceApi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by EhsanYazdanyar on 16/05/2018.
 */

public class ParsDate {

    private Date Now_Date = new Date();

    private String newdate = (String) DateFormat.format("yyyy-MM-dd", Now_Date);
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public Date Now(){

        Date myDate = null;
        try {
            myDate = sdf.parse(newdate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return myDate;
    }

    public Date Parse_Time(String date){

        Date date1 = null;
        try {
            date1 = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date1;
    }

    public Date GetUpdateTime(Date userDate , int Update_Day){

        Calendar c = Calendar.getInstance();
        c.setTime(userDate);
        c.add(Calendar.DATE, Update_Day);

        return c.getTime();

    }

    public String MyNow_DateFormat(){

        Date date = new Date();
        return (String) DateFormat.format("yyyy-MM-dd", date);

    }



}
