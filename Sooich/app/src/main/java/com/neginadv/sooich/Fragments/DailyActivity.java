package com.neginadv.sooich.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.Dialogs.Msg_Dialog2;
import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.VolleyErrorHelper;
import com.neginadv.sooich.classes.AnimateMonthViews;
import com.neginadv.sooich.classes.BaseBackPressedListener;
import com.neginadv.sooich.classes.ConvertDpAndPx;
import com.neginadv.sooich.classes.GetMonthByIndex;
import com.neginadv.sooich.classes.HandleNextAndPrevMonth;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.SlideAnimation;
import com.neginadv.sooich.Shp.LoginSessionManager;
import com.neginadv.sooich.utils.GetTaxis;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by EhsanYazdanyar on 4/8/2018.
 */

public class DailyActivity extends Fragment {

    @BindView(R.id.DA_Success_Cont)
    RelativeLayout Success_Cont;

    @BindView(R.id.DA_arrow_month_left)
    View Previous_month;
    @BindView(R.id.DA_arrow_month_right)
    View Next_month;
    @BindView(R.id.DA_arrow_Taxi_left)
    View Previous_Taxi;
    @BindView(R.id.DA_arrow_Taxi_right)
    View Next_Taxi;
    @BindView(R.id.graph_shanbe_back)
    View g_shanbe_back;
    @BindView(R.id.graph_shanbe)
    View g_shanbe;
    @BindView(R.id.graph_yekshanbe)
    View g_yekshanbe;
    @BindView(R.id.graph_doshanbe)
    View g_doshanbe;
    @BindView(R.id.graph_seshanbe)
    View g_seshanbe;
    @BindView(R.id.graph_charshanbe)
    View g_charshanbe;
    @BindView(R.id.graph_panjshanbe)
    View g_panjshanbe;
    @BindView(R.id.graph_jome)
    View g_jome;

    @BindView(R.id.graph_num)
    LinearLayout Graph_Num;

    @BindView(R.id.DA_Year)
    TextView Year_Txt;
    @BindView(R.id.DA_month_view)
    TextView month_view;
    @BindView(R.id.DA_Taxi_view)
    TextView Taxi_view;
    @BindView(R.id.DA_All_Input)
    TextView DA_All_Travel_Num;
    @BindView(R.id.DA_graph_NoResult)
    TextView DA_graph_NoResult;

    @BindView(R.id.DA_Taxi_Logo)
    ImageView Taxi_Logo;


    Unbinder unbinder;
    private AnimateMonthViews animateMonthViews, animateTaxisViews;
    private int shanbe_h, yeshanbe_h, doshanbe_h, seshanbe_h, charshanbe_h, panjshanbe_h, jome_h = 0;

    int Month, Taxis = 0;
    private String Token = "";
    private ConvertDpAndPx converter;
    private GetTaxis getTaxis;
    int finalHeight;
    private ConvertNumbers_Fa_En Num_Converter;

    boolean FirstLunch = true;
    private Msg_Dialog2 Error_Dialog, Progress_Dialog;

    private static final String TAG = "Daily Activity";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_daily_activity, container, false);
        ini(view);

        Request_DailyActivity(getActivity(), "", 0, getTaxis.GetTaxisName(Taxis));

        return view;

    }

    private void ini(View view) {

        unbinder = ButterKnife.bind(this, view);
        ManageOnBackPressed();
        converter = new ConvertDpAndPx();
        getTaxis = new GetTaxis(getActivity());
        Num_Converter = new ConvertNumbers_Fa_En();
        GetDpSizeOfViewBack();
        Token = new LoginSessionManager(getActivity()).GetUserToken();

    }

    @OnClick({R.id.DA_btn_Taxi_right, R.id.DA_btn_Taxi_left})
    public void ChangeTaxis(Button btn) {

        Progress(false);

        if (btn.getId() == R.id.DA_btn_Taxi_right) {
            if (Taxis == 3) {
                Taxis = 0;
            } else {
                Taxis += 1;
            }
            Request_DailyActivity(getActivity(), "", Month, getTaxis.GetTaxisName(Taxis));
        } else {
            if (Taxis == 0) {
                Taxis = 3;
            } else {
                Taxis -= 1;
            }
            Request_DailyActivity(getActivity(), "", Month, getTaxis.GetTaxisName(Taxis));
        }

    }

    @OnClick({R.id.DA_btn_month_right, R.id.DA_btn_month_left})
    public void Next_PrevMonth(Button btn) {

        Progress(false);

        if (btn.getId() == R.id.DA_btn_month_right) {
            Request_DailyActivity(getActivity(), "N", 0, getTaxis.GetTaxisName(Taxis));
        } else {
            Request_DailyActivity(getActivity(), "P", 0, getTaxis.GetTaxisName(Taxis));
        }

    }

    @OnClick(R.id.DA_back_btn)
    public void GoBack() {
        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.putExtra("G_Safar_Main", "G_Safar_Main");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void Clear_All_Views() {

        DA_graph_NoResult.setVisibility(View.INVISIBLE);
        Taxi_Logo.setImageResource(0);

    }

    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                GoBack();
            }
        });
    }

    public void Request_DailyActivity(final Activity activity, String action, int My_Month, String Taxi_co) {

        String New_M = "";

        if (My_Month == 0) {
            New_M = new HandleNextAndPrevMonth().Action(action, Month);
        } else {
            New_M = String.valueOf(My_Month);
        }

        JSONObject object = new JSONObject();

        try {
            object.put("month", New_M);
            object.put("taxi_co", Taxi_co);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Progress(true);

        JsonObjectRequest ObjReq = new JsonObjectRequest(Request.Method.POST, ServiceApi.Daily_Activity, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Progress(false);
                Success_Cont.setVisibility(View.VISIBLE);
                Clear_All_Views();

                FirstLunch = false;

                try {

                    int No_Trip = Integer.valueOf(response.getString("no_trip"));

                    if (No_Trip > 0) {
                        JSONArray array = response.getJSONArray("week_daily_activity");
                        int[] numbers = new int[array.length()];

                        int maxindex = 0;
                        int max_number = 0;

                        for (int x = 0; x < array.length(); x++) {
                            numbers[x] = array.getInt(x);
                            if (numbers[x] > numbers[maxindex]) {
                                maxindex = x;
                                max_number = numbers[x];
                            }
                        }

                        int input = max_number;
                        int high_number = ((input / 50) + 1) * 50;
                        int input_height = (150 * input) / high_number;
                        Log.e(TAG, "Full height: 150 - " + "high number: " + high_number + " -  input_height: " + input_height);

                        int New_Shanbe = converter.dpToPx((150 * numbers[0]) / high_number);
                        Days_Animation_Show(g_shanbe, 0, shanbe_h, New_Shanbe);
                        shanbe_h = New_Shanbe;

                        int New_YekShabe = converter.dpToPx((150 * numbers[1]) / high_number);
                        Days_Animation_Show(g_yekshanbe, 50, yeshanbe_h, New_YekShabe);
                        yeshanbe_h = New_YekShabe;

                        int New_DoShanbe = converter.dpToPx((150 * numbers[2]) / high_number);
                        Days_Animation_Show(g_doshanbe, 100, doshanbe_h, New_DoShanbe);
                        doshanbe_h = New_DoShanbe;

                        int New_SeShanbe = converter.dpToPx((150 * numbers[3]) / high_number);
                        Days_Animation_Show(g_seshanbe, 150, seshanbe_h, New_SeShanbe);
                        seshanbe_h = New_SeShanbe;

                        int New_CharShanbe = converter.dpToPx((150 * numbers[4]) / high_number);
                        Days_Animation_Show(g_charshanbe, 200, charshanbe_h, New_CharShanbe);
                        charshanbe_h = New_CharShanbe;

                        int New_PanjShanbe = converter.dpToPx((150 * numbers[5]) / high_number);
                        Days_Animation_Show(g_panjshanbe, 250, panjshanbe_h, New_PanjShanbe);
                        panjshanbe_h = New_PanjShanbe;

                        int New_Jome = converter.dpToPx((150 * numbers[6]) / high_number);
                        Days_Animation_Show(g_jome, 300, jome_h, New_Jome);
                        jome_h = New_Jome;

                        Log.e(TAG, (150 * numbers[1]) / high_number + "");

                        PutGraphnumbers(Graph_Num, high_number / 10);

                        DA_All_Travel_Num.setText(No_Trip + "");

                    } else {

                        DA_graph_NoResult.setVisibility(View.VISIBLE);

                        PutGraphnumbers(Graph_Num, 5);
                        DA_All_Travel_Num.setText("0");

                        ResetGraphAndValues();

                    }

                    String Year = response.getString("year");
                    Month = Integer.valueOf(response.getString("month"));

                    String MyMonth = new GetMonthByIndex().GetMonth(Month);
                    month_view.setText(MyMonth);
                    Year_Txt.setText(Num_Converter.EN_TO_FA(Year));


                } catch (JSONException e) {

                    e.printStackTrace();
                    Log.e(TAG, e.getMessage());

                }

                Taxi_view.setText(getTaxis.GetTaxisTitle(Taxis));
                Taxi_Logo.setImageResource(getTaxis.GetTaxiLogo(Taxis));

                Log.e(TAG, "response : " + response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Progress(false);

                if (FirstLunch) {
                    ErrorDialog();
                }

                new VolleyErrorHelper().getMessage(error, activity, null, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", Token);
                return params;

            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        ObjReq.setRetryPolicy(policy);

        String Reg_Tag = "Request_DailyActivity_Tag";
        RequestController.getInstance().addToRequestQueue(ObjReq, Reg_Tag);

    }

    private void ErrorDialog() {
        Error_Dialog = new Msg_Dialog2(getActivity(), getString(R.string.ERR_RecieveData));
        Error_Dialog.HaveAttentionImage(true);
        Error_Dialog.cancel_Btn.setText(getString(R.string.Back));
        Error_Dialog.OK_Btn.setText(getString(R.string.Retry));

        Error_Dialog.SetOkDialogResult(new Msg_Dialog2.Msg_Ok_Btn() {
            @Override
            public void GetOkAction() {
                Error_Dialog.CloseDialog();
                Request_DailyActivity(getActivity(), "", 0, getTaxis.GetTaxisName(Taxis));
            }
        });

        Error_Dialog.SetCancelDialogResult(new Msg_Dialog2.MSG_Cancel_Btn() {
            @Override
            public void GetCancelAction() {
                GoBack();
            }
        });
    }

    private void Progress(boolean Enable) {

        if (Enable) {
            Progress_Dialog = new Msg_Dialog2(getActivity());
        } else {
            if (Progress_Dialog != null) {
                Progress_Dialog.CloseDialog();
                Progress_Dialog = null;
            }
        }

    }

    private void PutGraphnumbers(ViewGroup layout, int G_Number) {

        int count = (layout.getChildCount()) - 1;
        for (int i = 0; i < layout.getChildCount(); i++) {
            TextView child = (TextView) ((LinearLayout) layout).getChildAt(i);

            String y = String.valueOf(count * G_Number);
            child.setText(Num_Converter.EN_TO_FA(y));
            count = count - 1;
        }

    }

    private void GetDpSizeOfViewBack() {

        try {
            ViewTreeObserver vto = g_shanbe_back.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                public boolean onPreDraw() {
                    g_shanbe_back.getViewTreeObserver().removeOnPreDrawListener(this);
                    finalHeight = g_shanbe_back.getMeasuredHeight();
                    finalHeight = converter.pxToDp(finalHeight);
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            finalHeight = 150;
        }

    }

    public void Days_Animation_Show(View view, int Delay, int StartHeight, int EndHeight) {

        final int ANIMATION_DURATION = 500;

        view.clearAnimation();
        Animation anim = new SlideAnimation(view, StartHeight, EndHeight);
        final AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(anim);
        animationSet.setInterpolator(new AccelerateInterpolator());
        animationSet.setDuration(ANIMATION_DURATION + Delay);
        view.setAnimation(animationSet);


    }

    private void ResetGraphAndValues() {

        Days_Animation_Show(g_shanbe, 0, shanbe_h, 0);
        Days_Animation_Show(g_yekshanbe, 0, yeshanbe_h, 0);
        Days_Animation_Show(g_doshanbe, 0, doshanbe_h, 0);
        Days_Animation_Show(g_seshanbe, 0, seshanbe_h, 0);
        Days_Animation_Show(g_charshanbe, 0, charshanbe_h, 0);
        Days_Animation_Show(g_panjshanbe, 0, panjshanbe_h, 0);
        Days_Animation_Show(g_jome, 0, jome_h, 0);
        shanbe_h = 0;
        yeshanbe_h = 0;
        doshanbe_h = 0;
        seshanbe_h = 0;
        charshanbe_h = 0;
        panjshanbe_h = 0;
        jome_h = 0;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (animateMonthViews == null) {
            animateMonthViews = new AnimateMonthViews(getActivity(), Next_month, Previous_month);
        }
        animateMonthViews.start();

        if (animateTaxisViews == null) {
            animateTaxisViews = new AnimateMonthViews(getActivity(), Next_Taxi, Previous_Taxi);
        }
        animateTaxisViews.start();
    }

    @Override
    public void onPause() {
        super.onPause();

        try {
            animateMonthViews.stop();
            animateTaxisViews.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Error_Dialog != null) {
            Error_Dialog.dismiss();
            Error_Dialog = null;
        }

        if (Progress_Dialog != null) {
            Progress_Dialog.dismiss();
            Progress_Dialog = null;
        }
        unbinder.unbind();
    }

}
