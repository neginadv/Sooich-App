package com.neginadv.sooich.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.CompleteRegisterActivity;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.Models.User_Profile;
import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.VolleyErrorHelper;
import com.neginadv.sooich.classes.BaseBackPressedListener;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.setCamaToNumber;
import com.neginadv.sooich.helper.Database;
import com.neginadv.sooich.helper.DatabaseHelper;
import com.neginadv.sooich.Shp.LoginSessionManager;
import com.neginadv.sooich.Shp.UserProfileManager;
import com.neginadv.sooich.utils.PracticalClasses;
import com.neginadv.sooich.utils.ReadJsonFromFile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * Created by EhsanYazdanyar on 4/3/2018.
 */


public class UserProfile extends Fragment {

    @BindView(R.id.UPF_name)
    TextView user_p_name;

    @BindView(R.id.UPF_phone)
    TextView user_p_phone;

    @BindView(R.id.UPF_car)
    TextView user_p_car;

    @BindView(R.id.UPF_daramad)
    TextView user_p_daramad;

    @BindView(R.id.UPF_sood)
    TextView user_p_sood;

    private UserProfileManager userProfileManager;
    LoginSessionManager LoginSession;
    private ConvertNumbers_Fa_En Num_Converter;
    private setCamaToNumber Cama;
    private Unbinder unbinder;

    private String Token = "";

    private static final String TAG = UserProfile.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_userprofile, container, false);
        ini(view);

        Check_UserProfile_Status(getActivity());

        return view;
    }

    private void ini(View view) {

        unbinder = ButterKnife.bind(this, view);
        ManageOnBackPressed();

        Cama = new setCamaToNumber();
        userProfileManager = new UserProfileManager(getActivity());
        Num_Converter = new ConvertNumbers_Fa_En();
        LoginSession = new LoginSessionManager(getActivity());
        Token = LoginSession.GetUserToken();

    }

    public void Check_UserProfile_Status(Activity activity) {

        boolean Status = userProfileManager.NeedUpdate();

        if (Status) {
            //get data from server and store in db
            getUserProfile(activity);

        } else {

//            user_profile_detail.setVisibility(View.VISIBLE);

            DatabaseHelper DB = new DatabaseHelper(getActivity());

            List<User_Profile> User = DB.GetUserProfile();
            if (User.size() > 0) {

                User_Profile user_profile = User.get(0);

                String User_Name = user_profile.getName();
                String User_Mobile = user_profile.getPhone();
                String User_Car = user_profile.getCar_Type();
                String User_Income = user_profile.getIncome();
                String User_Profit = user_profile.getProfit();

                user_p_name.setText(User_Name);
                user_p_phone.setText(Num_Converter.EN_TO_FA(User_Mobile));
                user_p_car.setText(Num_Converter.EN_TO_FA(User_Car));
                user_p_daramad.setText(Num_Converter.EN_TO_FA(Cama.Edit(User_Income)) + " " + getResources().getString(R.string.Rial));
                user_p_sood.setText(Num_Converter.EN_TO_FA(Cama.Edit(User_Profit)) + " " + getResources().getString(R.string.Rial));

                Log.e(TAG, "have value !!!" + User_Name);
            } else {
                Log.e(TAG, "No Res !!!");
            }

            DB.closeDb();

        }
    }


    public void getUserProfile(final Activity activity) {

//        userprofile_progress_RL.setVisibility(View.VISIBLE);
//        user_profile_detail.setVisibility(View.INVISIBLE);

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.User_Profile, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

//                userprofile_progress_RL.setVisibility(View.INVISIBLE);
//                user_profile_detail.setVisibility(View.VISIBLE);

                final DatabaseHelper DB = new DatabaseHelper(getActivity());

                ReadJsonFromFile GetJsonData = new ReadJsonFromFile(getActivity());

                try {
                    response = response.getJSONObject("user_data");

                    String user_name = response.getString("fullname");
                    String user_phone = response.getString("mobile_no");
//                    String user_car = DB.GetCarsByName(response.getString("car_type"), "EN");
                    String user_car =GetJsonData.GetCarByName(response.getString("car_type")).getCar_fa();
                    String user_income = response.getString("Income");
                    String user_profit = response.getString("profit");
                    String user_city = GetJsonData.GetCityByName(response.getString("city")).getFa_Name();
                    String user_state = GetJsonData.GetStateByName(response.getString("state")).getFa_Name();
                    String user_car_model = response.getString("car_type_models");
                    String user_base_kilometer = response.getString("base_kilometer");
                    String user_mount_cost = response.getString("mount_cost");
                    String user_price_tire = response.getString("price_tires");

                    /* Set Data 2 Array For Db Inserting */
                    final List<User_Profile> userProfile_List = new ArrayList<>();
                    User_Profile userProfile = new User_Profile();

                    userProfile.setName(user_name);
                    userProfile.setCar_Type(user_car);
                    userProfile.setPhone(user_phone);
                    userProfile.setIncome(user_income);
                    userProfile.setProfit(user_profit);
                    userProfile.setCity(user_city);
                    userProfile.setState(user_state);
                    userProfile.setCar_Model(user_car_model);
                    userProfile.setBase_Kilometer(user_base_kilometer);
                    userProfile.setMount_Cost(user_mount_cost);
                    userProfile.setPrice_Tire(user_price_tire);

                    userProfile_List.add(userProfile);

                    DB.DeleteFromTbl(Database.TABLE_PROFILE_DETAIL);

                    user_p_name.setText(user_name);
                    user_p_phone.setText(Num_Converter.EN_TO_FA(user_phone));
                    user_p_car.setText(Num_Converter.EN_TO_FA(user_car));
                    user_p_daramad.setText(Num_Converter.EN_TO_FA(Cama.Edit(user_income)) + " " + getResources().getString(R.string.Rial));
                    user_p_sood.setText(Num_Converter.EN_TO_FA(Cama.Edit(user_profit)) + " " + getResources().getString(R.string.Rial));


                    DB.InsertToUserProfile(userProfile_List);

                    userProfileManager.SetUpdated();

                    DB.closeDb();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("response: ", String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

//                userprofile_progress_RL.setVisibility(View.INVISIBLE);
                Log.e("error", String.valueOf(error));

                new VolleyErrorHelper().getMessage(error, getActivity(), null, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", Token);

                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Reg_Tag = "Get_User_Profile_Tag";
        RequestController.getInstance().addToRequestQueue(strReq, Reg_Tag);

    }

    @OnClick(R.id.UPF_back_btn)
    public void GoBack() {
        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.putExtra("MainPage", "MainPage");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @OnClick(R.id.UPF_Update_Profile)
    public void UpdateProfile() {
        Intent intent = new Intent(getActivity(), CompleteRegisterActivity.class);
        intent.putExtra("Action", "UPDATE_P");
        startActivity(intent);
    }

    @OnClick(R.id.UPF_logout)
    public void logout() {
        LoginSession.setLogout();
    }

    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                GoBack();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
