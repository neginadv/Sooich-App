package com.neginadv.sooich.classes;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.toolbox.ImageLoader;

/**
 * Created by EhsanYazdanyar on 24/07/2018.
 */

public class BitmapLruCache extends LruCache<String,Bitmap> implements ImageLoader.ImageCache {

    public static int getDefaultLruCacheSize(){
        final int MaxMemory = (int)(Runtime.getRuntime().maxMemory()/1024);
        final int cachSize = MaxMemory/8;

        return cachSize;
    }

    public BitmapLruCache() {
        super(getDefaultLruCacheSize());
    }

    @Override
    protected int sizeOf(String key, Bitmap value) {
        return value.getRowBytes()*value.getHeight()/1024;
    }

    @Override
    public Bitmap getBitmap(String url) {
        return get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        put(url,bitmap);
    }
}
