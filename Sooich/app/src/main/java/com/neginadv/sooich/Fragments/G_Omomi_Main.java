package com.neginadv.sooich.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.R;
import com.neginadv.sooich.classes.BaseBackPressedListener;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by EhsanYazdanyar on 4/7/2018.
 */

public class G_Omomi_Main extends Fragment {

    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_omomi_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        ManageOnBackPressed();

        return view;

    }

    @OnClick({R.id.BestTime_Travel_report, R.id.BestDays_Travel_Report, R.id.High_Travel_Report, R.id.Best_Profit_AllDrivers, R.id.G_OMOMI_back_btn})
    public void btn_choose(Button btn) {

        switch (btn.getId()) {

            case R.id.G_OMOMI_back_btn:
                Page_Intent("main");
                break;

            case R.id.BestTime_Travel_report:
                Page_Intent("BestHourRide");
                break;

            case R.id.BestDays_Travel_Report:
                Page_Intent("SuperiorDaysRide");
                break;

            case R.id.High_Travel_Report:
                Page_Intent("MostTravel");
                break;

            case R.id.Best_Profit_AllDrivers:
                //Page_Intent("MostProfitOfTaxis");
                break;

        }
    }

    private void Page_Intent(String tag) {

        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra(tag, tag);
        startActivity(intent);

    }

    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                Page_Intent("main");
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
