package com.neginadv.sooich.Converter;

import android.util.Log;

/**
 * Created by EhsanYazdanyar on 5/5/2018.
 */

public class ConvertDoubleToString {


    public String ConvertDoubleToString(String d) {

        String Returned_String;
        try {
            Returned_String = String.valueOf(Double.valueOf(d).intValue());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            Returned_String = "0";
            Log.e("", "Error is in ConvertDoubleToString (class)");
        }

        return Returned_String;

    }

}
