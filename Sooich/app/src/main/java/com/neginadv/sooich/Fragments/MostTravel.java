package com.neginadv.sooich.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.Dialogs.Msg_Dialog2;
import com.neginadv.sooich.adapters.RecyclerAdapter;
import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.Converter.ConvertDoubleToString;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.Models.BestDriver;
import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.VolleyErrorHelper;
import com.neginadv.sooich.classes.AnimateMonthViews;
import com.neginadv.sooich.classes.BaseBackPressedListener;
import com.neginadv.sooich.classes.GetMonthByIndex;
import com.neginadv.sooich.classes.HandleNextAndPrevMonth;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.setCamaToNumber;
import com.neginadv.sooich.Shp.LoginSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by EhsanYazdanyar on 4/11/2018.
 */

public class MostTravel extends Fragment {

    @BindView(R.id.MT_Success_Cont)
    RelativeLayout Success_Cont;

    @BindView(R.id.MT_arrow_month_left)
    View Previous_month;
    @BindView(R.id.MT_arrow_month_right)
    View Next_month;

    @BindView(R.id.MT_month_view)
    TextView month_view;
    @BindView(R.id.MT_Year)
    TextView Year_Txt;
    @BindView(R.id.MT_Uname)
    TextView Rank_Uname;
    @BindView(R.id.MT_UNumberTravel)
    TextView Rank_UnumberTravel;
    @BindView(R.id.MT_UCost)
    TextView Rank_UCost;
    @BindView(R.id.MT_URank)
    TextView Rank_URank;
    @BindView(R.id.MT_NoResult)
    TextView NoResult;

    @BindView(R.id.MT_Recycler)
    RecyclerView RW;


    private RecyclerAdapter MyAdapter;
    private List<BestDriver> BestDriver_ArrayList = new ArrayList<>();
    Unbinder unbinder;
    private AnimateMonthViews animateMonthViews;
    int Month = 0;
    private String Token = "";
    private ConvertNumbers_Fa_En Num_Converter;

    boolean FirstLunch = true;
    private Msg_Dialog2 Error_Dialog, Progress_Dialog;

    private static final String TAG = "Most Travel";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_most_travel, container, false);
        ini(view);

        MyAdapter = new RecyclerAdapter(BestDriver_ArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        RW.setLayoutManager(mLayoutManager);
        RW.setAdapter(MyAdapter);

        Request_Rank(getActivity(), "");



        return view;

    }

    private void ini(View view) {

        ManageOnBackPressed();
        unbinder = ButterKnife.bind(this, view);
        Num_Converter = new ConvertNumbers_Fa_En();
        Token = new LoginSessionManager(getActivity()).GetUserToken();

    }

    @OnClick({R.id.MT_btn_month_right, R.id.MT_btn_month_left})
    public void Next_PrevMonth(Button btn) {

        Progress(false);

        if (btn.getId() == R.id.MT_btn_month_right) {
            Request_Rank(getActivity(), "N");
        } else {
            Request_Rank(getActivity(), "P");
        }

    }

    @OnClick(R.id.MT_back_btn)
    public void GoBack() {
        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.putExtra("G_Omomi_Main", "G_Omomi_Main");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    public void Clear_All_Views() {

        Rank_Uname.setText("");
        Rank_UnumberTravel.setText("");
        Rank_UCost.setText("");
        Rank_URank.setText("");
        NoResult.setVisibility(View.INVISIBLE);
        BestDriver_ArrayList.clear();

    }

    public void Request_Rank(final Activity activity, String action) {

        String New_M = new HandleNextAndPrevMonth().Action(action, Month);

        JSONObject object = new JSONObject();

        try {
            object.put("month", New_M);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Progress(true);

        JsonObjectRequest ObjReq = new JsonObjectRequest(Request.Method.POST, ServiceApi.Users_Ranking, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Progress(false);
                Success_Cont.setVisibility(View.VISIBLE);
                Clear_All_Views();

                FirstLunch = false;

                try {
                    String Year = response.getString("year");
                    Month = Integer.valueOf(response.getString("month"));

                    JSONArray jArray = response.getJSONArray("user_no");

                    JSONObject User_Object = response.getJSONObject("user_data");

                    if (User_Object.length() > 0) {

                        Rank_URank.setText(Num_Converter.EN_TO_FA(response.getString("user_rank")));
                        Rank_Uname.setText(User_Object.getString("name"));
                        Rank_UnumberTravel.setText(Num_Converter.EN_TO_FA(User_Object.getString("no_trip")));
                        Rank_UCost.setText(Num_Converter.EN_TO_FA(new setCamaToNumber().Edit(new ConvertDoubleToString().ConvertDoubleToString(User_Object.getString("income")))) + " " + getResources().getString(R.string.Rial));

                    } else {

                        Rank_URank.setText("?");
                        Rank_Uname.setText("?");
                        Rank_UnumberTravel.setText("?");
                        Rank_UCost.setText("?");

                    }

                    if (jArray.length() > 0) {
                        int j = 0;
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject jsonObject = jArray.getJSONObject(i);

                            BestDriver bestDriver_Model = new BestDriver();

                            if (Integer.valueOf(response.getString("user_rank")) > (j + 1)) {
                                j = i + 1;
                            } else if (Integer.valueOf(response.getString("user_rank")) == (j + 1)) {
                                j = i + 2;
                            } else {
                                j = j + 1;
                            }

                            Log.e("i", j + "");
                            bestDriver_Model.setRank(j + "");
                            bestDriver_Model.setAmount(Num_Converter.EN_TO_FA(new setCamaToNumber().Edit(new ConvertDoubleToString().ConvertDoubleToString(jsonObject.getString("income")))) + " " + getResources().getString(R.string.Rial));
                            bestDriver_Model.setName(jsonObject.getString("name"));
                            bestDriver_Model.setNum_Travel(Num_Converter.EN_TO_FA(jsonObject.getString("no_trip")));

                            BestDriver_ArrayList.add(bestDriver_Model);

                        }

                        MyAdapter = new RecyclerAdapter(BestDriver_ArrayList);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        RW.setLayoutManager(mLayoutManager);
                        RW.setItemAnimator(new DefaultItemAnimator());
                        RW.setAdapter(MyAdapter);

                    } else {
                        NoResult.setVisibility(View.VISIBLE);
                    }

                    String MyMonth = new GetMonthByIndex().GetMonth(Month);
                    month_view.setText(MyMonth);
                    Year_Txt.setText(Num_Converter.EN_TO_FA(Year));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e(TAG, "response : " + response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Progress(false);

                if (FirstLunch) {
                    ErrorDialog();
                }

                new VolleyErrorHelper().getMessage(error, activity, null, null);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", Token);
                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        ObjReq.setRetryPolicy(policy);

        String Reg_Tag = "Request_Rank_Tag";
        RequestController.getInstance().addToRequestQueue(ObjReq, Reg_Tag);

    }

    private void ErrorDialog() {
        Error_Dialog = new Msg_Dialog2(getActivity(), getString(R.string.ERR_RecieveData));
        Error_Dialog.HaveAttentionImage(true);
        Error_Dialog.cancel_Btn.setText(getString(R.string.Back));
        Error_Dialog.OK_Btn.setText(getString(R.string.Retry));

        Error_Dialog.SetOkDialogResult(new Msg_Dialog2.Msg_Ok_Btn() {
            @Override
            public void GetOkAction() {
                Error_Dialog.CloseDialog();
                Request_Rank(getActivity(), "");
            }
        });

        Error_Dialog.SetCancelDialogResult(new Msg_Dialog2.MSG_Cancel_Btn() {
            @Override
            public void GetCancelAction() {
                GoBack();
            }
        });
    }

    private void Progress(boolean Enable) {

        if (Enable) {
            Progress_Dialog = new Msg_Dialog2(getActivity());
        } else {
            if (Progress_Dialog != null) {
                Progress_Dialog.CloseDialog();
                Progress_Dialog = null;
            }
        }

    }

    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                GoBack();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (animateMonthViews == null) {
            animateMonthViews = new AnimateMonthViews(getActivity(), Next_month, Previous_month);
        }
        animateMonthViews.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            animateMonthViews.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Error_Dialog != null) {
            Error_Dialog.dismiss();
            Error_Dialog = null;
        }

        if (Progress_Dialog != null) {
            Progress_Dialog.dismiss();
            Progress_Dialog = null;
        }
        unbinder.unbind();
    }

}
