package com.neginadv.sooich.classes;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by EhsanYazdanyar on 4/29/2018.
 */

public class CustomViewPager extends ViewPager{

    private boolean swipable;

    public CustomViewPager(Context context) {
        super(context);
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if(swipable) {
            return super.onInterceptTouchEvent(ev);
        }else {
            return false;
        }
    }

    public void setScrollEnable(boolean enable){
        swipable = enable;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (swipable) {
            return super.onTouchEvent(ev);
        }else {
            return false;
        }
    }


}
