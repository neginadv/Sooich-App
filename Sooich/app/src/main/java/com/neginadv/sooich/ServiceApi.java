package com.neginadv.sooich;

/**
 * Created by EhsanYazdanyar on 3/10/2018.
 */

public class ServiceApi {

    public static String BASE_URL = "http://192.168.1.151:8001/";
//    public static String BASE_URL = "http://192.168.1.104:8001/";
    public static String BASE_API_URL = BASE_URL + "api/";
    public static String register_page = BASE_API_URL + "user_signup/"; // تکمیل اطلاعات ثبت نام کاربر
    public static String sms_verifi = BASE_API_URL + "sms_verifi/";
    public static String User_Login = BASE_API_URL + "user_login/";
    public static String Forgot_Pass = BASE_API_URL + "user_forgot_pass/";
    public static String User_Profile = BASE_API_URL + "user_profile/";
    public static String Get_Default_Val = BASE_API_URL + "defaults_val/"; //دریافت اطلاعات خودروها-شهرها و ...
    public static String Get_Hour_Ranking = BASE_API_URL + "hour_ranking/"; //گزارش بر اساس بهترین ساعت های مسافرگیری
    public static String Number_Of_Travel = BASE_API_URL + "month_count_analysis/"; //تعداد سفرهای انجام شده
    public static String Income_From_Each_Taxi = BASE_API_URL + "month_income_analysis/"; //مبلغ درآمد از هر تاکسی یاب ویا به صورت تجمعی
    public static String Profit_Month = BASE_API_URL + "profit_month/"; //سود خالص ماهانه پس از کسر هزینه های سفر
    public static String Users_Ranking = BASE_API_URL + "users_ranking/"; //گزارش بیشترین سفارش گیری در بین راننده ها
    public static String Most_Profit_Taxis = BASE_API_URL + "profit_taxico_analysis/"; //گزارش بیشترین سودخالص تاکسی یاب ها بر اساس درصد
    public static String Daily_ActivityByOrder = BASE_API_URL + "daily_report_analysis/"; //گزارش روزانه فعالیت بر اساس مبلغ سفارش
    public static String Daily_Activity = BASE_API_URL + "daily_activity/"; // گزارش تعداد سفر براساس هر تاکسی یاب
    public static String Hourly_Activity = BASE_API_URL + "hourly_activity_report/"; //گزارش ساعات فعالیت
    public static String SuperiorDaysRide = BASE_API_URL + "week_analysis/"; //بهترین روزهای مسافرگیری دربین تمامی راننده ها
    public static String User_Alerts = BASE_API_URL + "user_alert/"; //نمایش پیام های کاربر(غیرفعال)
    public static String New_Travel = BASE_API_URL + "submit_re/"; //ثبت سفر جدید
    public static String Banner_Url = BASE_API_URL + "user_banner/"; //بنر تبلیغاتی
    public static String API_TOKEN = "3c3559a6A20557";

    /* Global Vars */
//    public static int SocketTimeout = 6000;
    public static int SocketTimeout = 2000;
    public static int ToastTimeOut = 5000;
    public static int Update_DefultValues = 1;
    public static final String LDVU = "Last_Default_Value_Update";

}
