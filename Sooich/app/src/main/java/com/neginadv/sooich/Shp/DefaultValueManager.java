package com.neginadv.sooich.Shp;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.format.DateFormat;
import android.util.Log;

import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.utils.ParsDate;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by EhsanYazdanyar on 11/24/2018.
 */

public class DefaultValueManager {

    private static final String TAG = DefaultValueManager.class.getSimpleName();

    private SharedPreferences DefaultValue_Shp;
    private SharedPreferences.Editor Shp_Editor;
    private Context _context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "Last_Default_Value_Update";

    public DefaultValueManager(Context context) {
        this._context = context;
        DefaultValue_Shp = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        Shp_Editor = DefaultValue_Shp.edit();
    }

    public boolean CanGetDefaultValue() {

        String Last_Date = DefaultValue_Shp.getString("Date", null);

        if (Last_Date == null) {

            Log.e(TAG, "Default value is null (Need Get New Default)");
            return true;

        } else {

            ParsDate parsDate = new ParsDate();
            Date now = parsDate.Now();

            if (parsDate.GetUpdateTime(parsDate.Parse_Time(Last_Date), ServiceApi.Update_DefultValues).after(now)) {

                Log.e(TAG, "Dont Need Update Default Values");
                return false;

            } else {

                Log.e(TAG, "Last Default Values Derecated (Need Get New Default)");
                return true;

            }
        }
    }

    public void InsertLastUpdateTime() {

        Date date = new Date();

        String newdate = (String) DateFormat.format("yyyy-MM-dd", date);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Shp_Editor.putString("Date", newdate);
        Shp_Editor.commit();

    }

}
