package com.neginadv.sooich.Shp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.neginadv.sooich.LoginAndReg;

/**
 * Created by EhsanYazdanyar on 3/17/2018.
 */

public class LoginSessionManager {

    private static String TAG = LoginSessionManager.class.getSimpleName();

    private SharedPreferences Login_Shp;
    private SharedPreferences.Editor Login_Shp_Editor;
    private Context _context;

    private String UserToken, UserName, UserPassword;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "SooichLogin";

    private static final String PREF_UNAME = "username";
    private static final String PREF_PASSWORD = "password";
    private static final String PREF_TOKEN = "token";

    private static final String PREF_TEMP_UNAME = "TempUname";
    private static final String PREF_TEMP_PASSWORD = "TempPass";
    private static final String PREF_TEMP_TOKEN = "TempToken";

    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

    public LoginSessionManager(Context context) {
        this._context = context;
        Login_Shp = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        Login_Shp_Editor = Login_Shp.edit();
    }

    public void setLogin(boolean isLoggedIn) {

        Login_Shp_Editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        Login_Shp_Editor.commit();

    }

    public void setLogout() {

        setLogin(false);
        RemoveTempData();
        Intent intent = new Intent(_context , LoginAndReg.class);
        _context.startActivity(intent);

    }

    public boolean isLoggedIn() {
        return Login_Shp.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    public String GetUserToken() {
        GetTempData();
        return UserToken;
    }

    public String GetUserName() {
        GetTempData();
        return UserToken;
    }

    public String GetUserPassword() {
        GetTempData();
        return UserToken;
    }

    private void GetTempData() {

        String UNAME = String.valueOf(Login_Shp.getString(PREF_TEMP_UNAME, null));

        if (UNAME != null) {
            UserName = Login_Shp.getString(PREF_TEMP_UNAME, null);
            UserPassword = Login_Shp.getString(PREF_TEMP_PASSWORD, null);
            UserToken = Login_Shp.getString(PREF_TEMP_TOKEN, null);
        } else {
            Log.e(TAG, "Temp Data Is Null !!");
        }

    }

    public void SaveUserData(String Mobile, String Password, String Token) {
        Login_Shp_Editor.putString(PREF_UNAME, Mobile);
        Login_Shp_Editor.putString(PREF_PASSWORD, Password);
        Login_Shp_Editor.putString(PREF_TOKEN, Token);
        Login_Shp_Editor.commit();
    }

    public String GetSavedUserName() {
        return Login_Shp.getString(PREF_UNAME, "");
    }

    public String GetSavedPassword() {
        return Login_Shp.getString(PREF_PASSWORD, "");
    }

    public void StoreTempUser(String Mobile, String Password, String Token) {

        boolean CheckUserSavedData = CheckUserSavedData(Mobile);
        if (CheckUserSavedData) {
            Login_Shp_Editor.putString(PREF_TEMP_UNAME, Login_Shp.getString(PREF_UNAME, null));
            Login_Shp_Editor.putString(PREF_TEMP_PASSWORD, Login_Shp.getString(PREF_PASSWORD, null));
            Login_Shp_Editor.putString(PREF_TEMP_TOKEN, Login_Shp.getString(PREF_TOKEN, null));
        } else {
            Login_Shp_Editor.putString(PREF_TEMP_UNAME, Mobile);
            Login_Shp_Editor.putString(PREF_TEMP_PASSWORD, Password);
            Login_Shp_Editor.putString(PREF_TEMP_TOKEN, Token);
        }

        Login_Shp_Editor.commit();

    }

    private boolean CheckUserSavedData(String NewUserName) {

        String SavedUserName = String.valueOf(Login_Shp.getString(PREF_UNAME, null));

        if (SavedUserName.equals(NewUserName)) {
            return true;
        } else {
            return false;
        }

    }

    private void RemoveTempData() {
        Login_Shp_Editor.putString(PREF_TEMP_UNAME, "");
        Login_Shp_Editor.putString(PREF_TEMP_PASSWORD, "");
        Login_Shp_Editor.putString(PREF_TEMP_TOKEN, "");
        Login_Shp_Editor.commit();

        new UserProfileManager(_context).ResetProfile();
    }

}