package com.neginadv.sooich.Models;

/**
 * Created by EhsanYazdanyar on 4/11/2018.
 */

public class BestDriver {

    private String Rank;
    private String Name;
    private String Num_Travel;
    private String Amount;

    public String getRank() {
        return Rank;
    }

    public void setRank(String rank) {
        Rank = rank;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNum_Travel() {
        return Num_Travel;
    }

    public void setNum_Travel(String num_Travel) {
        Num_Travel = num_Travel;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

}
