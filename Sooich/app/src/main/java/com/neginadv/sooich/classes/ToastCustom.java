package com.neginadv.sooich.classes;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;

/**
 * Created by Ehsan Yazdanyar on 3/14/2018.
 */

public class ToastCustom {


    public static void viewToast(Context context , String inputText){


        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup) ((Activity) context).findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(inputText);



        Toast mytoast = new Toast(context);
        mytoast.setView(layouttoast);
        mytoast.setDuration(ServiceApi.ToastTimeOut);
        mytoast.show();

    }

}
