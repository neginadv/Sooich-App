package com.neginadv.sooich.classes;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.neginadv.sooich.Converter.ConvertDoubleToString;
import com.neginadv.sooich.utils.Constants;
import com.neginadv.sooich.utils.NotificationService;

/**
 * Created by EhsanYazdanyar on 21/07/2018.
 */

public class LocationTrack extends Service {

    private final Context mContext;
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    boolean checkGPS = false;
    boolean checkNetwork = false;
    boolean canGetLocation = false;
    private int Access_Granted = PackageManager.PERMISSION_GRANTED;
    Location loc = null;
    double latitude;
    double longitude;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;
    private static final long MIN_TIME_BW_UPDATE = 0;
    protected LocationManager locationManager = null;

    private SharedPreferences Travel_Shp;
    private SharedPreferences.Editor Travel_Editor;

    private static final String TAG = "Location Track";

    public LocationTrack(Context context) {
        this.mContext = context;
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
    }

    public Location getLocation() {

        try {

            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != Access_Granted
                    && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != Access_Granted) {
                Log.e(TAG, "request permission");
            }

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATE, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
            /*if (locationManager != null) {
                loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Log.e("gps : ", "requestLocationUpdates");
            }*/

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "exception => " + e.getMessage());

        }

        return loc;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    private boolean canGetLocation() {
        return this.canGetLocation;
    }

    public boolean CheckGpsAndNetwork() {

        checkGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        checkNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!checkGPS && !checkNetwork) {
            Log.e(TAG, ": NO Service Provider is available");
            return false;
        } else {
            Log.e(TAG, ": is available");
            return true;
        }
    }

    public void DisableListener() {
        locationManager.removeUpdates(locationListener);
        stopSelf();

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {

            CheckLastKnownLocation(location);

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    public void CheckLastKnownLocation(Location NewLocation) {

        Travel_Shp = mContext.getSharedPreferences("Short_Travel", MODE_PRIVATE);

        String T_LocationLat = Travel_Shp.getString("TempLat", null); // Get Temp Lat


        Log.e("Location", NewLocation.getLatitude() + "");

        if (NewLocation.getLatitude() != 0.0) {

            latitude = NewLocation.getLatitude();
            longitude = NewLocation.getLongitude();

            if (T_LocationLat != null) {
                if (NewLocation.getLatitude() != Double.valueOf(T_LocationLat)) {

                    // Go To The Next Mission Of Travel
                    Intent serviceIntent = new Intent(mContext, NotificationService.class);
                    serviceIntent.setAction(Constants.ACTION.N_GetLocation);
                    mContext.startService(serviceIntent);

                    Log.e(TAG, "New Location Not Equal With Temp");
                }
            } else {

                // Temp Is Null
                // Go To The Next Mission Of Travel
                Intent serviceIntent = new Intent(mContext, NotificationService.class);
                serviceIntent.setAction(Constants.ACTION.N_GetLocation);
                mContext.startService(serviceIntent);

                Log.e(TAG, "Location Temp Is Null");
            }
        }
    }

    public boolean PermissionGranted(){

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != Access_Granted
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != Access_Granted) {
            return false;
        }else{
            return true;
        }

    }

}
