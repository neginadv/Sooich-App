package com.neginadv.sooich.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.neginadv.sooich.Models.CarType;
import com.neginadv.sooich.Models.State;
import com.neginadv.sooich.Models.Taxi_Co;
import com.neginadv.sooich.Models.User_Profile;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by EhsanYazdanyar on 3/23/2018.
 */

public class DatabaseHelper {

    private SQLiteDatabase mydb, mydb_read;
    private static final String TAG = "DataBase Helper";

    public DatabaseHelper(Context context) {
        mydb = new Database(context).getWritableDatabase();
        mydb_read = new Database(context).getReadableDatabase();

        //mydb = Database.getInstance(context.getApplicationContext()).getWritableDatabase();
        //mydb_read = Database.getInstance(context.getApplicationContext()).getReadableDatabase();

    }

    public void InsertToUserProfile(List<User_Profile> list) {

        ContentValues cv = new ContentValues();

        for (int i = 0; i < list.size(); i++) {

            User_Profile user_profile = list.get(i);

            cv.put(Database.USER_NAME, user_profile.getName());
            cv.put(Database.USER_CAR, user_profile.getCar_Type());
            cv.put(Database.USER_MOBILE, user_profile.getPhone());
            cv.put(Database.USER_INCOME, user_profile.getIncome());
            cv.put(Database.USER_PROFIT, user_profile.getProfit());
            cv.put(Database.USER_CITY, user_profile.getCity());
            cv.put(Database.USER_STATE, user_profile.getState());
            cv.put(Database.USER_CAR_MODEL, user_profile.getCar_Model());
            cv.put(Database.USER_BASE_KILOMETER, user_profile.getBase_Kilometer());
            cv.put(Database.USER_MOUNT_COST, user_profile.getMount_Cost());
            cv.put(Database.USER_PRICE_TIRE, user_profile.getPrice_Tire());

            mydb.insert(Database.TABLE_PROFILE_DETAIL, null, cv);

        }
    }

    public ArrayList<User_Profile> GetUserProfile() {

        ArrayList<User_Profile> data = new ArrayList<>();

        Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_PROFILE_DETAIL, null);

        if (cr.moveToFirst()) {

            do {
                User_Profile UProfile = new User_Profile();
                UProfile.setName((cr.getString(cr.getColumnIndex(Database.USER_NAME))));
                UProfile.setPhone(cr.getString(cr.getColumnIndex(Database.USER_MOBILE)));
                UProfile.setCar_Type(cr.getString(cr.getColumnIndex(Database.USER_CAR)));
                UProfile.setIncome(cr.getString(cr.getColumnIndex(Database.USER_INCOME)));
                UProfile.setProfit(cr.getString(cr.getColumnIndex(Database.USER_PROFIT)));
                UProfile.setCity(cr.getString(cr.getColumnIndex(Database.USER_CITY)));
                UProfile.setState(cr.getString(cr.getColumnIndex(Database.USER_STATE)));
                UProfile.setCar_Model(cr.getString(cr.getColumnIndex(Database.USER_CAR_MODEL)));
                UProfile.setBase_Kilometer(cr.getString(cr.getColumnIndex(Database.USER_BASE_KILOMETER)));
                UProfile.setMount_Cost(cr.getString(cr.getColumnIndex(Database.USER_MOUNT_COST)));
                UProfile.setPrice_Tire(cr.getString(cr.getColumnIndex(Database.USER_PRICE_TIRE)));

                data.add(UProfile);
            } while (cr.moveToNext());

        }
        cr.close();

        return data;

    }

    public void updateUserrProfile(String name, String car, String mobile, String income, String profit, String created_at) {

        ContentValues cv = new ContentValues();
        cv.put(Database.USER_NAME, name);
        cv.put(Database.USER_CAR, car);
        cv.put(Database.USER_MOBILE, mobile);
        cv.put(Database.USER_INCOME, income);
        cv.put(Database.USER_PROFIT, profit);
        cv.put(Database.USER_CREATED_AT, created_at);

        mydb.update(Database.TABLE_PROFILE_DETAIL, cv, Database.USER_MOBILE + " =?", new String[]{mobile});
    }


    //////////////////////////////////////////////////////////////////////////////////////////////// User Profile
    public List<User_Profile> getAllUserProfile() {

        List<User_Profile> data = new ArrayList<User_Profile>();

        Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_PROFILE_DETAIL, null);

        if (cr.moveToFirst()) {

            do {
                User_Profile tbl_data = new User_Profile();
                tbl_data.setID(cr.getInt((cr.getColumnIndex(Database.USER_ID))));
                tbl_data.setName((cr.getString(cr.getColumnIndex(Database.USER_NAME))));
                tbl_data.setPhone(cr.getString(cr.getColumnIndex(Database.USER_MOBILE)));
                tbl_data.setCar_Type(cr.getString(cr.getColumnIndex(Database.USER_CAR)));

                data.add(tbl_data);
            } while (cr.moveToNext());

        }
        cr.close();

        return data;

    }
    //////////////////////////////////////////////////////////////////////////////////////////////// End User Profile


    //////////////////////////////////////////////////////////////////////////////////////////////// TaxiCo
    public void InsertTaxiCo(List<Taxi_Co> list) {

        ContentValues cv = new ContentValues();

        for (int i = 0; i < list.size(); i++) {

            Taxi_Co taxi_co = list.get(i);
            cv.put(Database.TAXI_EN_NAME, taxi_co.getEn_name());
            cv.put(Database.TAXI_FA_NAME, taxi_co.getFa_name());
            mydb.insert(Database.TABLE_TAXI_CO, null, cv);
        }

    }

    public ArrayList<Taxi_Co> GetAllTaxiCo() {

        ArrayList<Taxi_Co> data = new ArrayList<Taxi_Co>();

        Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_TAXI_CO, null);

        if (cr.moveToFirst()) {

            do {
                Taxi_Co tbl_data = new Taxi_Co();
                tbl_data.setEn_name((cr.getString(cr.getColumnIndex(Database.TAXI_EN_NAME))));
                tbl_data.setFa_name(cr.getString(cr.getColumnIndex(Database.TAXI_FA_NAME)));

                data.add(tbl_data);
            } while (cr.moveToNext());

        }
        cr.close();

        return data;

    }

    public String GetTaxiCoByName(String Taxi_Fa_Or_En_Name, String FA_EN) {

        String query = null;
        String Column_Name = null;
        if (FA_EN.equals("FA")) {
            query = "SELECT * FROM taxi_co WHERE fa_name = ? LIMIT 1";
            Column_Name = Database.TAXI_EN_NAME;
        } else if (FA_EN.equals("EN")) {
            query = "SELECT * FROM taxi_co WHERE en_name = ? LIMIT 1";
            Column_Name = Database.TAXI_FA_NAME;
        }

        //Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_PROFILE_DETAIL + " where mobile=" + " =?", null);
        Cursor cr = mydb_read.rawQuery(query, new String[]{Taxi_Fa_Or_En_Name});

        if (cr.isBeforeFirst()) {
            cr.moveToFirst();
        }

        if (cr != null && cr.getCount() > 0) {
            return cr.getString(cr.getColumnIndex(Column_Name));
        } else {
            return "nadarad";
        }


    }

    //////////////////////////////////////////////////////////////////////////////////////////////// End TaxiCo


    //////////////////////////////////////////////////////////////////////////////////////////////// Cars
    public void insertCars(List<CarType> list) {

        ContentValues cv = new ContentValues();

        for (int i = 0; i < list.size(); i++) {

            CarType carType = list.get(i);
            cv.put(Database.CAR_EN, carType.getCar_en());
            cv.put(Database.CAR_FA, carType.getCar_fa());
            mydb.insert(Database.TABLE_CAR_TYPE, null, cv);
        }

        Log.e(TAG, "Insert Cars Complete . ");
    }

    public ArrayList<CarType> getAllCars() {

        ArrayList<CarType> data = new ArrayList<CarType>();

        Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_CAR_TYPE, null);

        if (cr.moveToFirst()) {

            do {
                CarType car_data = new CarType();
                car_data.setCar_fa((cr.getString(cr.getColumnIndex(Database.CAR_FA))));
                car_data.setCar_en(cr.getString(cr.getColumnIndex(Database.CAR_EN)));

                data.add(car_data);
            } while (cr.moveToNext());
        }
        cr.close();

        return data;

    }

    public String GetCarsByName(String Cars_Fa_Or_En_Name, String FA_EN) {

        String query = null;
        String Column_Name = null;
        if (FA_EN.equals("FA")) {
            query = "SELECT * FROM car_type WHERE car_fa = ? LIMIT 1";
            Column_Name = Database.CAR_EN;
        } else if (FA_EN.equals("EN")) {
            query = "SELECT * FROM car_type WHERE car_en = ? LIMIT 1";
            Column_Name = Database.CAR_FA;
        }

        //Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_PROFILE_DETAIL + " where mobile=" + " =?", null);
        Cursor cr = mydb_read.rawQuery(query, new String[]{Cars_Fa_Or_En_Name});

        if (cr.isBeforeFirst()) {
            cr.moveToFirst();
        }

        if (cr != null && cr.getCount() > 0) {
            return cr.getString(cr.getColumnIndex(Column_Name));
        } else {
            return "nadarad";
        }


    }
    //////////////////////////////////////////////////////////////////////////////////////////////// End Cars


    //////////////////////////////////////////////////////////////////////////////////////////////// Base Kilometer
    public void InsertBaseKilometer(String[] Kilometer) {

        ContentValues cv = new ContentValues();
        for (int i = 0; i < Kilometer.length; i++) {
            cv.put(Database.Kilometer, Kilometer[i]);
            mydb.insert(Database.TABLE_BASE_KILOMETER, null, cv);
        }

        Log.e(TAG, "Insert Base Kilometer WAS SUCCESSFULLY");

    }

    public List GetBaseKilometer() {

        List data = new ArrayList<>();

        Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_BASE_KILOMETER, null);

        if (cr.moveToFirst()) {

            do {
                data.add(cr.getString(cr.getColumnIndex(Database.Kilometer)));
            } while (cr.moveToNext());
        }
        cr.close();

        return data;

    }
    //////////////////////////////////////////////////////////////////////////////////////////////// End Base Kilometer


    //////////////////////////////////////////////////////////////////////////////////////////////// Mount Coust
    public void InsertMountCost(String[] MountCost) {

        ContentValues cv = new ContentValues();
        for (int i = 0; i < MountCost.length; i++) {
            cv.put(Database.Mount_Cost_Value, MountCost[i]);
            mydb.insert(Database.TABLE_MOUNT_COST, null, cv);
        }

        Log.e(TAG, "Insert Mount Coust WAS SUCCESSFULLY");

    }

    public List GetMountCost() {

        List data = new ArrayList<>();

        Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_MOUNT_COST, null);

        if (cr.moveToFirst()) {

            do {
                data.add(cr.getString(cr.getColumnIndex(Database.Mount_Cost_Value)));
            } while (cr.moveToNext());
        }
        cr.close();

        return data;

    }
    //////////////////////////////////////////////////////////////////////////////////////////////// End Mount Coust


    //////////////////////////////////////////////////////////////////////////////////////////////// Price Tire
    public void InsertPrice_Tire(String[] Price_Tire) {

        ContentValues cv = new ContentValues();
        for (int i = 0; i < Price_Tire.length; i++) {
            cv.put(Database.Price_Tires, Price_Tire[i]);
            mydb.insert(Database.TABLE_PRICE_TIRE, null, cv);
        }

        Log.e(TAG, "Insert Price Tire WAS SUCCESSFULLY");

    }

    public List GetPrice_Tire() {

        List data = new ArrayList<>();

        Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_PRICE_TIRE, null);

        if (cr.moveToFirst()) {

            do {
                data.add(cr.getString(cr.getColumnIndex(Database.Price_Tires)));
            } while (cr.moveToNext());
        }
        cr.close();

        return data;

    }
    //////////////////////////////////////////////////////////////////////////////////////////////// End Price Tire


    //////////////////////////////////////////////////////////////////////////////////////////////// Fuel Efficiency
    public void InsertFuel_Efficiency(String[] Fuel_Efficiency) {

        ContentValues cv = new ContentValues();
        for (int i = 0; i < Fuel_Efficiency.length; i++) {
            cv.put(Database.Fuel_Efficiency, Fuel_Efficiency[i]);
            mydb.insert(Database.TABLE_FUEL_EFFICIENCY, null, cv);
        }

        Log.e(TAG, "Insert Fuel Efficiency WAS SUCCESSFULLY");

    }

    public List GetFuel_Efficiency() {

        List data = new ArrayList<>();

        Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_FUEL_EFFICIENCY, null);

        if (cr.moveToFirst()) {

            do {
                data.add(cr.getString(cr.getColumnIndex(Database.Fuel_Efficiency)));
            } while (cr.moveToNext());
        }
        cr.close();

        return data;

    }
    //////////////////////////////////////////////////////////////////////////////////////////////// End Fuel Efficiency


    //////////////////////////////////////////////////////////////////////////////////////////////// State/////////////////////////////////////////////////////
    public void InsertState(List<State> list) {

        ContentValues cv = new ContentValues();

        for (int i = 0; i < list.size(); i++) {

            State state = list.get(i);
            cv.put(Database.STATE_EN, state.getEn_Name());
            cv.put(Database.STATE_FA, state.getFa_Name());
            mydb.insert(Database.TABLE_STATE, null, cv);
        }

        Log.e(TAG, "Insert States Complete . ");
    }

    public ArrayList<State> GetAllState() {

        ArrayList<State> data = new ArrayList<State>();

        Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_STATE, null);

        if (cr.moveToFirst()) {

            do {
                State state = new State();
                state.setFa_Name((cr.getString(cr.getColumnIndex(Database.STATE_FA))));
                state.setEn_Name(cr.getString(cr.getColumnIndex(Database.STATE_EN)));

                data.add(state);
            } while (cr.moveToNext());
        }
        cr.close();

        return data;

    }

    public String GetStateByName(String State_Fa_Or_En_Name, String FA_EN) {

        String query = null;
        String Column_Name = null;
        if (FA_EN.equals("FA")) {
            query = "SELECT * FROM state_Tbl WHERE state_fa = ? LIMIT 1";
            Column_Name = Database.STATE_EN;
        } else if (FA_EN.equals("EN")) {
            query = "SELECT * FROM state_Tbl WHERE state_en = ? LIMIT 1";
            Column_Name = Database.STATE_FA;
        }

        //Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_PROFILE_DETAIL + " where mobile=" + " =?", null);
        Cursor cr = mydb_read.rawQuery(query, new String[]{State_Fa_Or_En_Name});

        if (cr.isBeforeFirst()) {
            cr.moveToFirst();
        }

        if (cr != null && cr.getCount() > 0) {
            return cr.getString(cr.getColumnIndex(Column_Name));
        } else {
            return "nadarad";
        }


    }
    //////////////////////////////////////////////////////////////////////////////////////////////// End State


    public String getUserId(String phone) {

        //Cursor cr = mydb_read.rawQuery("select * from " + Database.TABLE_PROFILE_DETAIL + " where mobile=" + " =?", null);
        String query = "SELECT * FROM profile_detail WHERE mobile = ? LIMIT 1";
        Cursor cr = mydb_read.rawQuery(query, new String[]{phone});

        if (cr.isBeforeFirst()) {
            cr.moveToFirst();
        }

        if (cr != null && cr.getCount() > 0) {
            return cr.getString(cr.getColumnIndex(Database.USER_ID)) + " - " + cr.getString(cr.getColumnIndex(Database.USER_NAME));
        } else {
            return "nadarad";
        }


    }


    //////////////////////////////////////////////////////////////////////////////////////////////// Delete From Tbl
    public void DeleteFromTbl(String table) {

        mydb.delete(table, null, null);
        //mydb.execSQL("delete from " + table);
        Log.e(TAG, "Table: " + table + " Rows Removed Successfully.");

    }
    //////////////////////////////////////////////////////////////////////////////////////////////// End Delete From Tbl


    //////////////////////////////////////////////////////////////////////////////////////////////// Close Db
    public void closeDb() {

        if (mydb != null && mydb.isOpen()) {
            mydb.close();
        }

        if (mydb_read != null && mydb_read.isOpen()) {
            mydb_read.close();
        }

    }

}
