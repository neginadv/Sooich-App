package com.neginadv.sooich.utils;

import android.content.Context;
import android.util.Log;

import com.neginadv.sooich.Models.CarType;
import com.neginadv.sooich.Models.City;
import com.neginadv.sooich.Models.State;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by EhsanYazdanyar on 11/26/2018.
 */

public class ReadJsonFromFile {

    private Context c;
    private static final String TAG = ReadJsonFromFile.class.getSimpleName();

    private static final String State_File = "province.json";
    private static final String City_File = "cities.json";
    private static final String Car_Type = "car_type.json";

    public ReadJsonFromFile(Context context) {
        this.c = context;
    }

    private String GetFile(String FileName) {

        String json = null;
        try {
            InputStream file = c.getAssets().open(FileName);
            int size = file.available();
            byte[] buffer = new byte[size];
            file.read(buffer);
            file.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return json;
    }


    /* State */
    public List<State> GetState() {

        List<State> states = new ArrayList<>();

        try {

            JSONArray jsonArray = new JSONArray(GetFile(State_File));
            for (int i = 0; i < jsonArray.length(); i++) {
                State M_States = new State();
                M_States.setId(jsonArray.getJSONObject(i).getInt("id"));
                M_States.setFa_Name(jsonArray.getJSONObject(i).getString("title"));
                M_States.setEn_Name(jsonArray.getJSONObject(i).getString("slug"));
                states.add(M_States);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return null;
        }

        return states;

    }

    public State GetStateByName(String Name) {

        State state = new State();

        try {

            JSONArray jsonArray = new JSONArray(GetFile(State_File));
            for (int i = 0; i < jsonArray.length(); i++) {

                String State_Fa = jsonArray.getJSONObject(i).getString("title");
                String State_En = jsonArray.getJSONObject(i).getString("slug");

                if (Name.equals(State_Fa) || Name.equals(State_En)) {
                    state.setFa_Name(State_Fa);
                    state.setEn_Name(State_En);
                    state.setId(jsonArray.getJSONObject(i).getInt("id"));
                    break;
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return null;
        }

        return state;

    }


    /* City */
    public List<City> GetCities() {

        List<City> Cities = new ArrayList<>();

        try {

            JSONArray jsonArray = new JSONArray(GetFile(City_File));
            for (int i = 0; i < jsonArray.length(); i++) {
                City M_City = new City();
                M_City.setId(jsonArray.getJSONObject(i).getInt("id"));
                M_City.setFa_Name(jsonArray.getJSONObject(i).getString("title"));
                M_City.setEn_Name(jsonArray.getJSONObject(i).getString("slug"));
                M_City.setState_id(jsonArray.getJSONObject(i).getInt("province_id"));
                Cities.add(M_City);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return null;
        }

        return Cities;

    }

    public City GetCityByName(String Name) {

        City city = new City();

        try {

            JSONArray jsonArray = new JSONArray(GetFile(City_File));
            for (int i = 0; i < jsonArray.length(); i++) {

                String city_Fa = jsonArray.getJSONObject(i).getString("title");
                String city_En = jsonArray.getJSONObject(i).getString("slug");

                if (Name.equals(city_Fa) || Name.equals(city_En)) {
                    city.setFa_Name(city_Fa);
                    city.setEn_Name(city_En);
                    city.setId(jsonArray.getJSONObject(i).getInt("id"));
                    break;
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return null;
        }

        return city;

    }

    public List<String> GetCityByStateId(int id) {

        List<String> City_List = new ArrayList<>();

        try {

            JSONArray jsonArray = new JSONArray(GetFile(City_File));
            for (int i = 0; i < jsonArray.length(); i++) {

                int State_id = jsonArray.getJSONObject(i).getInt("province_id");

                if (id == State_id) {
                    City_List.add(jsonArray.getJSONObject(i).getString("title"));
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return null;
        }

        return City_List;

    }


    /* Cars */
    public List<CarType> GetCars() {

        List<CarType> Cars_List = new ArrayList<>();

        try {

            JSONArray jsonArray = new JSONArray(GetFile(Car_Type));
            for (int i = 0; i < jsonArray.length(); i++) {
                CarType M_CarType = new CarType();
                M_CarType.setId(jsonArray.getJSONObject(i).getInt("id"));
                M_CarType.setCar_fa(jsonArray.getJSONObject(i).getString("fa"));
                M_CarType.setCar_en(jsonArray.getJSONObject(i).getString("en"));
                Cars_List.add(M_CarType);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return null;
        }

        return Cars_List;

    }

    public CarType GetCarByName(String Name) {

        CarType Cars_Type = new CarType();

        try {

            JSONArray jsonArray = new JSONArray(GetFile(Car_Type));
            for (int i = 0; i < jsonArray.length(); i++) {

                String Car_Type_Fa = jsonArray.getJSONObject(i).getString("fa");
                String Car_Type_En = jsonArray.getJSONObject(i).getString("en");

                if (Name.equals(Car_Type_Fa) || Name.equals(Car_Type_En)) {
                    Cars_Type.setCar_fa(Car_Type_Fa);
                    Cars_Type.setCar_en(Car_Type_En);
                    Cars_Type.setId(jsonArray.getJSONObject(i).getInt("id"));
                    break;
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return null;
        }

        return Cars_Type;

    }

}
