package com.neginadv.sooich.utils;

import android.app.Activity;
import android.view.View;
import android.view.ViewTreeObserver;

import com.neginadv.sooich.R;
import com.neginadv.sooich.classes.KeyBoardVisibilityListener;

/**
 * Created by Ehsan on 11/21/2018.
 */

public class KeyboardListener {

    static int mAppHeight;

    public static void setKeyboardVisibilityListener(final Activity activity, final int MainView , final KeyBoardVisibilityListener keyBoardVisibilityListener) {

        final View contentView = activity.findViewById(MainView);

        contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            private int previousHeight;

            @Override
            public void onGlobalLayout() {

                int NewHeight = contentView.getHeight();

                if (NewHeight == previousHeight) {
                    return;
                }

                previousHeight = NewHeight;

                if (NewHeight >= mAppHeight) {

                    mAppHeight = NewHeight;

                }

                if (NewHeight != 0) {
                    if (mAppHeight > NewHeight) {
                        keyBoardVisibilityListener.onKeyboardVisibilityChange(true);

                    } else {
                        keyBoardVisibilityListener.onKeyboardVisibilityChange(false);
                    }
                }

            }
        });

    }

}
