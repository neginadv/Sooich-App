package com.neginadv.sooich.classes;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.neginadv.sooich.R;

/**
 * Created by EhsanYazdanyar on 4/22/2018.
 */

public class AnimateFadeInFadeOut {

    private Animation FadeIn;
    private Animation FadeOut;

    private View view;
    private Handler Handler = new Handler();

    public AnimateFadeInFadeOut(Context c, View view) {
        this.view = view;
        FadeIn = AnimationUtils.loadAnimation(c, R.anim.fadein);
        FadeOut = AnimationUtils.loadAnimation(c, R.anim.fadeout);
    }

    public void start() {
        Handler.removeCallbacks(Update);
        Handler.post(Update);
    }

    public void stop() {
        Handler.removeCallbacks(Update);
    }

    private Runnable Update = new Runnable() {
        public void run() {

            view.startAnimation(FadeOut);
            FadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation arg0) {
                }

                @Override
                public void onAnimationRepeat(Animation arg0) {
                }

                @Override
                public void onAnimationEnd(Animation arg0) {
                    view.startAnimation(FadeIn);
                }
            });

            Handler.postDelayed(Update, 2000);

        }
    };


}
