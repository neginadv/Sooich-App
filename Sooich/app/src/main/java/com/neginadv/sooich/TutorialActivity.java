package com.neginadv.sooich;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.neginadv.sooich.classes.ToastCustom;
import com.neginadv.sooich.classes.TutorialFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TutorialActivity extends FragmentActivity {

    private ViewPager viewPager;
    private ImageButton TT_Next , TT_Prev;
    Button Skip_Btn;
    private View indicator1;
    private View indicator2;
    private View indicator3;
    private View indicator4;
    private View indicator5;
    private ToastCustom toast;
    private SharedPreferences shp;
    private SharedPreferences.Editor editor;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ini();

        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        viewPager.setOnPageChangeListener(new PageChangeListener());
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        updateIndicators(0);

    }

    private void ini() {

        indicator1 = (View) findViewById(R.id.TT_indicator1);
        indicator2 = (View) findViewById(R.id.TT_indicator2);
        indicator3 = (View) findViewById(R.id.TT_indicator3);
        indicator4 = (View) findViewById(R.id.TT_indicator4);
        indicator5 = (View) findViewById(R.id.TT_indicator5);

        viewPager = (ViewPager) findViewById(R.id.TT_ViewPager);

        TT_Next = (ImageButton)findViewById(R.id.tt_next);
        TT_Prev = (ImageButton)findViewById(R.id.tt_prev);

        Skip_Btn = (Button)findViewById(R.id.TT_Skip_Btn);

        toast = new ToastCustom();
        shp = getSharedPreferences("FirstUse", MODE_PRIVATE);
        editor = shp.edit();

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private int Pages_count = 5;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new TutorialFragment(position);
        }

        @Override
        public int getCount() {
            return Pages_count;
        }

    }

    private class PageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            updateIndicators(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }

    }

    public void updateIndicators(int position) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int resizeValue = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, metrics);
        int defaultValue = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, metrics);

        switch (position) {

            case 0:

                ActionViews(indicator1 , resizeValue , true);
                ActionViews(indicator2 , defaultValue , false);
                ActionViews(indicator3 , defaultValue , false);
                ActionViews(indicator4 , defaultValue , false);
                ActionViews(indicator5 , defaultValue , false);

                break;

            case 1:

                ActionViews(indicator1 , defaultValue , false);
                ActionViews(indicator2 , resizeValue , true);
                ActionViews(indicator3 , defaultValue , false);
                ActionViews(indicator4 , defaultValue , false);
                ActionViews(indicator5 , defaultValue , false);


                break;

            case 2:

                ActionViews(indicator1 , defaultValue , false);
                ActionViews(indicator2 , defaultValue , false);
                ActionViews(indicator3 , resizeValue , true);
                ActionViews(indicator4 , defaultValue , false);
                ActionViews(indicator5 , defaultValue , false);

                break;

            case 3:

                ActionViews(indicator1 , defaultValue , false);
                ActionViews(indicator2 , defaultValue , false);
                ActionViews(indicator3 , defaultValue , false);
                ActionViews(indicator4 , resizeValue , true);
                ActionViews(indicator5 , defaultValue , false);

                break;

            case 4:

                ActionViews(indicator1 , defaultValue , false);
                ActionViews(indicator2 , defaultValue , false);
                ActionViews(indicator3 , defaultValue , false);
                ActionViews(indicator4 , defaultValue , false);
                ActionViews(indicator5 , resizeValue , true);

                break;
        }

    }

    private void ActionViews(View view , int Value ,boolean bg_colorize){

        view.getLayoutParams().height = Value;
        view.getLayoutParams().width = Value;
        view.requestLayout();


        if (bg_colorize){
            view.setBackgroundResource(R.drawable.tt_indicator_bg_color);
        }else{
            view.setBackgroundResource(R.drawable.tt_indicator_bg);
        }

    }

    public void Next_TT(View v){

        int LastPosition = viewPager.getCurrentItem();

        if (LastPosition == 4){
            editor.putString("Status" , "Used");
            editor.commit();
            Intent intent = new Intent(TutorialActivity.this , LoginAndReg.class);
            startActivity(intent);
        }else{
            viewPager.setCurrentItem(LastPosition + 1, true);
        }

    }

    public void Prev_TT(View v){

        int LastPosition = viewPager.getCurrentItem();
        viewPager.setCurrentItem(LastPosition - 1, true);

    }

    private Boolean exit = false;
    @Override
    public void onBackPressed() {

        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            toast.viewToast(TutorialActivity.this , getResources().getString(R.string.Exit_Txt));
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void SkipTT_WithBtn(View view){

        editor.putString("Status" , "Used");
        editor.commit();
        Intent intent = new Intent(TutorialActivity.this , LoginAndReg.class);
        startActivity(intent);

    }
}
