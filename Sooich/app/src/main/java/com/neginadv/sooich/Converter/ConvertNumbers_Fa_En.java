package com.neginadv.sooich.Converter;

/**
 * Created by EhsanYazdanyar on 5/7/2018.
 */

public class ConvertNumbers_Fa_En {

    public String EN_TO_FA(String Num){

        char[] PersianNums = {'٠','١','٢','٣','٤','٥','٦','٧','٨','٩'};
        StringBuilder builder = new StringBuilder();
        for(int i =0;i<Num.length();i++){
            if(Character.isDigit(Num.charAt(i))){
                builder.append(PersianNums[(int)(Num.charAt(i))-48]);
            }
            else{
                builder.append(Num.charAt(i));
            }
        }

        return builder.toString();
    }

    public String FA_TO_EN(String number) {
        char[] chars = new char[number.length()];
        for(int i=0;i<number.length();i++) {
            char ch = number.charAt(i);
            if (ch >= 0x0660 && ch <= 0x0669)
                ch -= 0x0660 - '0';
            else if (ch >= 0x06f0 && ch <= 0x06F9)
                ch -= 0x06f0 - '0';
            chars[i] = ch;
        }
        return new String(chars);
    }
}
