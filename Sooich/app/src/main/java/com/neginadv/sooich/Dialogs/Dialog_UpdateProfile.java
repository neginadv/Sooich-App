package com.neginadv.sooich.Dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.Models.CarType;
import com.neginadv.sooich.Models.City_Names;
import com.neginadv.sooich.Models.State;
import com.neginadv.sooich.Models.User_Profile;
import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.VolleyErrorHelper;
import com.neginadv.sooich.classes.GetSharedPreferencesData;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.ToastCustom;
import com.neginadv.sooich.helper.DatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by EhsanYazdanyar on 13/06/2018.
 */

public class Dialog_UpdateProfile {

    private static AlertDialog.Builder builder;
    private static View v;
    private ProgressBar progress;
    private AlertDialog d;
    private String Token = "";
    private EditText UserName_Edt, PhonNumber_Edt;
    private Button Send_Data, Close;
    private DatabaseHelper data;
    private ConvertNumbers_Fa_En FaNum;

    private Spinner Car_Type_Spin, State_Spinner, City_Spinner;
    private ProgressBar City_Progress;
    private boolean spinner_access = false;
    ToastCustom toast = new ToastCustom();

    private static final String TAG = "Dialog Update Profile";

    private String City = "";
    private String State = "";

    private List<City_Names> City_array = null;

    public Dialog_UpdateProfile(final Activity activity, final View B_Black) {

        builder = new AlertDialog.Builder(activity, R.style.Material_Dialog);
        builder.setCancelable(false);

        LayoutInflater inflater = LayoutInflater.from(activity);
        final View view = inflater.inflate(R.layout.update_profile_dialog, null);
        ini(activity, view);

        State_Spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                spinner_access = true;
                return false;
            }
        });

        State_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("LongLogTag")
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                if (spinner_access){
                    City_array = new ArrayList<>();
                    String State_FaName = State_Spinner.getSelectedItem().toString();
                    String State_EnName = data.GetStateByName(State_FaName, "FA");
                    GetCity(activity, State_EnName, State_FaName, "");
                    Log.e(TAG, State_EnName);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        builder.setView(view);
        v = view;

        List<User_Profile> User = data.GetUserProfile();
        if (User.size() > 0) {

            User_Profile user_profile = User.get(0);

            String User_Name = user_profile.getName();
            String User_Mobile = user_profile.getPhone();
            String User_Car = user_profile.getCar_Type();
            String User_Income = user_profile.getIncome();
            String User_Profit = user_profile.getProfit();
            String User_CarModel = user_profile.getCar_Model();
            String User_BaseKilometer = user_profile.getBase_Kilometer();
            String User_MountCost = user_profile.getMount_Cost();
            City = user_profile.getCity();
            State = user_profile.getState();


            UserName_Edt.setText(User_Name);
            PhonNumber_Edt.setText(User_Mobile);
            PhonNumber_Edt.setEnabled(false);

            SetDataIntoSpinners(activity, User_Car, null, "car_type");
            SetDataIntoSpinners(activity, State, null, "state");
            SetDataIntoSpinners(activity, City, State, "city");

//            Log.e(TAG, "User_Name !!!" + User_Name);
//            Log.e(TAG, "User_Mobile : " + User_Mobile);
//            Log.e(TAG, "User_Car : " + User_Car);
//            Log.e(TAG, "User_Income : " + User_Income);
//            Log.e(TAG, "User_Profit : " + User_Profit);
//            Log.e(TAG, "City : " + City);
//            Log.e(TAG, "State : " + State);

        } else {

            Log.e(TAG, "No Res !!!");
        }

        Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
                B_Black.setVisibility(View.INVISIBLE);

                /*if (Sabt_Safar == 1){
                    activity.recreate();
                }*/
            }
        });

        Send_Data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e(TAG , "Send Data Started");

                String UserName = UserName_Edt.getText().toString().trim();
                String PhonNumber = PhonNumber_Edt.getText().toString().trim();
                String Car = Car_Type_Spin.getSelectedItem().toString().trim();
                String State = State_Spinner.getSelectedItem().toString().trim();
                String City = City_Spinner.getSelectedItem().toString().trim();

                if (UserName.isEmpty()) {
                    toast.viewToast(activity, activity.getResources().getString(R.string.Complete_Reg_name));

                }else if (City.equals("NoVal")){
                    toast.viewToast(activity, activity.getResources().getString(R.string.Complete_Reg_Error_City_Choose));

                }else{

                    String En_Name_City = null;
                    if (City_array.size() > 0) {

                        for (int i = 0; i < City_array.size(); i++) {
                            String Array_Res = City_array.get(i).getFa_Name();
                            if (City.equals(Array_Res)) {
                                En_Name_City = City_array.get(i).getEn_Name();
                            }
                        }
//                        Log.e(TAG, En_Name_City);
                    }else{
                        String State_FaName = State_Spinner.getSelectedItem().toString();
                        En_Name_City = data.GetStateByName(State_FaName, "FA");
//                        Log.e(TAG, En_Name_City);
                    }

                    Log.e(TAG , UserName);
                    Log.e(TAG , PhonNumber);
                    Log.e(TAG , data.GetCarsByName(Car , "FA"));
                    Log.e(TAG , data.GetStateByName(State, "FA"));
                    Log.e(TAG , En_Name_City);


                    JSONObject Obj = new JSONObject();
                    try {
                        Obj.put("fullname", UserName);
//                        Obj.put("user_pass", user_pass);
//                        Obj.put("phoneNumber", PhonNumber);
                        Obj.put("state", data.GetStateByName(State, "FA"));
                        Obj.put("city", En_Name_City);
                        Obj.put("car_type", data.GetCarsByName(Car , "FA"));
//                        Obj.put("car_type_models", car_year);
//                        Obj.put("base_kilometer", Kilometer_Num);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Update_Profile(activity, Obj);
                        System.out.println(Obj);
//                    Update(CompleteRegisterActivity.this, Obj);

                }

            }
        });


        B_Black.setVisibility(View.VISIBLE);
        d = builder.show();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    private void ini(Activity activity, View view) {
//        Token = new GetSharedPreferencesData(activity).GetLoginData();
        data = new DatabaseHelper(activity);
        UserName_Edt = (EditText) view.findViewById(R.id.DUP_UserName_Edt);
        PhonNumber_Edt = (EditText) view.findViewById(R.id.DUP_PhonNumber_Edt);
        Car_Type_Spin = (Spinner) view.findViewById(R.id.DUP_Car_Spinner);
        State_Spinner = (Spinner) view.findViewById(R.id.DUP_State_Spinner);
        City_Spinner = (Spinner) view.findViewById(R.id.DUP_City_Spinner);
        Send_Data = (Button) view.findViewById(R.id.DUP_Send_Data);
        progress = (ProgressBar) view.findViewById(R.id.DUP_dialog_progress);
        Close = (Button) view.findViewById(R.id.DUP_dialog_close_Btn);
        FaNum = new ConvertNumbers_Fa_En();

        City_Progress = (ProgressBar) view.findViewById(R.id.DUP_City_Spinner_progress);
    }

    private void SetDataIntoSpinners(Activity activity, String value, String value2, String Spin) {

        switch (Spin) {

            case "car_type":

                ArrayList<CarType> arrayList = data.getAllCars();
                if (arrayList.size() > 0) {
                    List Cars = new ArrayList<>();
                    for (int i = 0; i < arrayList.size(); i++) {
                        Cars.add(arrayList.get(i).getCar_fa());
                    }
                    setSpinnerToAdapter(activity, Car_Type_Spin, Cars, value);
                } else {
                    Log.e(TAG, "CarType is null");
                }

                break;

            case "state":

                ArrayList<State> State_Array = data.GetAllState();
                if (State_Array.size() > 0) {
                    List states = new ArrayList<>();
                    for (int i = 0; i < State_Array.size(); i++) {
                        states.add(State_Array.get(i).getFa_Name());
                    }
                    setSpinnerToAdapter(activity, State_Spinner, states, data.GetStateByName(value, "EN"));
                } else {
                    Log.e(TAG, "State is null");
                }

                break;

            case "city":

                GetCity(activity, State, data.GetStateByName(value, "EN"), City);

                break;

        }

    }

    private void GetCity(final Activity activity, final String State_En, final String State_Fa, final String value) {

        City_Progress.setVisibility(View.VISIBLE);
        City_Spinner.setAdapter(null);
        Send_Data.setEnabled(false);

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                ServiceApi.Get_Default_Val, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(final JSONObject response) {

                City_Progress.setVisibility(View.INVISIBLE);
                Send_Data.setEnabled(true);

                try {

                    JSONObject object = response.getJSONObject("city");
                    JSONObject jobj = object.getJSONObject(State_En);
                    List fa_Names = null;
                    City_array = new ArrayList<>();

                    if (jobj.length() > 0) {
                        // Use Car Model For Citys
                        int i = 1;
                        Iterator iterator = jobj.keys();
                        while (iterator.hasNext()) {
                            String key = (String) iterator.next();

                            City_Names Cities = new City_Names();
                            Cities.setEn_Name(key);
                            Cities.setFa_Name(jobj.getString(key));

                            City_array.add(Cities);
                            i = i + 1;
                        }

                        fa_Names = new ArrayList<>();

                        String new_value = null;
                        for (int x = 0; x < City_array.size(); x++) {
                            fa_Names.add(City_array.get(x).getFa_Name());
                            if (value.equals(City_array.get(x).getEn_Name())) {
                                new_value = City_array.get(x).getFa_Name();
                            }
                        }

                        setSpinnerToAdapter(activity, City_Spinner, fa_Names, new_value);
                    } else {
                        fa_Names = new ArrayList<>();
                        fa_Names.add(State_Fa);
                        setSpinnerToAdapter(activity, City_Spinner, fa_Names, "");
                    }

                    Log.e("res", jobj + "");

                } catch (JSONException e) {
                    List No_Val = new ArrayList<>();
                    No_Val.add("NoVal");
                    setSpinnerToAdapter(activity, City_Spinner, No_Val, "");
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

//                City_Progress.setVisibility(View.INVISIBLE);
//                Send_Data.setEnabled(true);
                Log.e("error", String.valueOf(error));

                new VolleyErrorHelper().getMessage(error, activity, null, null);

                d.dismiss();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("APITOKEN", ServiceApi.API_TOKEN);

                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Reg_Tag = "GetCity_UpdateProfilr_Tag";
        RequestController.getInstance().addToRequestQueue(strReq, Reg_Tag);

    }

    public void setSpinnerToAdapter(final Activity activity, Spinner spin, List<String> list, String value) {

        final int Spinner_Lengh = (list.size()) - 1;

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setGravity(Gravity.CENTER);
                ((TextView) v).setTextColor(activity.getResources().getColor(R.color.weather_txt));
                ((TextView) v).setTextSize(12);

                return v;
            }

            @SuppressLint("LongLogTag")
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) v;
                /*if (position == 0) {
                    // Hide the second item from Spinner
                    tv.setVisibility(View.INVISIBLE);
                } else {

                    tv.setVisibility(View.VISIBLE);
                    tv.setGravity(Gravity.CENTER);
                    tv.setPadding(0, 30, 0, 30);
                    tv.setBackgroundDrawable(getResources().getDrawable(R.drawable.spinner_inner_tv));
                }*/
                tv.setVisibility(View.VISIBLE);
                tv.setGravity(Gravity.CENTER);
                tv.setPadding(0, 20, 0, 20);
                tv.setTextSize(12);

                if (position == Spinner_Lengh) {
                    tv.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.spinner_inner_tv2));
                } else {
                    tv.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.spinner_inner_tv));
                }

//                Log.e(TAG , Spinner_Lengh+"");

                return v;
            }

            // Disable the first item from Spinner
//            @Override
//            public boolean isEnabled(int position) {
//                if (position == 0) {
//                    return false;
//                } else {
//                    return true;
//                }
//            }

        };
        spin.setAdapter(dataAdapter);

        if (value != null) {
            for (int i = 0; i < dataAdapter.getCount(); i++) {
                if (value.trim().equals(dataAdapter.getItem(i).toString())) {
                    spin.setSelection(i);
                    break;
                }
            }
        }

    }

    public void Update_Profile(final Activity activity, JSONObject object) {

        progress.setVisibility(View.VISIBLE);
        Send_Data.setEnabled(false);
        Close.setEnabled(false);

        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.PUT,
                ServiceApi.User_Profile, object, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                progress.setVisibility(View.INVISIBLE);
                Send_Data.setEnabled(true);
                Close.setEnabled(true);

//                try {
//
//                    if (response.getString("status").equals("ok")){
//                        Send_Data.setVisibility(View.GONE);
//                        progress.setVisibility(View.GONE);
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }

                Log.e(TAG , "response:  " + String.valueOf(response));

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.INVISIBLE);
                Send_Data.setEnabled(true);
                Close.setEnabled(true);

                new VolleyErrorHelper().getMessage(error , activity , null , null);

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", Token);
                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        String Reg_Tag = "Submit_Travel_Tag";
        RequestController.getInstance().addToRequestQueue(strReq , Reg_Tag);

    }


}
