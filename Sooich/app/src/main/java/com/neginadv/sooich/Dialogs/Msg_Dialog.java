package com.neginadv.sooich.Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neginadv.sooich.R;

/**
 * Created by EhsanYazdanyar on 3/6/2018.
 */

public class Msg_Dialog extends Dialog {

    private static AlertDialog d;
    private ProgressBar pb;
    public Button OK_Btn, cancel_Btn , close_Btn;
    private TextView DM_Txt;
    private View Bg;
    private RelativeLayout Close_Rl , Check_Rl;
    public CheckBox checkBox;

    Msg_Ok_Btn OK_BTN;
    MSG_Cancel_Btn CANCEL_BTN;

    public Msg_Dialog(final Activity activity , final String Msg_Txt , final View bg , final boolean Closebtn , final boolean CheckBox , final String CheckBox_Txt) {
        super(activity);
        this.Bg = bg;

        final EditText edt_username, edt_password;
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.Material_Dialog);

        LayoutInflater inflater = LayoutInflater.from(activity);
        View view = inflater.inflate(R.layout.dialog_msg, null);
        builder.setView(view);

        OK_Btn = view.findViewById(R.id.DM_Ok);
        cancel_Btn = view.findViewById(R.id.DM_Cancel);
        close_Btn = view.findViewById(R.id.DM_dialog_close_Btn);
        DM_Txt = view.findViewById(R.id.DM_Txt);
        Close_Rl = view.findViewById(R.id.DVT_dialog_close_rl);
        Check_Rl = view.findViewById(R.id.DM_Check_Area);
        checkBox = view.findViewById(R.id.DM_Check);

        TextView CheckBox_TextView = view.findViewById(R.id.DM_Check_Txt);
        CheckBox_TextView.setText(CheckBox_Txt);

        bg.setVisibility(View.VISIBLE);
        DM_Txt.setText(Msg_Txt);


        if (!Closebtn){
            Close_Rl.setVisibility(View.GONE);
        }

        if (!CheckBox){
            Check_Rl.setVisibility(View.GONE);
        }

        close_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bg.setVisibility(View.INVISIBLE);
                d.dismiss();
            }
        });

        cancel_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CANCEL_BTN.GetCancelAction();
//                bg.setVisibility(View.INVISIBLE);
//                d.dismiss();
            }
        });

        OK_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OK_BTN.GetOkAction();
//                bg.setVisibility(View.INVISIBLE);
//                d.dismiss();
            }
        });

        d = builder.show();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.setCancelable(false);

    }

    public void SetOkDialogResult(Msg_Ok_Btn msg_ok_btn){
        OK_BTN = msg_ok_btn;
    }

    public void SetCancelDialogResult(MSG_Cancel_Btn msg_cancel_btn){
        CANCEL_BTN = msg_cancel_btn;
    }

    public interface Msg_Ok_Btn{
        void GetOkAction();
    }

    public interface MSG_Cancel_Btn{
        void GetCancelAction();
    }

    public void CloseDialog(){
        d.dismiss();
        Bg.setVisibility(View.INVISIBLE);
    }



}