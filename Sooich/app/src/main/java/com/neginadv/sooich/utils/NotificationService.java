package com.neginadv.sooich.utils;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.app.Service;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.neginadv.sooich.MainActivity;
import com.neginadv.sooich.Quick_Travel;
import com.neginadv.sooich.R;
import com.neginadv.sooich.classes.LocationTrack;
import com.neginadv.sooich.classes.ToastCustom;

import java.lang.reflect.Method;
import java.sql.Time;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by EhsanYazdanyar on 02/07/2018.
 */

public class NotificationService extends Service {

    Notification status;
    private SharedPreferences Travel_Shp;
    private SharedPreferences.Editor Travel_Editor;
    private String Current_Hour, Current_Min = "";
    private LocationTrack Location = null;
    private boolean Waiting = true;

    final static String TAG = "Notification Service";

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Travel_Shp = getSharedPreferences("Short_Travel", MODE_PRIVATE);
        Travel_Editor = Travel_Shp.edit();

        if (intent.getAction().equals(Constants.ACTION.STARTFOREGROUND_ACTION)) { ////////////////// Start First Notify

            int Travel_Status = Travel_Shp.getInt("Status", 0);

            Log.e(TAG, Travel_Status + "");

            if (Travel_Status == 1) {
                ShortTravel_Second();
            } else {
                ShortTravel_First();
            }

        }
        if (intent.getAction().equals(Constants.ACTION.N_GetLocation)) { ///////////////////////////GetLocation

            Log.e(TAG, "Location Track Stopped By GetLocation");
            Location.DisableListener();

            String Travel = Travel_Shp.getString("Travel", null);

            if (Travel != null) {

                if (Travel.equals("First")) {

                    Travel_Editor.putString("F_LocLat", String.valueOf(Location.getLatitude())); // Add Latitude Value To Travel SHP
                    Travel_Editor.putString("F_LocLong", String.valueOf(Location.getLongitude())); // Add Longitude Value To Travel SHP
                    Travel_Editor.putString("Travel", "End");
                    Travel_Editor.putString("TempLat", String.valueOf(Location.getLatitude())); //Store Last Lat For Next Location Checking
                    Travel_Editor.apply();

                    ShortTravel_Second();

                } else if (Travel.equals("End")) {

                    Travel_Editor.putString("E_LocLat", String.valueOf(Location.getLatitude())); // Add Latitude Value To Travel SHP
                    Travel_Editor.putString("E_LocLong", String.valueOf(Location.getLongitude())); // Add Longitude Value To Travel SHP
                    Travel_Editor.putString("TempLat", String.valueOf(Location.getLatitude())); //Store Last Lat For Next Location Checking
                    Travel_Editor.apply();

                    QuickTravelActivity(); // Now We Have End Location Value And Going To Complete Travel Activity

                }

            }

            Log.e(TAG, "Lat : " + Location.getLatitude());
            Log.e(TAG, "Long : " + Location.getLongitude());

        } else if (intent.getAction().equals(Constants.ACTION.N_SkipLocationSearching)) { /////////////////// Skip Location Searching

            Log.e(TAG, "Skipped Location Searching By User");
            Location.DisableListener();
            String Travel = Travel_Shp.getString("Travel", null);

            if (Travel != null) {

                if (Travel.equals("First")) {
                    ShortTravel_Second();
                } else if (Travel.equals("End")) {
                    QuickTravelActivity();
                }

            }

            Toast.makeText(this, "skip", Toast.LENGTH_SHORT).show();

        } else if (intent.getAction().equals(Constants.ACTION.STOPFOREGROUND_ACTION)) { //////////// Stop ForGround Service

            Log.i(TAG, "Received Stop Foreground Intent");
//            Toast.makeText(this, "Service Stoped", Toast.LENGTH_SHORT).show();
            stopForeground(true);
            stopSelf();

        } else if (intent.getAction().equals(Constants.ACTION.N_StartTravel)) { //////////////////// Start Travel

            Calendar Now = Calendar.getInstance();
            Current_Hour = String.valueOf(Now.get(Calendar.HOUR_OF_DAY));
            Current_Min = String.valueOf(Now.get(Calendar.MINUTE));

            if (Current_Hour.trim().length() < 2) {
                Current_Hour = "0" + Current_Hour;
            }

            if (Current_Min.trim().length() < 2) {
                Current_Min = "0" + Current_Min;
            }

            Location = new LocationTrack(getApplicationContext());

            Travel_Editor.putInt("Status", 1);
            Travel_Editor.putString("StartTime", Current_Hour + ":" + Current_Min);
            Travel_Editor.putString("Travel", "First");
            Travel_Editor.apply();

            //Todo Must Check Permission With Gps And Network
            if (Location.CheckGpsAndNetwork() && Location.PermissionGranted()) {

                Log.e(TAG , "Perm Need Or GPs Is OFF");
                ShortTravel_Waiting();
                Location = new LocationTrack(getApplicationContext());//
                Location.getLocation();

            } else {
                ShortTravel_Second();
            }

        } else if (intent.getAction().equals(Constants.ACTION.N_EndTravel)) {/////////////////////// End Travel

            Location = new LocationTrack(getApplicationContext());

            //Todo Must Check Permission With Gps And Network
            if (Location.CheckGpsAndNetwork() && Location.PermissionGranted()) {

                Log.e(TAG , "Perm Need Or GPs Is OFF");
                String First_LocLat = Travel_Shp.getString("F_LocLat", null);

                //Check First Location Value
                if (First_LocLat != null) {

                    //First Location Value Is'nt Null - Get End Location
                    ShortTravel_Waiting();
                    Location = new LocationTrack(getApplicationContext());//
                    Location.getLocation();

                } else {
                    //First Location Value Is Null (User Skipped) - Go To Complete Travel Activity
                    QuickTravelActivity();
                }
            } else {
                // Gps Is off
                QuickTravelActivity();
            }

        } else if (intent.getAction().equals(Constants.ACTION.N_CancelTravel)) { /////////////////// Cancel Travel

            ReseTTravelStatus();

            ShortTravel_First();

            Toast.makeText(this, getResources().getString(R.string.ShortNotify_CancelTravel_Toast), Toast.LENGTH_SHORT).show();

        } else if (intent.getAction().equals(Constants.ACTION.N_ResetQuickTravel)) { /////////////// Reset Quick Travel

            ReseTTravelStatus();

            ShortTravel_First();

        } else if (intent.getAction().equals(Constants.ACTION.N_ResetQuickWithoutLunch)) { ///////// Reset Quick Travel2

            ReseTTravelStatus();

        }

        return START_STICKY;

    }

    private void ShortTravel_First() {

        RemoteViews views = new RemoteViews(getPackageName(), R.layout.shortnotify_first);

        views.setViewVisibility(R.id.status_bar_icon, View.VISIBLE);
        views.setViewVisibility(R.id.status_bar_album_art, View.GONE);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


        Intent StartTravelIntent = new Intent(this, NotificationService.class);
        StartTravelIntent.setAction(Constants.ACTION.N_StartTravel);
        PendingIntent pStartTravelIntent = PendingIntent.getService(this, 0, StartTravelIntent, 0);

        views.setOnClickPendingIntent(R.id.Nstart_travel, pStartTravelIntent);

        views.setTextViewText(R.id.status_bar_text, getResources().getString(R.string.ShortNotify_StartTravel_Header1));

        views.setTextViewText(R.id.status_bar_artist_name, getResources().getString(R.string.ShortNotify_StartTravel_Header2));

        status = new Notification.Builder(this).build();
        /*long[] vibrate = {0, 100, 0, 100};
        status.vibrate = vibrate;*/
        status.contentView = views;
        status.flags = Notification.FLAG_ONGOING_EVENT;
        status.flags = Notification.FLAG_NO_CLEAR;
        status.icon = R.drawable.notif_icon;
        status.contentIntent = pendingIntent;

        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
    }

    private void ShortTravel_Second() {

        RemoteViews views = new RemoteViews(getPackageName(), R.layout.shortnotify_second);

        views.setViewVisibility(R.id.status_bar_icon, View.VISIBLE);
        views.setViewVisibility(R.id.status_bar_album_art, View.GONE);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Constants.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Intent StartTravelIntent = new Intent(this, NotificationService.class);
        StartTravelIntent.setAction(Constants.ACTION.N_StartTravel);
        PendingIntent pStartTravelIntent = PendingIntent.getService(this, 0, StartTravelIntent, 0);


        ////////////////////////////////////////////////////////////////////////////////////////////TODO

        Intent EndTravelIntent = new Intent(this, NotificationService.class);
        EndTravelIntent.setAction(Constants.ACTION.N_EndTravel);
        PendingIntent pEndTravelIntent = PendingIntent.getService(this, 0, EndTravelIntent, 0);

        views.setOnClickPendingIntent(R.id.Nend_travel, pEndTravelIntent);

        Intent CancelTravelIntent = new Intent(this, NotificationService.class);
        CancelTravelIntent.setAction(Constants.ACTION.N_CancelTravel);
        PendingIntent pCancelTravelIntent = PendingIntent.getService(this, 0, CancelTravelIntent, 0);

        views.setOnClickPendingIntent(R.id.NCancel_travel, pCancelTravelIntent);

        ////////////////////////////////////////////////////////////////////////////////////////////TODO

        views.setOnClickPendingIntent(R.id.Nstart_travel, pStartTravelIntent);

        views.setTextViewText(R.id.status_bar_text, getResources().getString(R.string.ShortNotify_InTravel_Header1));

        status = new Notification.Builder(this).build();
        /*long[] vibrate = {0, 500, 200, 300};
        status.vibrate = vibrate;*/

        status.contentView = views;

        status.flags = Notification.FLAG_ONGOING_EVENT;
        status.flags = Notification.FLAG_NO_CLEAR;
        status.icon = R.drawable.notif_icon;
        status.contentIntent = pendingIntent;
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
    }

    private void ShortTravel_Waiting() {

        RemoteViews views = new RemoteViews(getPackageName(), R.layout.shortnotify_waiting);

        views.setViewVisibility(R.id.status_bar_icon, View.VISIBLE);
        views.setViewVisibility(R.id.status_bar_album_art, View.GONE);

        Intent SkipSearchingIntent = new Intent(this, NotificationService.class);
        SkipSearchingIntent.setAction(Constants.ACTION.N_SkipLocationSearching);
        PendingIntent PSkipSearchingIntent = PendingIntent.getService(this, 0, SkipSearchingIntent, 0);

        views.setOnClickPendingIntent(R.id.NManual_Inserting, PSkipSearchingIntent);

        views.setTextViewText(R.id.status_bar_text, getResources().getString(R.string.Please_Wait));

        views.setTextViewText(R.id.status_bar_artist_name, getResources().getString(R.string.ShortNotify_SearchLocation));

        status = new Notification.Builder(this).build();

        status.contentView = views;
        status.flags = Notification.FLAG_ONGOING_EVENT;
        status.flags = Notification.FLAG_NO_CLEAR;
        status.icon = R.drawable.notif_icon;
        status.contentIntent = PSkipSearchingIntent;

        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);

    }

    private void Collapse_StatusBar() {

        try {
            Object service = getSystemService("statusbar");
            Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
            if (Build.VERSION.SDK_INT <= 16) {
                Method collapse = statusbarManager.getMethod("collapse");
                collapse.setAccessible(true);
                collapse.invoke(service);
            } else {
                Method collapse2 = statusbarManager.getMethod("collapsePanels");
                collapse2.setAccessible(true);
                collapse2.invoke(service);
            }
        } catch (Exception ex) {
        }

    }

    private void QuickTravelActivity() {

        Intent intent = new Intent(getApplicationContext(), Quick_Travel.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Collapse_StatusBar();
        startActivity(intent);

    }

    private void ReseTTravelStatus() {

        Travel_Editor.putInt("Status", 0);
        Travel_Editor.putString("StartTime", "");
        Travel_Editor.putString("F_LocLat", null);
        Travel_Editor.putString("F_LocLong", null);
        Travel_Editor.putString("E_LocLat", null);
        Travel_Editor.putString("E_LocLong", null);
        Travel_Editor.apply();

    }

}
