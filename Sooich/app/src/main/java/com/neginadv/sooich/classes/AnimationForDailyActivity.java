package com.neginadv.sooich.classes;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.neginadv.sooich.R;

/**
 * Created by EhsanYazdanyar on 5/5/2018.
 */

public class AnimationForDailyActivity {


    private Animation Slide_Top;
    private Animation Slide_Bottom;
    private Animation Slide_Top_Back;
    private Animation Slide_Bottom_back;

    private View Next_View;
    private View Previous_View;
    private android.os.Handler Handler = new Handler();

    public AnimationForDailyActivity(Context c , View Next_month , View Previous_month){

        Slide_Top = AnimationUtils.loadAnimation(c, R.anim.slide_top);
        Slide_Bottom = AnimationUtils.loadAnimation(c, R.anim.slide_bottom);
        Slide_Top_Back = AnimationUtils.loadAnimation(c, R.anim.slide_top_back);
        Slide_Bottom_back = AnimationUtils.loadAnimation(c, R.anim.slide_bottom_back);

        this.Next_View = Next_month;
        this.Previous_View = Previous_month;

    }

    public void start() {
//        Handler.removeCallbacks(Update);
        Handler.post(Update);
    }

    public void stop() {
        Handler.removeCallbacks(Update);
    }

    private Runnable Update  = new Runnable() {
        public void run() {

            Next_View.startAnimation(Slide_Top);
            Slide_Top.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation arg0) {
                }

                @Override
                public void onAnimationRepeat(Animation arg0) {
                }

                @Override
                public void onAnimationEnd(Animation arg0) {
                    Next_View.startAnimation(Slide_Top_Back);
                }
            });

            Previous_View.startAnimation(Slide_Bottom);
            Slide_Bottom.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation arg0) {
                }

                @Override
                public void onAnimationRepeat(Animation arg0) {
                }

                @Override
                public void onAnimationEnd(Animation arg0) {
                    Previous_View.startAnimation(Slide_Bottom_back);
                }
            });

            Handler.postDelayed(Update, 2000);

        }
    };

}
