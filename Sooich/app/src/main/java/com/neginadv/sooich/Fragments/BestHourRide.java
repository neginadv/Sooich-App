package com.neginadv.sooich.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.Dialogs.Msg_Dialog2;
import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.VolleyErrorHelper;
import com.neginadv.sooich.classes.AnimateMonthViews;
import com.neginadv.sooich.classes.BaseBackPressedListener;
import com.neginadv.sooich.classes.GetMonthByIndex;
import com.neginadv.sooich.classes.HandleNextAndPrevMonth;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.Shp.LoginSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by EhsanYazdanyar on 4/10/2018.
 */

public class BestHourRide extends Fragment {

    @BindView(R.id.BHR_Success_Cont)
    RelativeLayout Success_Cont;

    @BindView(R.id.BHR_arrow_month_left)
    View Previous_month;
    @BindView(R.id.BHR_arrow_month_right)
    View Next_month;

    @BindView(R.id.BHR_year)
    TextView year;
    @BindView(R.id.BHR_Most_Time)
    TextView Best_Time;
    @BindView(R.id.BHR_Least_Time)
    TextView Worst_Time;
    @BindView(R.id.BHR_Average_Travel_Txt)
    TextView Average_Travel;
    @BindView(R.id.BHR_month_view)
    TextView month_view;
    @BindView(R.id.BHR_Least_NoValue)
    TextView BHR_Least_NoValue;
    @BindView(R.id.BHR_Most_NoValue)
    TextView BHR_Most_NoValue;

    @BindView(R.id.BHR_Most_Img)
    ImageView Best_Img;
    @BindView(R.id.BHR_Least_Img)
    ImageView Worst_Img;

    @BindView(R.id.BHR_back_btn)
    Button back_btn;
    @BindView(R.id.BHR_btn_month_left)
    Button Previous_month_btn;
    @BindView(R.id.BHR_btn_month_right)
    Button Next_month_btn;


    Unbinder unbinder;
    private AnimateMonthViews animateMonthViews;
    private int Month = 0;
    private String Token = "";
    private ConvertNumbers_Fa_En Num_Converter;

    boolean FirstLunch = true;
    private Msg_Dialog2 Error_Dialog, Progress_Dialog;

    private static final String TAG = "Best Hour Ride";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_best_hour_ride, container, false);

        ini(view);

        Request_BestHour(getActivity(), null);

        return view;
    }


    private void ini(View view) {
        unbinder = ButterKnife.bind(this, view);
        ManageOnBackPressed();
        Num_Converter = new ConvertNumbers_Fa_En();
        Token = new LoginSessionManager(getActivity()).GetUserToken();
    }

    @OnClick(R.id.BHR_back_btn)
    public void GoBack() {
        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.putExtra("G_Omomi_Main", "G_Omomi_Main");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @OnClick({R.id.BHR_btn_month_right, R.id.BHR_btn_month_left})
    public void Next_PrevMonth(Button btn) {

        if (btn.getId() == R.id.BHR_btn_month_right) {
            Request_BestHour(getActivity(), "N");
        } else {
            Request_BestHour(getActivity(), "P");
        }

    }

    public void Clear_All_Views() {

        Best_Time.setText("");
        Best_Img.setImageResource(0);
        Worst_Time.setText("");
        Worst_Img.setImageResource(0);
        Average_Travel.setText("");
        BHR_Most_NoValue.setText("");
        BHR_Least_NoValue.setText("");

    }

    public void Request_BestHour(final Activity activity, String action) {

        String New_M = new HandleNextAndPrevMonth().Action(action, Month);

        JSONObject object = new JSONObject();

        try {
            object.put("month", New_M);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Progress(true);

        JsonObjectRequest ObjReq = new JsonObjectRequest(Request.Method.POST, ServiceApi.Get_Hour_Ranking, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Progress(false);
                Success_Cont.setVisibility(View.VISIBLE);
                Clear_All_Views();

                FirstLunch = false;
                try {
                    String worst_hour = response.getString("worst_hour");

                    if (worst_hour.equals("0")) {
                        BHR_Least_NoValue.setText(getResources().getString(R.string.BHR_No_Result));
                    } else {

                        JSONObject Worst_Hour = response.getJSONObject("worst_hour");

                        String Hour = Worst_Hour.getString("hour");
                        String Income = Worst_Hour.getString("income");

                        Worst_Time.setText(getResources().getString(R.string.Hour) + " " + Num_Converter.EN_TO_FA(Hour));
                        setTimeImage(Integer.valueOf(Hour), Worst_Img);

                    }

                    String best_hour = response.getString("best_hour");

                    if (best_hour.equals("0")) {

                        BHR_Most_NoValue.setText(getResources().getString(R.string.BHR_No_Result));

                    } else {

                        JSONObject Best_Hour = response.getJSONObject("best_hour");
                        String Hour = Best_Hour.getString("hour");
                        String Income = Best_Hour.getString("income");

                        Best_Time.setText(getResources().getString(R.string.Hour) + " " + Num_Converter.EN_TO_FA(Hour));
                        setTimeImage(Integer.valueOf(Hour), Best_Img);

                    }

                    String Year = response.getString("year");
                    Month = Integer.valueOf(response.getString("month"));
                    String day_av = response.getString("day_av");

                    String MyMonth = new GetMonthByIndex().GetMonth(Month);
                    month_view.setText(MyMonth);

                    year.setText(Num_Converter.EN_TO_FA(Year));
                    Average_Travel.setText(Num_Converter.EN_TO_FA(day_av));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e(TAG, "response : " + response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Progress(false);

                if (FirstLunch) {
                    ErrorDialog();
                }

                new VolleyErrorHelper().getMessage(error, activity, null, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", Token);
                return params;

            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        ObjReq.setRetryPolicy(policy);

        String Reg_Tag = "Request_BestHour_Tag";
        RequestController.getInstance().addToRequestQueue(ObjReq, Reg_Tag);

    }

    private void ErrorDialog() {
        Error_Dialog = new Msg_Dialog2(getActivity(), getString(R.string.ERR_RecieveData));
        Error_Dialog.HaveAttentionImage(true);
        Error_Dialog.cancel_Btn.setText(getString(R.string.Back));
        Error_Dialog.OK_Btn.setText(getString(R.string.Retry));

        Error_Dialog.SetOkDialogResult(new Msg_Dialog2.Msg_Ok_Btn() {
            @Override
            public void GetOkAction() {
                Error_Dialog.CloseDialog();
                Request_BestHour(getActivity(), "");
            }
        });

        Error_Dialog.SetCancelDialogResult(new Msg_Dialog2.MSG_Cancel_Btn() {
            @Override
            public void GetCancelAction() {
                GoBack();
            }
        });
    }

    private void Progress(boolean Enable) {

        if (Enable) {
            Progress_Dialog = new Msg_Dialog2(getActivity());
        } else {
            if (Progress_Dialog != null) {
                Progress_Dialog.CloseDialog();
                Progress_Dialog = null;
            }
        }

    }

    public void setTimeImage(int Time, ImageView Img) {

        if (Time <= 5 && Time >= 0) {

            Img.setImageResource(R.mipmap.bhr_moon);
        } else if (Time > 5 && Time <= 18) {

            Img.setImageResource(R.mipmap.bhr_sun);
        } else if (Time > 18 && Time <= 20) {

            Img.setImageResource(R.mipmap.bhr_sun_moon);
        } else if (Time > 20 && Time <= 23) {

            Img.setImageResource(R.mipmap.bhr_moon);
        }

    }

    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                GoBack();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (animateMonthViews == null) {
            animateMonthViews = new AnimateMonthViews(getActivity(), Next_month, Previous_month);
        }
        animateMonthViews.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            animateMonthViews.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


}
