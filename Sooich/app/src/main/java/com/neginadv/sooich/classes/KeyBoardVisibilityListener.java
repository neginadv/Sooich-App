package com.neginadv.sooich.classes;

/**
 * Created by EhsanYz on 11/21/2018.
 */

public interface KeyBoardVisibilityListener {
    void onKeyboardVisibilityChange(boolean KeyboardVisible);
}
