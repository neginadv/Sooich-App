package com.neginadv.sooich.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.Converter.ConvertDoubleToString;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.Dialogs.Msg_Dialog2;
import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.VolleyErrorHelper;
import com.neginadv.sooich.classes.BaseBackPressedListener;
import com.neginadv.sooich.classes.GetMonthByIndex;
import com.neginadv.sooich.classes.HandleNextAndPrevMonth;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.setCamaToNumber;
import com.neginadv.sooich.Shp.LoginSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by EhsanYazdanyar on 4/10/2018.
 */

public class DailyActivityByOrder extends Fragment {

    @BindView(R.id.DABO_Success_Cont)
    RelativeLayout Success_Cont;

    @BindView(R.id.DABO_TP_Year)
    TextView year;
    @BindView(R.id.DABO_TP_MONTH_Txt)
    TextView month_view;
    @BindView(R.id.DABO_TP_DAY_Txt)
    TextView Day_View;
    @BindView(R.id.DABO_Snapp_amount)
    TextView Snapp_Income;
    @BindView(R.id.DABO_Snapp_Num_Travel)
    TextView Snapp_Count;
    @BindView(R.id.DABO_Tap30_amount)
    TextView Tap30_Income;
    @BindView(R.id.DABO_Tap30_Num_Travel)
    TextView Tap30_Count;
    @BindView(R.id.DABO_Ding_amount)
    TextView Ding_Income;
    @BindView(R.id.DABO_Ding_Num_Travel)
    TextView Ding_Count;
    @BindView(R.id.DABO_Carpino_amount)
    TextView Carpino_Income;
    @BindView(R.id.DABO_Carpino_Num_Travel)
    TextView Carpino_Count;


    Unbinder unbinder;
    int Month, Day = 0;
    private String Token = "";
    private ConvertNumbers_Fa_En Num_Converter;

    boolean FirstLunch = true;
    private Msg_Dialog2 Error_Dialog, Progress_Dialog;

    private static final String TAG = "Daily Activity By Order";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_daily_activity_by_order, container, false);
        ini(view);

        Request_DailyActivity_ByOrder(getActivity());

        return view;

    }

    private void ini(View view) {

        unbinder = ButterKnife.bind(this, view);
        ManageOnBackPressed();
        Num_Converter = new ConvertNumbers_Fa_En();
        Token = new LoginSessionManager(getActivity()).GetUserToken();

    }

    @OnClick(R.id.DABO_back_btn)
    public void GoBack() {
        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.putExtra("G_Safar_Main", "G_Safar_Main");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @OnClick({R.id.DABO_TP_MONTH_Btn_Down, R.id.DABO_TP_MONTH_Btn_Up})
    public void Next_PrevMonth(Button Btn) {
        if (Btn.getId() == R.id.DABO_TP_MONTH_Btn_Down) {
            //prev
            SetNewMonth("P");
        } else {
            //next
            SetNewMonth("N");
        }
    }

    @OnClick({R.id.DABO_TP_DAY_Btn_Down, R.id.DABO_TP_DAY_Btn_Up})
    public void Next_PrevDay(Button Btn) {
        if (Btn.getId() == R.id.DABO_TP_DAY_Btn_Down) {
            //prev
            ChangeDay(Day, "P", Month);
        } else {
            //next
            ChangeDay(Day, "N", Month);
        }
    }

    @OnClick(R.id.DABO_GetReport_Btn)
    public void Getreport() {
        Request_DailyActivity_ByOrder(getActivity());
    }

    public void Clear_All_Views() {

        Snapp_Income.setText("");
        Snapp_Count.setText("");

        Tap30_Income.setText("");
        Tap30_Count.setText("");

        Ding_Income.setText("");
        Ding_Count.setText("");

        Carpino_Income.setText("");
        Carpino_Count.setText("");

    }

    public void Request_DailyActivity_ByOrder(final Activity activity) {

        String New_M = "";
        String New_Day = "";

        if (Month == 0) {
            New_M = "";
        } else {
            New_M = String.valueOf(Month);
        }

        if (Day == 0) {
            New_Day = "";
        } else {
            New_Day = String.valueOf(Day);
        }

        final JSONObject object = new JSONObject();

        try {
            object.put("month", New_M);
            object.put("day", New_Day);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Progress(true);

        JsonObjectRequest ObjReq = new JsonObjectRequest(Request.Method.POST, ServiceApi.Daily_ActivityByOrder, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Progress(false);
                Success_Cont.setVisibility(View.VISIBLE);
                Clear_All_Views();

                FirstLunch = false;

                try {

                    JSONObject trip_taxi_co = response.getJSONObject("trip_taxi_co");

                    GetTaxisObject("ding", Ding_Count, Ding_Income, trip_taxi_co);
                    GetTaxisObject("snapp", Snapp_Count, Snapp_Income, trip_taxi_co);
                    GetTaxisObject("carpino", Carpino_Count, Carpino_Income, trip_taxi_co);
                    GetTaxisObject("tapsi", Tap30_Count, Tap30_Income, trip_taxi_co);


                    String Year = response.getString("year");
                    Month = Integer.valueOf(response.getString("month"));
                    Day = Integer.valueOf(response.getString("day"));

                    String MyMonth = new GetMonthByIndex().GetMonth(Month);
                    month_view.setText(MyMonth);
                    Day_View.setText(Num_Converter.EN_TO_FA(String.valueOf(Day)));
                    year.setText(Num_Converter.EN_TO_FA(Year));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, e.getMessage());
                }

                Log.e(TAG, "response : " + response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Progress(false);

                if (FirstLunch) {
                    ErrorDialog();
                }

                new VolleyErrorHelper().getMessage(error, activity, null, null);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", Token);
                return params;

            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        ObjReq.setRetryPolicy(policy);

        String Reg_Tag = "Request_DailyActivity_ByOrder_Tag";
        RequestController.getInstance().addToRequestQueue(ObjReq, Reg_Tag);

    }

    private void ErrorDialog() {
        Error_Dialog = new Msg_Dialog2(getActivity(), getString(R.string.ERR_RecieveData));
        Error_Dialog.HaveAttentionImage(true);
        Error_Dialog.cancel_Btn.setText(getString(R.string.Back));
        Error_Dialog.OK_Btn.setText(getString(R.string.Retry));

        Error_Dialog.SetOkDialogResult(new Msg_Dialog2.Msg_Ok_Btn() {
            @Override
            public void GetOkAction() {
                Error_Dialog.CloseDialog();
                Request_DailyActivity_ByOrder(getActivity());
            }
        });

        Error_Dialog.SetCancelDialogResult(new Msg_Dialog2.MSG_Cancel_Btn() {
            @Override
            public void GetCancelAction() {
                GoBack();
            }
        });
    }

    private void Progress(boolean Enable) {

        if (Enable) {
            Progress_Dialog = new Msg_Dialog2(getActivity());
        } else {
            if (Progress_Dialog != null) {
                Progress_Dialog.CloseDialog();
                Progress_Dialog = null;
            }
        }

    }

    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                GoBack();
            }
        });
    }

    public void ChangeDay(int Now, String Action, int Month) {

        if (Action.equals("N")) {

            if (Month > 6 && Month != 12) {
                if (Now == 30) {
                    Day = 1;
                } else {
                    Day = Now + 1;
                }
            } else if (Month <= 6) {
                if (Now == 31) {
                    Day = 1;
                } else {
                    Day = Now + 1;
                }
            } else if (Month == 12) {
                if (Now == 29) {
                    Day = 1;
                } else {
                    Day = Now + 1;
                }
            }

        } else if (Action.equals("P")) {
            if (Month > 6 && Month != 12) {
                if (Now == 1) {
                    Day = 30;
                } else {
                    Day = Now - 1;
                }
            } else if (Month <= 6) {
                if (Now == 1) {
                    Day = 31;
                } else {
                    Day = Now - 1;
                }
            } else if (Month == 12) {
                if (Now == 1) {
                    Day = 29;
                } else {
                    Day = Now - 1;
                }
            }
        }

        Day_View.setText(Num_Converter.EN_TO_FA(String.valueOf(Day)));

    }

    public void SetNewMonth(String Action) {

        int Pre_Month = Integer.valueOf(new HandleNextAndPrevMonth().Action(Action, Month));
        month_view.setText(new GetMonthByIndex().GetMonth(Pre_Month));
        Day_View.setText("1");
        Day = 1;
        Month = Pre_Month;

    }

    public void GetTaxisObject(String Taxi, TextView Count, TextView Income, JSONObject object) {

        try {
            if (object.has(Taxi)) {
                JSONObject Obj = object.getJSONObject(Taxi);
                Count.setText(Num_Converter.EN_TO_FA(Obj.getString("no_trip")));
                Income.setText(Num_Converter.EN_TO_FA(new setCamaToNumber().Edit(new ConvertDoubleToString().ConvertDoubleToString(Obj.getString("income"))) + " " + getResources().getString(R.string.Rial)));

            } else {
                Count.setText(Num_Converter.EN_TO_FA("0"));
                Income.setText(Num_Converter.EN_TO_FA("0") + " " + getResources().getString(R.string.Rial));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "GetTaxisObject : " + e.getMessage());
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
