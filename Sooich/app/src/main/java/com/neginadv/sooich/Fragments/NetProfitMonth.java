package com.neginadv.sooich.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.neginadv.sooich.ActivityForFragments;
import com.neginadv.sooich.Converter.ConvertNumbers_Fa_En;
import com.neginadv.sooich.Dialogs.Msg_Dialog2;
import com.neginadv.sooich.R;
import com.neginadv.sooich.ServiceApi;
import com.neginadv.sooich.VolleyErrorHelper;
import com.neginadv.sooich.classes.AnimateMonthViews;
import com.neginadv.sooich.classes.BaseBackPressedListener;
import com.neginadv.sooich.classes.GetMonthByIndex;
import com.neginadv.sooich.classes.HandleNextAndPrevMonth;
import com.neginadv.sooich.classes.RequestController;
import com.neginadv.sooich.classes.setCamaToNumber;
import com.neginadv.sooich.Shp.LoginSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by EhsanYazdanyar on 4/8/2018.
 */

public class NetProfitMonth extends Fragment {

    @BindView(R.id.NPM_Success_Cont)
    RelativeLayout Success_Cont;

    @BindView(R.id.NPM_month_view)
    TextView month_view;
    @BindView(R.id.NPM_Year)
    TextView Year_Txt;
    @BindView(R.id.NPM_All_Income_Amount)
    TextView All_Cost;
    @BindView(R.id.NPM_Fuel_Amount)
    TextView Fuel;
    @BindView(R.id.NPM_Tire_Amount)
    TextView Tire;
    @BindView(R.id.NPM_Break_Amount)
    TextView Break;
    @BindView(R.id.NPM_Oil_Amount)
    TextView Oil;
    @BindView(R.id.NPM_Net_Profit_Amount)
    TextView Net_Profit;

    @BindView(R.id.NPM_arrow_month_left)
    View Previous_month;
    @BindView(R.id.NPM_arrow_month_right)
    View Next_month;

    Unbinder unbinder;
    private AnimateMonthViews animateMonthViews;
    int Month = 0;
    private String Token = "";
    private ConvertNumbers_Fa_En Num_Converter;

    boolean FirstLunch = true;
    private Msg_Dialog2 Error_Dialog, Progress_Dialog;

    private static final String TAG = "Net Profit Month";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.f_net_profit_month, container, false);
        ini(view);

        Request_Profit_Month(getActivity(), "");

        return view;
    }

    private void ini(View view) {

        ManageOnBackPressed();
        unbinder = ButterKnife.bind(this, view);
        Num_Converter = new ConvertNumbers_Fa_En();
        Token = new LoginSessionManager(getActivity()).GetUserToken();

    }

    @OnClick({R.id.NPM_btn_month_right, R.id.NPM_btn_month_left})
    public void Next_PrevMonth(Button btn) {

        Progress(false);

        if (btn.getId() == R.id.NPM_btn_month_right) {
            Request_Profit_Month(getActivity(), "N");
        } else {
            Request_Profit_Month(getActivity(), "P");
        }

    }

    @OnClick(R.id.NPM_back_btn)
    public void GoBack() {
        Intent intent = new Intent(getActivity(), ActivityForFragments.class);
        intent.putExtra("G_Safar_Main", "G_Safar_Main");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    private void Clear_All_Views() {

        All_Cost.setText("");
        Fuel.setText("");
        Tire.setText("");
        Break.setText("");
        Oil.setText("");
        Net_Profit.setText("");

    }

    private void Request_Profit_Month(final Activity activity, String action) {

        String New_M = new HandleNextAndPrevMonth().Action(action, Month);

        JSONObject object = new JSONObject();

        try {
            object.put("month", New_M);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Progress(true);

        JsonObjectRequest ObjReq = new JsonObjectRequest(Request.Method.POST, ServiceApi.Profit_Month, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Progress(false);
                Success_Cont.setVisibility(View.VISIBLE);
                Clear_All_Views();

                FirstLunch = false;

                try {
                    String Year = response.getString("year");
                    Month = Integer.valueOf(response.getString("month"));

                    All_Cost.setText(Num_Converter.EN_TO_FA(new setCamaToNumber().Edit(response.getString("income"))));
                    Fuel.setText(Num_Converter.EN_TO_FA(new setCamaToNumber().Edit(response.getString("fuel"))));
                    Tire.setText(Num_Converter.EN_TO_FA(new setCamaToNumber().Edit(response.getString("tires"))));
                    Break.setText(Num_Converter.EN_TO_FA(new setCamaToNumber().Edit(response.getString("lent_tormoz"))));
                    Oil.setText(Num_Converter.EN_TO_FA(new setCamaToNumber().Edit(response.getString("roqan_motor"))));
                    Net_Profit.setText(Num_Converter.EN_TO_FA(new setCamaToNumber().Edit(response.getString("profit"))));

                    String MyMonth = new GetMonthByIndex().GetMonth(Month);
                    month_view.setText(MyMonth);
                    Year_Txt.setText(Num_Converter.EN_TO_FA(Year));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e(TAG, "response : " + response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Progress(false);

                if (FirstLunch) {
                    ErrorDialog();
                }

                new VolleyErrorHelper().getMessage(error, activity, null, null);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("USERTOKEN", Token);
                return params;
            }
        };

        int socketTimeout = ServiceApi.SocketTimeout;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        ObjReq.setRetryPolicy(policy);

        String Reg_Tag = "Request_Profit_Month";
        RequestController.getInstance().addToRequestQueue(ObjReq, Reg_Tag);

    }

    private void ErrorDialog() {
        Error_Dialog = new Msg_Dialog2(getActivity(), getString(R.string.ERR_RecieveData));
        Error_Dialog.HaveAttentionImage(true);
        Error_Dialog.cancel_Btn.setText(getString(R.string.Back));
        Error_Dialog.OK_Btn.setText(getString(R.string.Retry));

        Error_Dialog.SetOkDialogResult(new Msg_Dialog2.Msg_Ok_Btn() {
            @Override
            public void GetOkAction() {
                Error_Dialog.CloseDialog();
                Request_Profit_Month(getActivity(), "");
            }
        });

        Error_Dialog.SetCancelDialogResult(new Msg_Dialog2.MSG_Cancel_Btn() {
            @Override
            public void GetCancelAction() {
                GoBack();
            }
        });
    }

    private void Progress(boolean Enable) {

        if (Enable) {
            Progress_Dialog = new Msg_Dialog2(getActivity());
        } else {
            if (Progress_Dialog != null) {
                Progress_Dialog.CloseDialog();
                Progress_Dialog = null;
            }
        }

    }

    public void ManageOnBackPressed() {

        ((ActivityForFragments) getActivity()).setOnBackPressedListener(new BaseBackPressedListener((FragmentActivity) getActivity()) {
            @Override
            public void doBack() {
                GoBack();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (animateMonthViews == null) {
            animateMonthViews = new AnimateMonthViews(getActivity(), Next_month, Previous_month);
        }
        animateMonthViews.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            animateMonthViews.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Error_Dialog != null) {
            Error_Dialog.dismiss();
            Error_Dialog = null;
        }

        if (Progress_Dialog != null) {
            Progress_Dialog.dismiss();
            Progress_Dialog = null;
        }
        unbinder.unbind();
    }

}
