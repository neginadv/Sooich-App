package com.neginadv.sooich.classes;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by EhsanYazdanyar on 01/09/2018.
 */

public class Banner_Handler {

    private Context c;
    private SharedPreferences shp;

    public Banner_Handler(Context context){
        this.c = context;
    }

    public void Set_Banner_To_ImageView(String URL , ImageView Adv_imgView , final ProgressBar Adv_PBar , final TextView Adv_txtView){

        Adv_PBar.setVisibility(View.VISIBLE);
        Picasso.get().load(URL).fit().centerCrop().into(Adv_imgView , new Callback.EmptyCallback(){
            @Override
            public void onSuccess() {
                super.onSuccess();
                Adv_PBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onError(Exception e) {
                super.onError(e);
                Adv_txtView.setVisibility(View.VISIBLE);
                Adv_PBar.setVisibility(View.INVISIBLE);
            }

        });

    }


}
