package com.neginadv.sooich.classes;

/**
 * Created by EhsanYazdanyar on 4/24/2018.
 */

public interface IOnFocusListenable {
    public void onWindowFocusChanged(boolean hasFocus);
}
